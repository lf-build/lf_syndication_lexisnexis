﻿using System.IO;
using LendFoundry.Configuration;
using LendFoundry.Configuration.Client;
using LendFoundry.EventHub;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Lookup.Client;
using LendFoundry.Foundation.ServiceDependencyResolver;
using LendFoundry.Foundation.Services;
using LendFoundry.Security.Tokens;
using LendFoundry.Syndication.LexisNexis.ADLFunctions.Proxy;
using LendFoundry.Syndication.LexisNexis.Bankruptcy.Proxy;
using LendFoundry.Syndication.LexisNexis.CriminalRecord.Proxy;
using LendFoundry.Tenant.Client;
using LendFoundry.Syndication.LexisNexis.LienJudgment;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.PlatformAbstractions;
using Swashbuckle.AspNetCore.Swagger;

namespace LendFoundry.Syndication.LexisNexis.Api {
    /// <summary>
    /// Startup
    /// </summary>
    public class Startup {
        /// <summary>
        /// Startup
        /// </summary>
        /// <param name="env"></param>
        public Startup (IHostingEnvironment env) { }
        /// <summary>
        /// Configure
        /// </summary>
        /// <param name="app"></param>
        /// <param name="env"></param>
        /// <param name="loggerFactory"></param>
        public void Configure (IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory) {
            try {

                app.UseHealthCheck ();
                app.UseCors(env);
                app.UseSwagger ();

                app.UseSwaggerUI (c => {
                    c.SwaggerEndpoint ("/swagger/v1/swagger.json", "LexisNexis Service");
                });

                app.UseErrorHandling ();
                app.UseRequestLogging ();
                // app.UseMiddleware<RequestLoggingMiddleware>();
                app.UseMvc ();
            } catch (System.Exception) {
                throw;
            }
        }
        /// <summary>
        /// ConfigureServices
        /// </summary>
        /// <param name="services"></param>
        public void ConfigureServices (IServiceCollection services) {

            services.AddSwaggerGen (c => {
                c.SwaggerDoc("docs", new Info
                {
                    Version = PlatformServices.Default.Application.ApplicationVersion,
                        Title = "lexisnexis"
                });
                c.AddSecurityDefinition ("apiKey", new ApiKeyScheme () {
                    Type = "apiKey",
                        Name = "Authorization",
                        Description = "For accessing the API a valid JWT token must be passed in all the queries in the 'Authorization' header. The syntax used in the 'Authorization' header should be Bearer xxxxx.yyyyyy.zzzz",
                        In = "header"
                });
                c.DescribeAllEnumsAsStrings ();
                c.IgnoreObsoleteProperties ();
                c.DescribeStringEnumsInCamelCase ();
                c.IgnoreObsoleteActions ();
                var basePath = PlatformServices.Default.Application.ApplicationBasePath;
                var xmlPath = Path.Combine (basePath, "LendFoundry.Syndication.LexisNexis.Api.xml");
                c.IncludeXmlComments (xmlPath);
            });
            services.TryAddSingleton<IHttpContextAccessor, HttpContextAccessor> ();
            // services
            services.AddTokenHandler ();
            services.AddHttpServiceLogging (Settings.ServiceName);
            services.AddTenantTime ();
            services.AddConfigurationService<LexisNexisConfiguration> (Settings.ServiceName);
            services.AddDependencyServiceUriResolver<LexisNexisConfiguration>(Settings.ServiceName);
            services.AddMvc().AddLendFoundryJsonOptions((options) =>{});

            // aspnet mvc related           
            services.AddCors ();

            // interface implements
           
            services.AddEventHub (Settings.ServiceName);
            services.AddTenantService ();

            services.AddLookupService ();

            // configuration factory
            services.AddTransient (provider => provider.GetRequiredService<IConfigurationService<LexisNexisConfiguration>> ().Get ());

            services.AddTransient<ILexisNexisService, LexisNexisService> ();
            services.AddTransient<IBankruptcyService, BankruptcyService> ();
            services.AddTransient<IBankruptcyProxy, BankruptcyProxy> ();
            services.AddTransient<ICriminalRecordService, CriminalRecordService> ();
            services.AddTransient<ICriminalRecordProxy, CriminalRecordProxy> ();
            services.AddTransient<IADLSearchPorxy, ADLSearchPorxy> ();
            services.AddTransient<ILienJudgmentClient,LienJudgmentClient> ();

            // eventhub factory
            services.AddTransient (p => {
                var currentTokenReader = p.GetService<ITokenReader> ();
                var staticTokenReader = new StaticTokenReader (currentTokenReader.Read ());
                return p.GetService<IEventHubClientFactory> ().Create (staticTokenReader);
            });
        }

    }
}