﻿using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Lookup;
using LendFoundry.Foundation.Services;
using LendFoundry.Syndication.LexisNexis.LienJudgment;
using LendFoundry.SyndicationStore.Events;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Threading.Tasks;
using LendFoundry.Syndication.LexisNexis.LienJudgment.Response.Search;

namespace LendFoundry.Syndication.LexisNexis.Api.Controllers
{
    /// <summary>
    /// LienJudgmentController
    /// </summary>
    [Route("/lien-judgment")]
    public class LienJudgmentController : ExtendedController
    {
        /// <summary>
        /// LienJudgmentController
        /// </summary>
        /// <param name="service"></param>
        /// <param name="logger"></param>
        /// <param name="lookup"></param>
        /// <returns></returns>
        public LienJudgmentController(ILexisNexisService service, ILogger logger,ILookupService lookup) : base(logger)
        {
            Service = service;
            Lookup = lookup;
        }

        private ILexisNexisService Service { get; }

        private ILookupService Lookup { get; }

        /// <summary>
        /// Search
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost("/{entityType}/{entityId}/search")]
        public async Task<IActionResult> Search(string entityType, string entityId, [FromBody]LienJudgmentSearchRequest request)
        {
             return await ExecuteAsync(async () => Ok(await Service.LienJudgmentSearch(entityType,entityId,request)));
        }
    }
}
