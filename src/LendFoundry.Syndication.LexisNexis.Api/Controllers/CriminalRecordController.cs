﻿using LendFoundry.EventHub;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Lookup;
using LendFoundry.Foundation.Services;
using LendFoundry.Syndication.LexisNexis.CriminalRecord;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.LexisNexis.Api.Controllers
{
    /// <summary>
    /// CriminalRecordController
    /// </summary>
    [Route("/criminalrecord")]
    public class CriminalRecordController : ExtendedController
    {
        /// <summary>
        /// CriminalRecordController constructor
        /// </summary>
        /// <param name="service"></param>
        /// <param name="logger"></param>
        /// <param name="eventHubClient"></param>
        /// <param name="lookup"></param>
        /// <returns></returns>
        public CriminalRecordController(ILexisNexisService service, ILogger logger, IEventHubClient eventHubClient, ILookupService lookup) : base(logger)
        {
            Service = service;
            EventHubClient = eventHubClient;
            Lookup = lookup;
        }

        private ILexisNexisService Service { get; }

        private IEventHubClient EventHubClient { get; }

        private ILookupService Lookup { get; }

        /// <summary>
        /// CriminalSearch
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost("{entityType}/{entityId}/criminalrecord-search")]
        [ProducesResponseType(typeof(CriminalRecord.Proxy.ICriminalRecordSearchResponse), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public async Task<IActionResult> CriminalSearch(string entityType, string entityId, [FromBody]CriminalRecordSearchRequest request)
        {
            return await ExecuteAsync(async () => Ok(await Service.CriminalSearch(entityType, entityId, request)));
        }

        /// <summary>
        /// CriminalReport
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost("{entityType}/{entityId}/criminalrecord-report")]
        [ProducesResponseType(typeof(CriminalRecord.Proxy.CriminalReport.ICriminalReportResponse), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public async Task<IActionResult> CriminalReport(string entityType, string entityId, [FromBody]CriminalRecordReportRequest request)
        {
            return await ExecuteAsync(async () => Ok(await Service.CriminalRecordReport(entityType, entityId, request)));
        }
    }
}
