﻿using System.Threading.Tasks;
using LendFoundry.EventHub;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Lookup;
using LendFoundry.Foundation.Services;
using LendFoundry.Syndication.LexisNexis.ADLFunctions;
using Microsoft.AspNetCore.Mvc;

namespace LendFoundry.Syndication.LexisNexis.Api.Controllers
{
    /// <summary>
    /// ADLSearchController
    /// </summary>
    public class ADLSearchController : ExtendedController
    {
        /// <summary>
        /// ADLSearchController constructor
        /// </summary>
        /// <param name="service"></param>
        /// <param name="logger"></param>
        /// <param name="eventHubClient"></param>
        /// <param name="lookup"></param>
        /// <returns></returns>
        public ADLSearchController(ILexisNexisService service, ILogger logger, IEventHubClient eventHubClient, ILookupService lookup) : base(logger)
        {
            Service = service;
            EventHubClient = eventHubClient;
            Lookup = lookup;
        }
        private ILexisNexisService Service { get; }

        private IEventHubClient EventHubClient { get; }

        private ILookupService Lookup { get; }
        
        /// <summary>
        /// ADLSearch
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost("{entityType}/{entityId}/adl-search")]
        [ProducesResponseType(typeof(IADLSearchResponse), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public async Task<IActionResult> ADLSearch(string entityType, string entityId, [FromBody]ADLSearchRequest request)
        {
            return await ExecuteAsync(async () => Ok(await Service.ADLSearch(entityType, entityId, request)));
        }
    }
}