﻿using LendFoundry.EventHub;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using LendFoundry.Foundation.Lookup;
using LendFoundry.Syndication.LexisNexis.Bankruptcy;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.LexisNexis.Api.Controllers
{
    /// <summary>
    /// BankruptcyController
    /// </summary>
    [Route("/bankruptcy")]
    public class BankruptcyController : ExtendedController
    {
        /// <summary>
        /// BankruptcyController
        /// </summary>
        /// <param name="service"></param>
        /// <param name="logger"></param>
        /// <param name="eventHubClient"></param>
        /// <param name="lookup"></param>
        /// <returns></returns>
        public BankruptcyController(ILexisNexisService service, ILogger logger, IEventHubClient eventHubClient, ILookupService lookup) : base(logger)
        {
            Service = service;
            EventHubClient = eventHubClient;
            Lookup = lookup;
        }

        private ILexisNexisService Service { get; }

        private IEventHubClient EventHubClient { get; }

        private ILookupService Lookup { get; }

        /// <summary>
        /// BankruptcySearch
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost("{entityType}/{entityId}/bankruptcy-search")]
        [ProducesResponseType(typeof(IBankruptcySearchResponse), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public async Task<IActionResult> BankruptcySearch(string entityType, string entityId, [FromBody]SearchBankruptcyRequest request)
        {
            return await ExecuteAsync(async () => Ok(await Service.FcraBankruptcySearch(entityType, entityId, request)));
        }

        /// <summary>
        /// FcraBankruptcyReport
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost("{entityType}/{entityId}/bankruptcy-fcra-report")]
        [ProducesResponseType(typeof(IBankruptcyReportResponse), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public async Task<IActionResult> FcraBankruptcyReport(string entityType, string entityId, [FromBody]BankruptcyFcraReportRequest request)
        {
            return await ExecuteAsync(async () => Ok(await Service.FcraBankruptcyReport(entityType, entityId, request)));
        }

        /// <summary>
        /// BankruptcyReport
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost("{entityType}/{entityId}/bankruptcy-report")]
        [ProducesResponseType(typeof(IBankruptcyReportResponse), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public async Task<IActionResult> BankruptcyReport(string entityType, string entityId, [FromBody]BankruptcyReportRequest request)
        {
            return await ExecuteAsync(async () => Ok(await Service.BankruptcyReport(entityType, entityId, request)));
        }
    }
}
