﻿namespace LendFoundry.Syndication.LexisNexis.Api.Configuration
{
    /// <summary>
    /// LexisNexis
    /// </summary>
    public class LexisNexisApiConfiguration
    {

    }

    /// <summary>
    /// LexisNexis
    /// </summary>
    public class SpecialFeatureType
    {
        /// <summary>
        /// LexisNexis
        /// </summary>
        /// <value></value>
        public string SpecialFeatureCode { get; set; }
        /// <summary>
        /// LexisNexis
        /// </summary>
        /// <value></value>
        public string SpecialFeatureValue { get; set; }
    }

    /// <summary>
    /// LexisNexis
    /// </summary>
    public class User
    {
        /// <summary>
        /// LexisNexis
        /// </summary>
        public User() { }

        /// <summary>
        /// LexisNexis
        /// </summary>
        /// <value></value>
        public string ReferenceCode { get; set; }
        /// <summary>
        /// LexisNexis
        /// </summary>
        /// <value></value>
        public string BillingCode { get; set; }
        /// <summary>
        /// LexisNexis
        /// </summary>
        /// <value></value>
        public string QueryId { get; set; }
        /// <summary>
        /// LexisNexis
        /// </summary>
        /// <value></value>
        public string GrammLeachBlileyPurpose { get; set; }
        /// <summary>
        /// LexisNexis
        /// </summary>
        /// <value></value>
        public string DriverLicensePurpose { get; set; }
        /// <summary>
        /// LexisNexis
        /// </summary>
        /// <value></value>
        public EndUserInfo EndUser { get; set; }
        /// <summary>
        /// LexisNexis
        /// </summary>
        /// <value></value>
        public int MaxWaitSeconds { get; set; }
        /// <summary>
        /// LexisNexis
        /// </summary>
        /// <value></value>
        public string AccountNumber { get; set; }
    }

    /// <summary>
    /// LexisNexis
    /// </summary>
    public class FraudPointOption
    {
        /// <summary>
        /// LexisNexis
        /// </summary>
        /// <value></value>
        public FraudPointModels IncludeModels { get; set; }
        /// <summary>
        /// LexisNexis
        /// </summary>
        /// <value></value>
        public string AttributesVersionRequest { get; set; }
        /// <summary>
        /// LexisNexis
        /// </summary>
        /// <value></value>
        public string RedFlagsReport { get; set; }
        /// <summary>
        /// LexisNexis
        /// </summary>
        /// <value></value>
        public bool IncludeRiskIndices { get; set; }
    }

    /// <summary>
    /// LexisNexis
    /// </summary>
    public class FraudPointModels
    {
        /// <summary>
        /// LexisNexis
        /// </summary>
        /// <value></value>
        public string FraudPointModel { get; set; }
        /// <summary>
        /// LexisNexis
        /// </summary>
        /// <value></value>
        public ModelRequest[] ModelRequests { get; set; }
    }

    /// <summary>
    /// LexisNexis
    /// </summary>
    public class ModelRequest
    {
        /// <summary>
        /// LexisNexis
        /// </summary>
        /// <value></value>
        public string ModelName { get; set; }
        /// <summary>
        /// LexisNexis
        /// </summary>
        /// <value></value>
        public ModelOption[] ModelOptions { get; set; }
    }

    /// <summary>
    /// LexisNexis
    /// </summary>
    public class ModelOption
    {
        /// <summary>
        /// LexisNexis
        /// </summary>
        /// <value></value>
        public string OptionName { get; set; }
        /// <summary>
        /// LexisNexis
        /// </summary>
        /// <value></value>
        public string OptionValue { get; set; }
    }

    /// <summary>
    /// LexisNexis
    /// </summary>
    public class FlexIdOption
    {
        /// <summary>
        /// LexisNexis
        /// </summary>
        /// <value></value>
        public string[] WatchLists { get; set; }
        /// <summary>
        /// LexisNexis
        /// </summary>
        /// <value></value>
        public bool UseDateOfBirthFilter { get; set; } = true;
        /// <summary>
        /// LexisNexis
        /// </summary>
        /// <value></value>
        public int DateOfBirthRadius { get; set; } = 5;
        /// <summary>
        /// LexisNexis
        /// </summary>
        /// <value></value>
        public bool IncludeMsOverride { get; set; } = true;
        /// <summary>
        /// LexisNexis
        /// </summary>
        /// <value></value>
        public bool PoBoxCompliance { get; set; } = true;
        /// <summary>
        /// LexisNexis
        /// </summary>
        /// <value></value>
        public RequireExactMatch RequireExactMatch { get; set; }
        /// <summary>
        /// LexisNexis
        /// </summary>
        /// <value></value>
        public bool IncludeAllRiskIndicators { get; set; } = true;
        /// <summary>
        /// LexisNexis
        /// </summary>
        /// <value></value>
        public bool IncludeVerifiedElementSummary { get; set; } = true;
        /// <summary>
        /// LexisNexis
        /// </summary>
        /// <value></value>
        public bool IncludeDrivingLicenseVerification { get; set; } = true;
        /// <summary>
        /// LexisNexis
        /// </summary>
        /// <value></value>
        public DateOfBirthMatchOptions DateOfBirthMatch { get; set; }
        /// <summary>
        /// LexisNexis
        /// </summary>
        /// <value></value>
        public FlexIdModels IncludeModels { get; set; }
        /// <summary>
        /// LexisNexis
        /// </summary>
        /// <value></value>
        public string CustomCviModelName { get; set; }
        /// <summary>
        /// LexisNexis
        /// </summary>
        /// <value></value>
        public string LastSeenThreshold { get; set; }
        /// <summary>
        /// LexisNexis
        /// </summary>
        /// <value></value>
        public bool IncludeMiOverride { get; set; } = true;
        /// <summary>
        /// LexisNexis
        /// </summary>
        /// <value></value>
        public bool IncludeSsnVerification { get; set; } = true;
        /// <summary>
        /// LexisNexis
        /// </summary>
        /// <value></value>
        public OptionsForCviCalculation CviCalculationOptions { get; set; }
        /// <summary>
        /// LexisNexis
        /// </summary>
        /// <value></value>
        public string InstantIdVersion { get; set; }
    }

    /// <summary>
    /// LexisNexis
    /// </summary>
    public class DateOfBirthMatchOptions
    {
        /// <summary>
        /// LexisNexis
        /// </summary>
        /// <value></value>
        public DateOfBirthMatchType MatchType { get; set; } = DateOfBirthMatchType.FuzzyCCYYMMDD;
        /// <summary>
        /// LexisNexis
        /// </summary>
        /// <value></value>
        public int MatchYearRadius { get; set; } = 5;
    }

    /// <summary>
    /// LexisNexis
    /// </summary>
    public enum DateOfBirthMatchType
    {
        /// <summary>
        /// LexisNexis
        /// </summary>
        FuzzyCCYYMMDD,
        /// <summary>
        /// LexisNexis
        /// </summary>
        FuzzyCCYYMM,
        /// <summary>
        /// LexisNexis
        /// </summary>
        RadiusCCYY,
        /// <summary>
        /// LexisNexis
        /// </summary>
        ExactCCYYMMDD,
        /// <summary>
        /// LexisNexis
        /// </summary>
        ExactCCYYMM
    }

    /// <summary>
    /// LexisNexis
    /// </summary>
    public class FlexIdModels
    {
        /// <summary>
        /// LexisNexis
        /// </summary>
        /// <value></value>
        public FraudPointModelWithOptions FraudPointModel { get; set; }
        /// <summary>
        /// LexisNexis
        /// </summary>
        /// <value></value>
        public ModelRequest[] ModelRequests { get; set; }
    }

    /// <summary>
    /// LexisNexis
    /// </summary>
    public class OptionsForCviCalculation
    {
        /// <summary>
        /// LexisNexis
        /// </summary>
        public OptionsForCviCalculation() { }

        /// <summary>
        /// LexisNexis
        /// </summary>
        /// <value></value>
        public bool IncludeDateOfBirth { get; set; } = true;
        /// <summary>
        /// LexisNexis
        /// </summary>
        /// <value></value>
        public bool IncludeDriverLicense { get; set; } = true;
        /// <summary>
        /// LexisNexis
        /// </summary>
        /// <value></value>
        public bool DisableCustomerNetworkOption { get; set; } = true;
    }

    /// <summary>
    /// LexisNexis
    /// </summary>
    public class FraudPointModelWithOptions
    {
        /// <summary>
        /// LexisNexis
        /// </summary>
        public FraudPointModelWithOptions() { }

        /// <summary>
        /// LexisNexis
        /// </summary>
        /// <value></value>
        public string ModelName { get; set; }
        /// <summary>
        /// LexisNexis
        /// </summary>
        /// <value></value>
        public bool IncludeRiskIndices { get; set; } = true;
    }

    /// <summary>
    /// LexisNexis
    /// </summary>
    public class RequireExactMatch
    {
        /// <summary>
        /// LexisNexis
        /// </summary>
        /// <value></value>
        public bool LastName { get; set; } = false;
        /// <summary>
        /// LexisNexis
        /// </summary>
        /// <value></value>
        public bool FirstName { get; set; } = false;
        /// <summary>
        /// LexisNexis
        /// </summary>
        /// <value></value>
        public bool FirstNameAllowNickname { get; set; } = true;
        /// <summary>
        /// LexisNexis
        /// </summary>
        /// <value></value>
        public bool Address { get; set; } = false;
        /// <summary>
        /// LexisNexis
        /// </summary>
        /// <value></value>
        public bool HomePhone { get; set; } = false;
        /// <summary>
        /// LexisNexis
        /// </summary>
        /// <value></value>
        public bool Ssn { get; set; } = false;
        /// <summary>
        /// LexisNexis
        /// </summary>
        /// <value></value>
        public bool DriverLicense { get; set; } = false;
    }

    /// <summary>
    /// LexisNexis
    /// </summary>
    public class InstantIdOption
    {
        /// <summary>
        /// LexisNexis
        /// </summary>
        public InstantIdOption() { }

        /// <summary>
        /// LexisNexis
        /// </summary>
        /// <value></value>
        public string[] WatchLists { get; set; }
        /// <summary>
        /// LexisNexis
        /// </summary>
        /// <value></value>
        public bool IncludeClOverride { get; set; } = true;
        /// <summary>
        /// LexisNexis
        /// </summary>
        /// <value></value>
        public bool IncludeMsOverride { get; set; } = true;
        /// <summary>
        /// LexisNexis
        /// </summary>
        /// <value></value>
        public bool IncludeDrivingLicenseVerification { get; set; } = true;
        /// <summary>
        /// LexisNexis
        /// </summary>
        /// <value></value>
        public bool PoBoxCompliance { get; set; } = true;
        /// <summary>
        /// LexisNexis
        /// </summary>
        /// <value></value>
        public InstantIdModelsFp IncludeModels { get; set; }
        /// <summary>
        /// LexisNexis
        /// </summary>
        /// <value></value>
        public string RedFlagsReport { get; set; } = "Version1";
        /// <summary>
        /// LexisNexis
        /// </summary>
        /// <value></value>
        public string GlobalWatchlistThreshold { get; set; }
        /// <summary>
        /// LexisNexis
        /// </summary>
        /// <value></value>
        public DateOfBirthMatchOptions DateOfBirthMatch { get; set; }
        /// <summary>
        /// LexisNexis
        /// </summary>
        /// <value></value>
        public bool IncludeAllRiskIndicators { get; set; } = true;
        /// <summary>
        /// LexisNexis
        /// </summary>
        /// <value></value>
        public RequireExactMatch RequireExactMatch { get; set; }
        /// <summary>
        /// LexisNexis
        /// </summary>
        /// <value></value>
        public string CustomCviModelName { get; set; }
        /// <summary>
        /// LexisNexis
        /// </summary>
        /// <value></value>
        public string LastSeenThreshold { get; set; }
        /// <summary>
        /// LexisNexis
        /// </summary>
        /// <value></value>
        public bool IncludeMiOverride { get; set; } = true;
        /// <summary>
        /// LexisNexis
        /// </summary>
        /// <value></value>
        public OptionsForCviCalculation CviCalculationOptions { get; set; }
        /// <summary>
        /// LexisNexis
        /// </summary>
        /// <value></value>
        public string InstantIdVersion { get; set; }
        /// <summary>
        /// LexisNexis
        /// </summary>
        /// <value></value>
        public bool IncludeDeliveryPointBarcode { get; set; } = true;
    }

    /// <summary>
    /// LexisNexis
    /// </summary>
    public class InstantIdModelsFp
    {
        /// <summary>
        /// LexisNexis
        /// </summary>
        /// <value></value>
        public bool Thindex { get; set; }
        /// <summary>
        /// LexisNexis
        /// </summary>
        /// <value></value>
        public StudentDefenderModel StudentDefenderModel { get; set; }
        /// <summary>
        /// LexisNexis
        /// </summary>
        /// <value></value>
        public ModelRequest[] ModelRequests { get; set; }
        /// <summary>
        /// LexisNexis
        /// </summary>
        /// <value></value>
        public FraudPointModelWithOptions FraudPointModel { get; set; }
    }

    /// <summary>
    /// LexisNexis
    /// </summary>
    public class StudentDefenderModel
    {
        /// <summary>
        /// LexisNexis
        /// </summary>
        /// <value></value>
        public bool StudentDefender { get; set; }
        /// <summary>
        /// LexisNexis
        /// </summary>
        /// <value></value>
        public bool IsStudentApplicant { get; set; }
    }
}