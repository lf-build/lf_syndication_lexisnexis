﻿using System;

namespace LendFoundry.Syndication.LexisNexis.Api
{
    /// <summary>
    /// Settings
    /// </summary>
    public class Settings
    {
        /// <summary>
        /// ServiceName
        /// </summary>
        /// <returns></returns>
        public static string ServiceName => Environment.GetEnvironmentVariable($"CONFIGURATION_NAME") ?? "lexisnexis";
    }
}