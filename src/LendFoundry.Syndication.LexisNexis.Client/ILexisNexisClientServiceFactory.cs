﻿using LendFoundry.Security.Tokens;

namespace LendFoundry.Syndication.LexisNexis.Client
{
    /// <summary>
    /// ILexisNexisClientServiceFactory
    /// </summary>
    public interface ILexisNexisClientServiceFactory
    {
        /// <summary>
        /// Create
        /// </summary>
        /// <param name="reader"></param>
        /// <returns></returns>
        ILexisNexisService Create(ITokenReader reader);
    }
}

