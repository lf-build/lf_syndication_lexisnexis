﻿using System;
using LendFoundry.Security.Tokens;
using Microsoft.Extensions.DependencyInjection;

namespace LendFoundry.Syndication.LexisNexis.Client {
    /// <summary>
    /// LexisNexisClientServiceExtentions
    /// </summary>
    public static class LexisNexisClientServiceExtentions {
        /// <summary>
        /// AddLexisNexisService
        /// </summary>
        /// <param name="services"></param>
        /// <param name="endpoint"></param>
        /// <param name="port"></param>
        /// <returns></returns>
        [Obsolete ("Need to use the overloaded with Uri")]
        public static IServiceCollection AddLexisNexisService (this IServiceCollection services, string endpoint, int port) {
            services.AddTransient<ILexisNexisClientServiceFactory> (p => new LexisNexisClientServiceFactory (p, endpoint, port));
            services.AddTransient (p => p.GetService<ILexisNexisClientServiceFactory> ().Create (p.GetService<ITokenReader> ()));
            return services;
        }
    }
}