﻿using System;
using LendFoundry.Security.Tokens;
using LendFoundry.Foundation.Client;
using LendFoundry.Foundation.Logging;
using Microsoft.Extensions.DependencyInjection;

namespace LendFoundry.Syndication.LexisNexis.Client
{
    /// <summary>
    /// LexisNexisClientServiceFactory
    /// </summary>
    public class LexisNexisClientServiceFactory : ILexisNexisClientServiceFactory
    {
        #region Constructors
        /// <summary>
        /// LexisNexisClientServiceFactory
        /// </summary>
        /// <param name="provider"></param>
        /// <param name="endpoint"></param>
        /// <param name="port"></param>
        public LexisNexisClientServiceFactory(IServiceProvider provider, string endpoint, int port)
        {
            Provider = provider;
            Uri = new UriBuilder("http", endpoint, port).Uri;
        }
        /// <summary>
        /// LexisNexisClientServiceFactory
        /// </summary>
        /// <param name="provider"></param>
        /// <param name="uri"></param>
        public LexisNexisClientServiceFactory(IServiceProvider provider, Uri uri = null)
        {
            Provider = provider;
            Uri = uri;
        }
        #endregion

        #region Private Properties
        private IServiceProvider Provider { get; }

        private Uri Uri { get; }

        #endregion

        #region Public Methods
        /// <summary>
        /// Create
        /// </summary>
        /// <param name="tokenReader"></param>
        /// <returns></returns>
        public ILexisNexisService Create(ITokenReader tokenReader)
        {
            var uri = Uri;
            if (uri == null)
            {
                var logger = Provider.GetService<ILoggerFactory>().Create(NullLogContext.Instance);
                uri = Provider.GetRequiredService<IDependencyServiceUriResolverFactory>().Create(tokenReader, logger).Get("LexisNexis");
            }
            var client = Provider.GetServiceClient(tokenReader, uri);
             return new LexisNexisClientService(client);
        }

        #endregion
    }
}

