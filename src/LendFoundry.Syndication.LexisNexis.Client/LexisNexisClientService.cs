﻿using System.Threading.Tasks;
using LendFoundry.Syndication.LexisNexis.Bankruptcy;
using LendFoundry.Syndication.LexisNexis.CriminalRecord;
using LendFoundry.Syndication.LexisNexis.CriminalRecord.Proxy;
using LendFoundry.Syndication.LexisNexis.CriminalRecord.Proxy.CriminalReport;
using LendFoundry.Foundation.Client;
using RestSharp;
using LendFoundry.Syndication.LexisNexis.Bankruptcy.Proxy.BankruptcyReport;
using LendFoundry.Syndication.LexisNexis.Bankruptcy.Proxy;
using LendFoundry.Syndication.LexisNexis.ADLFunctions;
using LendFoundry.Syndication.LexisNexis.LienJudgment;
using LendFoundry.Syndication.LexisNexis.LienJudgment.Response.Search;
using LendFoundry.Syndication.LexisNexis.LienJudgment.Search;

namespace LendFoundry.Syndication.LexisNexis.Client
{
    /// <summary>
    /// LexisNexisClientService
    /// </summary>
    public class LexisNexisClientService : ILexisNexisService
    {
        /// <summary>
        /// LexisNexisClientService
        /// </summary>
        /// <param name="client"></param>
        public LexisNexisClientService(IServiceClient client)
        {
            Client = client;
        }

        private IServiceClient Client { get; }

        /// <summary>
        /// FcraBankruptcyReport
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="reportRequest"></param>
        /// <returns></returns>
        public async Task<IBankruptcyReportResponse> FcraBankruptcyReport(string entityType, string entityId, IBankruptcyFcraReportRequest reportRequest)
        {
            var request = new RestRequest("/bankruptcy/{entityType}/{entityId}/bankruptcy-fcra-report", Method.POST);
            request.AddUrlSegment("entityType", entityType);
            request.AddUrlSegment("entityId", entityId);
            request.AddJsonBody(reportRequest);
            return await Client.ExecuteAsync<BankruptcyReportResponse>(request);
        }
        /// <summary>
        /// FcraBankruptcySearch
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="searchRequest"></param>
        /// <returns></returns>
        public async Task<IBankruptcySearchResponse> FcraBankruptcySearch(string entityType, string entityId, ISearchBankruptcyRequest searchRequest)
        {
            var request = new RestRequest("/bankruptcy/{entityType}/{entityId}/bankruptcy-search", Method.POST);
            request.AddUrlSegment("entityType", entityType);
            request.AddUrlSegment("entityId", entityId);
            request.AddJsonBody(searchRequest);
            return await Client.ExecuteAsync<BankruptcySearchResponse>(request);
        }
        /// <summary>
        /// CriminalRecordReport
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="criminalRecordRequest"></param>
        /// <returns></returns>
        public async Task<ICriminalReportResponse> CriminalRecordReport(string entityType, string entityId, ICriminalRecordReportRequest criminalRecordRequest)
        {
            var request = new RestRequest("/criminalrecord/{entityType}/{entityId}/criminalrecord-report", Method.POST);
            request.AddUrlSegment("entityType", entityType);
            request.AddUrlSegment("entityId", entityId);
            request.AddJsonBody(criminalRecordRequest);
            return await Client.ExecuteAsync<CriminalReportResponse>(request);
        }
        /// <summary>
        /// CriminalSearch
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="criminalSearchRequest"></param>
        /// <returns></returns>
        public async Task<ICriminalRecordSearchResponse> CriminalSearch(string entityType, string entityId, ICriminalRecordSearchRequest criminalSearchRequest)
        {
            var request = new RestRequest("/criminalrecord/{entityType}/{entityId}/criminalrecord-search", Method.POST);
            request.AddUrlSegment("entityType", entityType);
            request.AddUrlSegment("entityId", entityId);
            request.AddJsonBody(criminalSearchRequest);
            return await Client.ExecuteAsync<CriminalRecordSearchResponse>(request);
        }
        /// <summary>
        /// ADLSearch
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<IADLSearchResponse> ADLSearch(string entityType, string entityId, IADLSearchRequest request)
        {
            var adlrequest = new RestRequest("{entityType}/{entityId}/adl-search", Method.POST);
            adlrequest.AddUrlSegment("entityType", entityType);
            adlrequest.AddUrlSegment("entityId", entityId);
            adlrequest.AddJsonBody(request);
            return await Client.ExecuteAsync<ADLSearchResponse>(adlrequest);
        }
        /// <summary>
        /// BankruptcyReport
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="reportRequest"></param>
        /// <returns></returns>
        public async Task<IBankruptcyReportResponse> BankruptcyReport(string entityType, string entityId, IBankruptcyReportRequest reportRequest)
        {
            var request = new RestRequest("/bankruptcy/{entityType}/{entityId}/bankruptcy-report", Method.POST);
            request.AddUrlSegment("entityType", entityType);
            request.AddUrlSegment("entityId", entityId);
            request.AddJsonBody(reportRequest);
            return await Client.ExecuteAsync<BankruptcyReportResponse>(request);
        }       
        /// <summary>
        /// LienJudgmentSearch
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="searchRequest"></param>
        /// <returns></returns>
        public async Task<ILienJudgmentSearchResponse> LienJudgmentSearch(string entityType, string entityId, ILienJudgmentSearchRequest searchRequest)
        {
            var request = new RestRequest("lien-judgment/{entityType}/{entityId}/search", Method.POST);
            request.AddUrlSegment("entityType", entityType);
            request.AddUrlSegment("entityId", entityId);
            request.AddJsonBody(searchRequest);
            return await Client.ExecuteAsync<LendFoundry.Syndication.LexisNexis.LienJudgment.Response.Search.LienJudgmentSearchResponse>(request);
        }       
    }
}
