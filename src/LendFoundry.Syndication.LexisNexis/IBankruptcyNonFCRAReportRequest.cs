﻿namespace LendFoundry.Syndication.LexisNexis.Bankruptcy
{
    /// <summary>
    /// lexisnexis
    /// </summary>
    public interface IBankruptcyReportRequest
    {        
        /// <summary>
        /// lexisnexis
        /// </summary>
        string UniqueId { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        string BusinessId { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        string TMSId { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        string OwnerId { get; set; }
    }
}