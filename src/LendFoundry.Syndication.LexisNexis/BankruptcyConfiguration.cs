﻿using LendFoundry.Foundation.Client;
using Newtonsoft.Json;

namespace LendFoundry.Syndication.LexisNexis
{
    /// <summary>
    /// lexisnexis
    /// </summary>
    public class BankruptcyConfiguration
    {
        /// <summary>
        /// lexisnexis
        /// </summary>
        public string BankruptcyFCRAUrl { get; set; }// = "https://wsonline.seisint.com/Demo/WsAccurintFCRA/?ver_=1.89";
        /// <summary>
        /// lexisnexis
        /// </summary>
        public string FCRAUserName { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        public string FCRAPassword { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        public string BankruptcyNonFCRAUrl { get; set; }// = "https://wsonline.seisint.com/Demo/WsAccurintFCRA/?ver_=1.89";
        /// <summary>
        /// lexisnexis
        /// </summary>
        public string NonFCRAUserName { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        public string NonFCRAPassword { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        [JsonConverter(typeof(InterfaceConverter<Bankruptcy.IUser, Bankruptcy.User>))]
        public Bankruptcy.IUser EndUser { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        [JsonConverter(typeof(InterfaceConverter<Bankruptcy.ISearchOption, Bankruptcy.SearchOption>))]
        public Bankruptcy.ISearchOption Options { get; set; }
    }
}