﻿using LendFoundry.Foundation.Client;
using LendFoundry.Syndication.LexisNexis.BusinessReport;
using Newtonsoft.Json;

namespace LendFoundry.Syndication.LexisNexis.Bankruptcy.Proxy.BankruptcyReport
{
    /// <summary>
    /// lexisnexis
    /// </summary>
    public interface IBankruptcyReportComment
    {
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string Description { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        [JsonConverter(typeof(InterfaceConverter<IDate, BusinessReport.Date>))]
        IDate FilingDate { get; set; }
    }
}
