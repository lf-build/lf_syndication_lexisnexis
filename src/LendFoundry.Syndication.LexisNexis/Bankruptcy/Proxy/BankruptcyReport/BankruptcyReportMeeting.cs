using LendFoundry.Foundation.Client;
using LendFoundry.Syndication.LexisNexis.Bankruptcy.Proxy.BankruptcyReport.NonFCRAServiceReference;
using LendFoundry.Syndication.LexisNexis.Bankruptcy.Proxy.BankruptcyReport.ServiceReference;
using LendFoundry.Syndication.LexisNexis.BusinessReport;
using Newtonsoft.Json;

namespace LendFoundry.Syndication.LexisNexis.Bankruptcy.Proxy.BankruptcyReport
{
    /// <summary>
    /// lexisnexis
    /// </summary>
    public class BankruptcyReportMeeting : IBankruptcyReportMeeting
    {
        /// <summary>
        /// lexisnexis
        /// </summary>
        public BankruptcyReportMeeting()
        {
        }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <param name="meeting"></param>
        public BankruptcyReportMeeting(ServiceReference.Bankruptcy3Meeting meeting)
        {
            if (meeting == null)
                return;

            Date = meeting.Date != null ? new BusinessReport.Date { Day = meeting.Date.Day, Month = meeting.Date.Month, Year = meeting.Date.Year } : null;
            Time = meeting.Time;
            Address = meeting.Address;
        }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <param name="meeting"></param>
        public BankruptcyReportMeeting(Bankruptcy2Meeting meeting)
        {
            if (meeting == null)
                return;

            Date = meeting.Date != null ? new BusinessReport.Date { Day = meeting.Date.Day, Month = meeting.Date.Month, Year = meeting.Date.Year } : null;
            Time = meeting.Time;
            Address = meeting.Address;
        }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        [JsonConverter(typeof(InterfaceConverter<IDate,BusinessReport.Date>))]
        public IDate Date { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string Time { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string Address { get; set; }
    }
}