﻿using LendFoundry.Foundation.Client;
using LendFoundry.Syndication.LexisNexis.BusinessReport;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace LendFoundry.Syndication.LexisNexis.Bankruptcy.Proxy.BankruptcyReport
{
    /// <summary>
    /// lexisnexis
    /// </summary>
    public interface IBankruptcyReportDebtor
    {
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string BusinessId { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string UniqueId { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string Ssn { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string AppendedSSN { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string TaxId { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string AppendedTaxId { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        [JsonConverter(typeof(InterfaceConverter<IDate,Date>))]
        IDate DateTransferred { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        [JsonConverter(typeof(InterfaceConverter<IDate, Date>))]
        IDate ConvertedDate { get; set; }       
        
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        [JsonConverter(typeof(InterfaceListConverter<IName, Name>))]
        List<IName> Names { get; set; }
        
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        [JsonConverter(typeof(InterfaceListConverter<IAddress, Address>))]
        List<IAddress> Addresses { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        [JsonConverter(typeof(InterfaceListConverter<IPhone, Phone>))]
        List<IPhone> Phones { get; set; }
    }
}
