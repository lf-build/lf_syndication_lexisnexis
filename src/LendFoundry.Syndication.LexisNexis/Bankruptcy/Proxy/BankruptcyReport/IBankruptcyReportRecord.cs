﻿using System;
using System.Collections.Generic;
using LendFoundry.Foundation.Client;
using LendFoundry.Syndication.LexisNexis.BusinessReport;
using Newtonsoft.Json;

namespace LendFoundry.Syndication.LexisNexis.Bankruptcy.Proxy.BankruptcyReport {
    /// <summary>
    /// lexisnexis
    /// </summary>
    public interface IBankruptcyReportRecord {
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string CaseNumber { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        Boolean IsFcra { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string CourtLocation { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string CourtCode { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string CaseType { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        [JsonConverter (typeof (InterfaceConverter<IDate, BusinessReport.Date>))]
        IDate FilingDate { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        [JsonConverter (typeof (InterfaceConverter<IDate, BusinessReport.Date>))]
        IDate OriginalFilingDate { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        [JsonConverter (typeof (InterfaceConverter<IDate, BusinessReport.Date>))]
        IDate ReopenDate { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        [JsonConverter (typeof (InterfaceConverter<IDate, BusinessReport.Date>))]
        IDate ClosedDate { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string CourtName { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string FilingType { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string FilerType { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string CorpFlag { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string FilingStatus { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string FilingJurisdiction { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string Chapter { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string OriginalChapter { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string JudgeName { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        [JsonConverter(typeof(InterfaceConverter<IBankruptcyReportMeeting, BankruptcyReportMeeting>))]
        IBankruptcyReportMeeting Meeting { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string JudgeIdentification { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        [JsonConverter (typeof (InterfaceConverter<IDate, BusinessReport.Date>))]
        IDate ClaimsDeadline { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>

        [JsonConverter (typeof (InterfaceConverter<IDate, BusinessReport.Date>))]
        IDate ComplaintDeadline { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        [JsonConverter (typeof (InterfaceConverter<IDate, BusinessReport.Date>))]
        IDate DischargeDate { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string Disposition { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string SelfRepresented { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string AssetsForUnsecured { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string Assets { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string Liabilities { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        [JsonConverter (typeof (InterfaceListConverter<IBankruptcyReportStatus, BankruptcyReportStatus>))]
        List<IBankruptcyReportStatus> StatusHistory { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        [JsonConverter (typeof (InterfaceListConverter<IBankruptcyReportComment, BankruptcyReportComment>))]
        List<IBankruptcyReportComment> Comments { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        [JsonConverter (typeof (InterfaceListConverter<IBankruptcyReportTrustee, BankruptcyReportTrustee>))]
        List<IBankruptcyReportTrustee> Trustee { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        [JsonConverter (typeof (InterfaceListConverter<IBankruptcyReportAttorney, BankruptcyReportAttorney>))]
        List<IBankruptcyReportAttorney> Attorneys { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        [JsonConverter (typeof (InterfaceListConverter<IBankruptcyReportDebtor, BankruptcyReportDebtor>))]
        List<IBankruptcyReportDebtor> Debtors { get; set; }
    }
}