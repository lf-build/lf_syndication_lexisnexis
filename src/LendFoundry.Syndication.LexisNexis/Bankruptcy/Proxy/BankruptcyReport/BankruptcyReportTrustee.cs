using LendFoundry.Syndication.LexisNexis.Bankruptcy.Proxy.BankruptcyReport.NonFCRAServiceReference;
using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.LexisNexis.Bankruptcy.Proxy.BankruptcyReport
{
    /// <summary>
    /// lexisnexis
    /// </summary>
    public class BankruptcyReportTrustee : IBankruptcyReportTrustee
    {
        /// <summary>
        /// lexisnexis
        /// </summary>
        public BankruptcyReportTrustee()
        {
        }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <param name="trustee"></param>
        public BankruptcyReportTrustee(ServiceReference.Bankruptcy3Trustee trustee)
        {
            if (trustee == null)
                return;
            UniqueId = trustee.UniqueId;
            Title = trustee.Title;
            Names = trustee.Name != null ? new List<IName> { new Name(trustee.Name) } : new List<IName>();
            Address = trustee.Address != null ? new List<IAddress> { new Address(trustee.Address) } : null;
            Phones = trustee.Phone10 != null ? new List<IPhone> { new Phone { Phone10 = trustee.Phone10 } } : null;
        }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <param name="trustee"></param>
        public BankruptcyReportTrustee(BankruptcyPerson2 trustee)
        {
            if (trustee == null)
                return;
            UniqueId = trustee.UniqueId;
            Names = trustee.Names != null ? new List<IName>(trustee.Names.Select(name => new Name(name))) : null;
            Address = trustee.Addresses != null ? new List<IAddress>(trustee.Addresses.Select(add => new Address(add))) : null;
            Phones = trustee.Phones != null ? new List<IPhone>(trustee.Phones.Select(phone => new Phone(phone))) : null;
            BusinessIds = trustee.BusinessIds;
            //Emails = trustee.Emails != null ? trustee.Emails.ToList():new List<string>(); //deprecated in wsdl
            AppendedSSN = trustee.AppendedSSN;
            TaxId = trustee.TaxId;
            AppendedTaxId = trustee.AppendedTaxId;
        }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string UniqueId { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string Title { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        [JsonConverter(typeof(InterfaceListConverter<IName, Name>))]
        public List<IName> Names { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        [JsonConverter(typeof(InterfaceListConverter<IAddress, Address>))]
        public List<IAddress> Address { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        [JsonConverter(typeof(InterfaceListConverter<IPhone, Phone>))]
        public List<IPhone> Phones { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public BusinessIdentity BusinessIds { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public List<string> Emails { get; set; }

       
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string AppendedSSN { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string TaxId { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string AppendedTaxId { get; set; }
    }
}