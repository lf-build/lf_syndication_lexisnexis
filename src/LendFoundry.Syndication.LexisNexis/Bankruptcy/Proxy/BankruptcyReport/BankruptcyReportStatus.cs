using LendFoundry.Foundation.Client;

using LendFoundry.Syndication.LexisNexis.BusinessReport;
using Newtonsoft.Json;

namespace LendFoundry.Syndication.LexisNexis.Bankruptcy.Proxy.BankruptcyReport
{
    /// <summary>
    /// lexisnexis
    /// </summary>
    public class BankruptcyReportStatus : IBankruptcyReportStatus
    {
        /// <summary>
        /// lexisnexis
        /// </summary>
        public BankruptcyReportStatus() { }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <param name="status"></param>
        public BankruptcyReportStatus(ServiceReference.BankruptcyStatus status)
        {
            if (status == null)
                return;
            Type = status.Type;
            Date = status.Date != null ? new BusinessReport.Date { Year = status.Date.Year, Day = status.Date.Day, Month = status.Date.Month } : null;
        }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <param name="status"></param>
        public BankruptcyReportStatus(NonFCRAServiceReference.BankruptcyStatus status)
        {
            if (status == null)
                return;
            Type = status.Type;
            Date = status.Date != null ? new BusinessReport.Date { Year = status.Date.Year, Day = status.Date.Day, Month = status.Date.Month } : null;
        }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string Type { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        [JsonConverter(typeof(InterfaceConverter<IDate, Date>))]
        public IDate Date { get; set; }
    }
}