﻿using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace LendFoundry.Syndication.LexisNexis.Bankruptcy.Proxy.BankruptcyReport
{
    /// <summary>
    /// lexisnexis
    /// </summary>
    public interface IBankruptcyReportTrustee
    {
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string UniqueId { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string Title { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        [JsonConverter(typeof(InterfaceListConverter<IName, Name>))]
        List<IName> Names { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        [JsonConverter(typeof(InterfaceListConverter<IAddress, Address>))]
        List<IAddress> Address { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        [JsonConverter(typeof(InterfaceListConverter<IPhone, Phone>))]
        List<IPhone> Phones { get; set; }
        //BusinessIdentity BusinessIds { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        List<string> Emails { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string AppendedSSN { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string TaxId { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string AppendedTaxId { get; set; }
    }
}
