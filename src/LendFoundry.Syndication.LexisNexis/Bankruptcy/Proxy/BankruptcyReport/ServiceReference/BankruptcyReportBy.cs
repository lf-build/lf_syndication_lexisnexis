﻿namespace LendFoundry.Syndication.LexisNexis.Bankruptcy.Proxy.BankruptcyReport.ServiceReference
{
    /// <summary>
    /// lexisnexis
    /// </summary>
    public partial class FcraBankruptcyReport3By
    {
        /// <summary>
        /// lexisnexis
        /// </summary>
        public FcraBankruptcyReport3By()
        { }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <param name="reportBy"></param>
        public FcraBankruptcyReport3By(IBankruptcyFcraReportRequest reportBy)
        {
            if (reportBy == null)
                return;

            UniqueId = reportBy.UniqueId;

        }
    }
}
