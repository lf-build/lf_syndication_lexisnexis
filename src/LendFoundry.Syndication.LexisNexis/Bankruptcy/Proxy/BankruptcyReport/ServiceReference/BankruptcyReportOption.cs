﻿namespace LendFoundry.Syndication.LexisNexis.Bankruptcy.Proxy.BankruptcyReport.ServiceReference
{
    /// <summary>
    /// lexisnexis
    /// </summary>
    public partial class FcraBankruptcyReport3Option
    {
        /// <summary>
        /// lexisnexis
        /// </summary>
        public FcraBankruptcyReport3Option()
        { }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <param name="searchOption"></param>
        public FcraBankruptcyReport3Option(ISearchOption searchOption)
        {
            if (searchOption == null)
                return;

            FCRAPurpose = searchOption.FCRAPurpose;
            FCRAPurposeSpecified = true;
        }
    }
}
