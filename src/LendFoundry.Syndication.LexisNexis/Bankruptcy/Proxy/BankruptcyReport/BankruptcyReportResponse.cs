using LendFoundry.Syndication.LexisNexis.Bankruptcy.Proxy.BankruptcyReport.NonFCRAServiceReference;
using LendFoundry.Syndication.LexisNexis.Bankruptcy.Proxy.BankruptcyReport.ServiceReference;
using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.LexisNexis.Bankruptcy.Proxy.BankruptcyReport
{
    /// <summary>
    /// lexisnexis
    /// </summary>
    public class BankruptcyReportResponse : IBankruptcyReportResponse
    {
        /// <summary>
        /// lexisnexis
        /// </summary>
        public BankruptcyReportResponse()
        {
        }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <param name="response"></param>
        /// <param name="isFcra"></param>
        public BankruptcyReportResponse(FcraBankruptcyReport3Response response, Boolean isFcra = true)
        {
            if (response == null)
                return;
            Header = new ResponseHeader(response.Header);
            //RecordCount = response.;
            if (response.BankruptcyReportRecords != null)
            {
                Records = new List<IBankruptcyReportRecord>(response.BankruptcyReportRecords.Select(record => new BankruptcyReportRecord(record, isFcra)));
            }
        }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <param name="response"></param>
        public BankruptcyReportResponse(BankruptcyReport2Response response)
        {
            if (response == null)
                return;
            Header = new ResponseHeader(response.Header);
            //RecordCount = response.;
            if (response.BankruptcyReportRecords != null)
            {
                Records = new List<IBankruptcyReportRecord>(response.BankruptcyReportRecords.Select(record => new BankruptcyReportRecord(record)));
            }
        }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        [JsonConverter(typeof(InterfaceConverter<IResponseHeader, ResponseHeader>))]
        public IResponseHeader Header { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public int RecordCount { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        [JsonConverter(typeof(InterfaceListConverter<IBankruptcyReportRecord, BankruptcyReportRecord>))]
        public List<IBankruptcyReportRecord> Records { get; set; }
    }
}