﻿namespace LendFoundry.Syndication.LexisNexis.Bankruptcy
{
    /// <summary>
    /// lexisnexis
    /// </summary>
    public class BankruptcyFcraReportRequest : IBankruptcyFcraReportRequest
    {
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string UniqueId { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string QueryId { get; set; }

    }
}
