﻿namespace LendFoundry.Syndication.LexisNexis.Bankruptcy.Proxy.BankruptcyReport.NonFCRAServiceReference
{
    /// <summary>
    /// User
    /// </summary>
    public partial class User
    {
        /// <summary>
        /// User
        /// </summary>
        public User()
        { }
        /// <summary>
        /// User
        /// </summary>
        /// <param name="request"></param>
        public User(IBankruptcyReportRequest request)
        {
            if (request == null)
                return;

            QueryId = request.TMSId;

        }

        /// <summary>
        /// User
        /// </summary>
        /// <param name="user"></param>
        public User(IUser user)
        {
            if (user == null)
                return;
            ReferenceCode = user.ReferenceCode;
            BillingCode = user.BillingCode;
            QueryId = user.QueryId;
            GLBPurpose = user.GLBPurpose;
            DLPurpose = user.GLBPurpose;
            EndUser = new EndUserInfo
            {
                City = user.EndUser.City,
                CompanyName = user.EndUser.CompanyName,
                State = user.EndUser.State,
                StreetAddress1 = user.EndUser.StreetAddress1,
                Zip5 = user.EndUser.Zip5
            };
            MaxWaitSeconds = user.MaxWaitSeconds;
            AccountNumber = user.AccountNumber;
        }
    }
}
