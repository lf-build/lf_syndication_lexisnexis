﻿using LendFoundry.Syndication.LexisNexis.BusinessReport;
using LendFoundry.Foundation.Client;
using Newtonsoft.Json;

namespace LendFoundry.Syndication.LexisNexis.Bankruptcy.Proxy.BankruptcyReport
{
    /// <summary>
    /// lexisnexis
    /// </summary>
    public class BankruptcyReportComment : IBankruptcyReportComment
    {
        /// <summary>
        /// lexisnexis
        /// </summary>
        public BankruptcyReportComment() { }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <param name="comment"></param>
        public BankruptcyReportComment(ServiceReference.BankruptcyComment comment)
        {
            if (comment == null)
                return;
            Description = comment.Description;
            FilingDate = comment.FilingDate != null ? new BusinessReport.Date { Year = comment.FilingDate.Year, Day = comment.FilingDate.Day, Month = comment.FilingDate.Month } : null;
        }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <param name="comment"></param>
        public BankruptcyReportComment(NonFCRAServiceReference.BankruptcyComment comment)
        {
            if (comment == null)
                return;
            Description = comment.Description;
            FilingDate = comment.FilingDate != null ? new BusinessReport.Date { Year = comment.FilingDate.Year, Day = comment.FilingDate.Day, Month = comment.FilingDate.Month } : null;
        }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string Description { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        [JsonConverter(typeof(InterfaceConverter<IDate, BusinessReport.Date>))]
        public IDate FilingDate{ get; set; }
    }
}
