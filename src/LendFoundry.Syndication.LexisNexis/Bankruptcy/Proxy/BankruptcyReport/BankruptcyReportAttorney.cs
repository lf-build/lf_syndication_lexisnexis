using LendFoundry.Foundation.Client;
using LendFoundry.Syndication.LexisNexis.Bankruptcy.Proxy.BankruptcyReport.NonFCRAServiceReference;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.LexisNexis.Bankruptcy.Proxy.BankruptcyReport
{
    /// <summary>
    /// lexisnexis
    /// </summary>
    public class BankruptcyReportAttorney : IBankruptcyReportAttorney
    {
        /// <summary>
        /// lexisnexis
        /// </summary>
        public BankruptcyReportAttorney()
        {
        }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <param name="attorney"></param>
        public BankruptcyReportAttorney(ServiceReference.BankruptcyReport3Attorney attorney)
        {
            if (attorney == null)
                return;
            UniqueId = attorney.UniqueId;
            BusinessId = attorney.BusinessId;
            Names = attorney.Phones != null ? new List<IName>(attorney.Names.Select(name => new Name(name))) : null;
            Addresses = attorney.Addresses != null ? new List<IAddress>(attorney.Addresses.Select(address => new Address(address))) : null;
            Phones = attorney.Phones != null ? new List<IPhone>(attorney.Phones.Select(phone => new Phone(phone))) : null;
            Emails = attorney.Emails != null ? new List<string>(attorney.Emails.Select(email => email)) : null;
        }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <param name="attorney"></param>
        public BankruptcyReportAttorney(BankruptcyPerson2 attorney)
        {
            if (attorney == null)
                return;
            UniqueId = attorney.UniqueId;
            BusinessId = attorney.BusinessId;
            Names = attorney.Phones != null ? new List<IName>(attorney.Names.Select(name => new Name(name))) : null;
            Addresses = attorney.Addresses != null ? new List<IAddress>(attorney.Addresses.Select(address => new Address(address))) : null;
            Phones = attorney.Phones != null ? new List<IPhone>(attorney.Phones.Select(ph => new Phone(ph))) : null;
           // Emails = attorney.Emails != null ? new List<string>(attorney.Emails.Select(email => email)) : null;  // deprecated on wsdl
        }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string BusinessId { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string UniqueId { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string TaxId { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string AppendedTaxId { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        [JsonConverter(typeof(InterfaceListConverter<IName, Name>))]
        public List<IName> Names { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        [JsonConverter(typeof(InterfaceListConverter<IAddress, Address>))]
        public List<IAddress> Addresses { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        [JsonConverter(typeof(InterfaceListConverter<IPhone, Phone>))]
        public List<IPhone> Phones { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public List<string> Emails { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        [JsonConverter(typeof(InterfaceListConverter<IAddress, Address>))]
        public List<IAddress> Address { get; set; }

       // public BusinessIdentity BusinessIds { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string AppendedSSN { get; set; }
    }
}