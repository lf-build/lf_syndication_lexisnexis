﻿namespace LendFoundry.Syndication.LexisNexis.Bankruptcy
{
    /// <summary>
    /// lexisnexis
    /// </summary>
    public class BankruptcyReportRequest : IBankruptcyReportRequest
    {
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string BusinessId { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string TMSId { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string UniqueId { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string OwnerId { get; set; }
    }
}
