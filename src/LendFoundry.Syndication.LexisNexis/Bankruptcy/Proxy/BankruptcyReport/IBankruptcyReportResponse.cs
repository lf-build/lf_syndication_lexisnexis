﻿using LendFoundry.Syndication.LexisNexis.Bankruptcy.Proxy.BankruptcyReport;
using System.Collections.Generic;

namespace LendFoundry.Syndication.LexisNexis.Bankruptcy
{
    /// <summary>
    /// lexisnexis
    /// </summary>
    public interface IBankruptcyReportResponse
    {
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        IResponseHeader Header { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        int RecordCount { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        List<IBankruptcyReportRecord> Records { get; set; }
    }
}
