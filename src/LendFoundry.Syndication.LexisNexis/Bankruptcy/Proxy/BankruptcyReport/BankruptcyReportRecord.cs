using LendFoundry.Syndication.LexisNexis.Bankruptcy.Proxy.BankruptcyReport.NonFCRAServiceReference;
using LendFoundry.Foundation.Client;
using LendFoundry.Syndication.LexisNexis.BusinessReport;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.LexisNexis.Bankruptcy.Proxy.BankruptcyReport
{
    /// <summary>
    /// lexisnexis
    /// </summary>
    public class BankruptcyReportRecord : IBankruptcyReportRecord
    {
        /// <summary>
        /// lexisnexis
        /// </summary>
        public BankruptcyReportRecord()
        {
        }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <param name="record"></param>
        /// <param name="isFcra"></param>
        public BankruptcyReportRecord(ServiceReference.BankruptcyReport3Record record, Boolean isFcra = true)
        {
            if (record == null)
                return;

            CaseNumber = record.CaseNumber;
            IsFcra = isFcra;
            CourtLocation = record.CourtLocation;
            CourtCode = record.CourtCode;
            CaseType = record.CaseType;
            FilingDate = record.FilingDate != null ? new BusinessReport.Date { Day = record.FilingDate.Day, Month = record.FilingDate.Month, Year = record.FilingDate.Year } : null;
            OriginalFilingDate = record.OriginalFilingDate != null ? new BusinessReport.Date { Day = record.OriginalFilingDate.Day, Month = record.OriginalFilingDate.Month, Year = record.OriginalFilingDate.Year } : null;
            ReopenDate = record.ReopenDate != null ? new BusinessReport.Date { Day = record.ReopenDate.Day, Month = record.ReopenDate.Month, Year = record.ReopenDate.Year } : null;
            ClosedDate = record.ClosedDate != null ? new BusinessReport.Date { Day = record.ClosedDate.Day, Month = record.ClosedDate.Month, Year = record.ClosedDate.Year } : null;
            CourtName = record.CourtName;
            CourtLocation = record.CourtLocation;
            CourtCode = record.CourtCode;
            CorpFlag = record.CorpFlag;
            FilingType = record.FilingType;
            FilerType = record.FilerType;
            FilingStatus = record.FilingStatus;
            FilingJurisdiction = record.FilingJurisdiction;
            Chapter = record.Chapter;
            OriginalChapter = record.OriginalChapter;
            JudgeName = record.JudgeName;
             Meeting = record.Meeting != null ? new BankruptcyReportMeeting(record.Meeting) : null;
            JudgeIdentification = record.JudgeIdentification;
            ClaimsDeadline = record.ClaimsDeadline != null ? new BusinessReport.Date { Day = record.ClaimsDeadline.Day, Month = record.ClaimsDeadline.Month, Year = record.ClaimsDeadline.Year } : null;
            ComplaintDeadline = record.ComplaintDeadline != null ? new BusinessReport.Date { Day = record.ComplaintDeadline.Day, Month = record.ComplaintDeadline.Month, Year = record.ComplaintDeadline.Year } : null;
            DischargeDate = record.DischargeDate != null ? new BusinessReport.Date { Day = record.DischargeDate.Day, Month = record.DischargeDate.Month, Year = record.DischargeDate.Year } : null;
            Disposition = record.Disposition;
            SelfRepresented = record.SelfRepresented;
            AssetsForUnsecured = record.AssetsForUnsecured;
            Assets = record.Assets;
            Liabilities = record.Liabilities;
            AssetsForUnsecured = record.AssetsForUnsecured;
            StatusHistory = record.DischargeDate != null ? new List<IBankruptcyReportStatus>(record.StatusHistory.Select(status => new BankruptcyReportStatus(status))) : null;
            Comments = record.Comments != null ? new List<IBankruptcyReportComment>(record.Comments.Select(comment => new BankruptcyReportComment(comment))) : null;
            Trustee = record.Trustee != null ? new List<IBankruptcyReportTrustee> { new BankruptcyReportTrustee(record.Trustee) } : new List<IBankruptcyReportTrustee>();
            //record.Trustee != null ? new BankruptcyReportTrustee(record.Trustee) : null;
            Attorneys = record.Attorneys != null ? new List<IBankruptcyReportAttorney>(record.Attorneys.Select(attorneys => new BankruptcyReportAttorney(attorneys))) : null;
            Debtors = record.Debtors != null ? new List<IBankruptcyReportDebtor>(record.Debtors.Select(debtor => new BankruptcyReportDebtor(debtor))) : null;
        }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <param name="record"></param>
        public BankruptcyReportRecord(BankruptcyReport2Record record)
        {
            if (record == null)
                return;
            CaseNumber = record.CaseNumber;
            CourtLocation = record.CourtLocation;
            CourtCode = record.CourtCode;
            CaseType = record.CaseType;
            FilingDate = record.FilingDate != null ? new BusinessReport.Date { Day = record.FilingDate.Day, Month = record.FilingDate.Month, Year = record.FilingDate.Year } : null;
            OriginalFilingDate = record.OriginalFilingDate != null ? new BusinessReport.Date { Day = record.OriginalFilingDate.Day, Month = record.OriginalFilingDate.Month, Year = record.OriginalFilingDate.Year } : null;
            ReopenDate = record.ReopenDate != null ? new BusinessReport.Date { Day = record.ReopenDate.Day, Month = record.ReopenDate.Month, Year = record.ReopenDate.Year } : null;
            ClosedDate = record.ClosedDate != null ? new BusinessReport.Date { Day = record.ClosedDate.Day, Month = record.ClosedDate.Month, Year = record.ClosedDate.Year } : null;
            CourtName = record.CourtName;
            CourtLocation = record.CourtLocation;
            CourtCode = record.CourtCode;
            CorpFlag = record.CorpFlag;
            FilingType = record.FilingType;
            FilerType = record.FilerType;
            FilingStatus = record.FilingStatus;
            FilingJurisdiction = record.FilingJurisdiction;
            Chapter = record.Chapter;
            OriginalChapter = record.OriginalChapter;
            JudgeName = record.JudgeName;
            Meeting = record.Meeting != null ? new BankruptcyReportMeeting(record.Meeting) : null;
            JudgeIdentification = record.JudgeIdentification;
            ClaimsDeadline = record.ClaimsDeadline != null ? new BusinessReport.Date { Day = record.ClaimsDeadline.Day, Month = record.ClaimsDeadline.Month, Year = record.ClaimsDeadline.Year } : null;
            ComplaintDeadline = record.ComplaintDeadline != null ? new BusinessReport.Date { Day = record.ComplaintDeadline.Day, Month = record.ComplaintDeadline.Month, Year = record.ComplaintDeadline.Year } : null;
            DischargeDate = record.DischargeDate != null ? new BusinessReport.Date { Day = record.DischargeDate.Day, Month = record.DischargeDate.Month, Year = record.DischargeDate.Year } : null;
            Disposition = record.Disposition;
            SelfRepresented = record.SelfRepresented.ToString();
            AssetsForUnsecured = record.AssetsForUnsecured.ToString();
            Assets = record.Assets;
            Liabilities = record.Liabilities;
            AssetsForUnsecured = record.AssetsForUnsecured.ToString();
            StatusHistory = record.StatusHistory != null ? new List<IBankruptcyReportStatus>(record.StatusHistory.Select(status => new BankruptcyReportStatus(status))) : new List<IBankruptcyReportStatus>();
            Comments = record.Comments != null ? new List<IBankruptcyReportComment>(record.Comments.Select(comment => new BankruptcyReportComment(comment))) : new List<IBankruptcyReportComment>();
            Trustee = record.Trustees != null ? new List<IBankruptcyReportTrustee>(record.Trustees.Select(trustee => new BankruptcyReportTrustee(trustee))) : new List<IBankruptcyReportTrustee>();
            Attorneys = record.Attorneys != null ? new List<IBankruptcyReportAttorney>(record.Attorneys.Select(attorneys => new BankruptcyReportAttorney(attorneys))) : new List<IBankruptcyReportAttorney>();
            Debtors = record.Debtors != null ? new List<IBankruptcyReportDebtor>(record.Debtors.Select(debtor => new BankruptcyReportDebtor(debtor))) : new List<IBankruptcyReportDebtor>();

        }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string CaseNumber { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public Boolean IsFcra { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string CourtLocation { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string CourtCode { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string CaseType { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        [JsonConverter(typeof(InterfaceConverter<IDate, BusinessReport.Date>))]
        public IDate FilingDate { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        [JsonConverter(typeof(InterfaceConverter<IDate, LendFoundry.Syndication.LexisNexis.BusinessReport.Date>))]
        public IDate OriginalFilingDate { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        [JsonConverter(typeof(InterfaceConverter<IDate, LendFoundry.Syndication.LexisNexis.BusinessReport.Date>))]
        public IDate ReopenDate { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        [JsonConverter(typeof(InterfaceConverter<IDate, LendFoundry.Syndication.LexisNexis.BusinessReport.Date>))]
        public IDate ClosedDate { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string CourtName { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string FilingType { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string FilerType { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string CorpFlag { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string FilingStatus { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string FilingJurisdiction { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string Chapter { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string OriginalChapter { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string JudgeName { get; set; }
       
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        [JsonConverter(typeof(InterfaceConverter<IBankruptcyReportMeeting, BankruptcyReportMeeting>))]
        public IBankruptcyReportMeeting Meeting { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string JudgeIdentification { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        [JsonConverter(typeof(InterfaceConverter<IDate, LendFoundry.Syndication.LexisNexis.BusinessReport.Date>))]
        public IDate ClaimsDeadline { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        [JsonConverter(typeof(InterfaceConverter<IDate, LendFoundry.Syndication.LexisNexis.BusinessReport.Date>))]
        public IDate ComplaintDeadline { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        [JsonConverter(typeof(InterfaceConverter<IDate, LendFoundry.Syndication.LexisNexis.BusinessReport.Date>))]
        public IDate DischargeDate { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string Disposition { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string SelfRepresented { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string AssetsForUnsecured { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string Assets { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string Liabilities { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        [JsonConverter(typeof(InterfaceListConverter<IBankruptcyReportStatus, BankruptcyReportStatus>))]
        public List<IBankruptcyReportStatus> StatusHistory { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        [JsonConverter(typeof(InterfaceListConverter<IBankruptcyReportComment, BankruptcyReportComment>))]
        public List<IBankruptcyReportComment> Comments { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        [JsonConverter(typeof(InterfaceListConverter<IBankruptcyReportTrustee, BankruptcyReportTrustee>))]
        public List<IBankruptcyReportTrustee> Trustee { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        [JsonConverter(typeof(InterfaceListConverter<IBankruptcyReportAttorney, BankruptcyReportAttorney>))]
        public List<IBankruptcyReportAttorney> Attorneys { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        [JsonConverter(typeof(InterfaceListConverter<IBankruptcyReportDebtor, BankruptcyReportDebtor>))]
        public List<IBankruptcyReportDebtor> Debtors { get; set; }
    }
}