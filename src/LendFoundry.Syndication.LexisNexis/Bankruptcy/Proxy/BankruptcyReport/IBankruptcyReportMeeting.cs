﻿using LendFoundry.Foundation.Client;
using LendFoundry.Syndication.LexisNexis.BusinessReport;
using Newtonsoft.Json;

namespace LendFoundry.Syndication.LexisNexis.Bankruptcy.Proxy.BankruptcyReport
{

    /// <summary>
    /// lexisnexis
    /// </summary>
    public interface IBankruptcyReportMeeting
    {
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        [JsonConverter(typeof(InterfaceConverter<IDate, BusinessReport.Date>))]
        IDate Date { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string Time { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string Address { get; set; }
    }
}
