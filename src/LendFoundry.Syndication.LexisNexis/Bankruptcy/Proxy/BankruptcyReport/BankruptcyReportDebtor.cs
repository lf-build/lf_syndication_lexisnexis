using LendFoundry.Foundation.Client;
using LendFoundry.Syndication.LexisNexis.Bankruptcy.Proxy.BankruptcyReport.NonFCRAServiceReference;
using LendFoundry.Syndication.LexisNexis.BusinessReport;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.LexisNexis.Bankruptcy.Proxy.BankruptcyReport
{
    /// <summary>
    /// lexisnexis
    /// </summary>
    public class BankruptcyReportDebtor : IBankruptcyReportDebtor
    {
        /// <summary>
        /// lexisnexis
        /// </summary>
        public BankruptcyReportDebtor()
        {
        }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <param name="debtor"></param>
        public BankruptcyReportDebtor(ServiceReference.BankruptcyReport3Debtor debtor)
        {
            if (debtor == null)
                return;
            UniqueId = debtor.UniqueId;
            BusinessId = debtor.BusinessId;
            Ssn = debtor.SSN;
            AppendedSSN = debtor.AppendedSSN;
            TaxId = debtor.TaxId;
            AppendedTaxId = debtor.AppendedTaxId;
            Names = debtor.Names != null ? new List<IName>(debtor.Names.Select(name => new Name(name))) : null;
            Addresses = debtor.Addresses != null ? new List<IAddress>(debtor.Addresses.Select(address => new Address(address))) : null;
            Phones = debtor.Phones != null ? new List<IPhone>(debtor.Phones.Select(phone => new Phone(phone))) : null;
            DateTransferred = debtor.DateTransferred != null ? new BusinessReport.Date { Year = debtor.DateTransferred.Year, Day = debtor.DateTransferred.Day, Month = debtor.DateTransferred.Month } : null;
            ConvertedDate = debtor.ConvertedDate != null ? new BusinessReport.Date { Year = debtor.ConvertedDate.Year, Day = debtor.ConvertedDate.Day, Month = debtor.ConvertedDate.Month } : null;
        }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <param name="debtor"></param>
        public BankruptcyReportDebtor(BankruptcyPerson2 debtor)
        {
            if (debtor == null)
                return;
            UniqueId = debtor.UniqueId;
            BusinessId = debtor.BusinessId;
            Ssn = debtor.SSN;
            AppendedSSN = debtor.AppendedSSN;
            TaxId = debtor.TaxId;
            AppendedTaxId = debtor.AppendedTaxId;
            Names = debtor.Names != null ? new List<IName>(debtor.Names.Select(name => new Name(name))) : null;
            Addresses = debtor.Addresses != null ? new List<IAddress>(debtor.Addresses.Select(address => new Address(address))) : null;
            Phones = debtor.Phones != null ? new List<IPhone>(debtor.Phones.Select(phone => new Phone(phone))) : null;
            //DateTransferred = debtor.DateTransferred != null ? new BusinessReport.Date { Year = debtor.DateTransferred.Year, Day = debtor.DateTransferred.Day, Month = debtor.DateTransferred.Month } : null;
        }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string BusinessId { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string UniqueId { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string Ssn { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string AppendedSSN { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string TaxId { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string AppendedTaxId { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        [JsonConverter(typeof(InterfaceConverter<IDate, BusinessReport.Date>))]
        public IDate DateTransferred { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        [JsonConverter(typeof(InterfaceConverter<IDate, BusinessReport.Date>))]
        public IDate ConvertedDate { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        [JsonConverter(typeof(InterfaceListConverter<IName, Name>))]
        public List<IName> Names { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        [JsonConverter(typeof(InterfaceListConverter<IAddress, Address>))]
        public List<IAddress> Addresses { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        [JsonConverter(typeof(InterfaceListConverter<IPhone, Phone>))]
        public List<IPhone> Phones { get; set; }
    }
}