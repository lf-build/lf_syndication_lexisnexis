﻿using LendFoundry.Foundation.Client;
using LendFoundry.Syndication.LexisNexis.BusinessReport;
using Newtonsoft.Json;

namespace LendFoundry.Syndication.LexisNexis.Bankruptcy.Proxy.BankruptcyReport
{
    /// <summary>
    /// lexisnexis
    /// </summary>
    public interface IBankruptcyReportStatus
    {
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string Type { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>

        [JsonConverter(typeof(InterfaceConverter<IDate, BusinessReport.Date>))]
        IDate Date { get; set; }
    }
}
