﻿using System.Threading.Tasks;
using LendFoundry.Syndication.LexisNexis.Bankruptcy.Proxy.BankruptcyReport.NonFCRAServiceReference;
using LendFoundry.Syndication.LexisNexis.Bankruptcy.Proxy.BankruptcyReport.ServiceReference;
using LendFoundry.Syndication.LexisNexis.Bankruptcy.Proxy.BankruptcySearch.NonFCRAServiceReference;
using LendFoundry.Syndication.LexisNexis.Bankruptcy.Proxy.BankruptcySearch.ServiceReference;

namespace LendFoundry.Syndication.LexisNexis.Bankruptcy.Proxy {
    /// <summary>
    /// lexisnexis
    /// </summary>
    public interface IBankruptcyProxy {
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <param name="User"></param>
        /// <param name="SearchBy"></param>
        /// <param name="Options"></param>
        /// <returns></returns>
        Task<FcraBankruptcySearch3Response> BankruptcySearch (BankruptcySearch.ServiceReference.User User, FcraBankruptcySearch3By SearchBy, FcraBankruptcySearch3Option Options);
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <param name="user"></param>
        /// <param name="reportBy"></param>
        /// <param name="options"></param>
        /// <returns></returns>
        Task<FcraBankruptcyReport3Response> BankruptcyReport (BankruptcyReport.ServiceReference.User user, FcraBankruptcyReport3By reportBy, FcraBankruptcyReport3Option options);
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <param name="user"></param>
        /// <param name="searchBy"></param>
        /// <param name="options"></param>
        /// <returns></returns>
        Task<BankruptcySearch.NonFCRAServiceReference.BankruptcySearch2Response> NonFCRABankruptcySearch (BankruptcySearch.NonFCRAServiceReference.User user, BankruptcySearch2By searchBy, BankruptcySearch2Option options);
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <param name="user"></param>
        /// <param name="reportBy"></param>
        /// <param name="options"></param>
        /// <returns></returns>
        Task<BankruptcyReport.NonFCRAServiceReference.BankruptcyReport2Response> NonFCRABankruptcyReport (BankruptcyReport.NonFCRAServiceReference.User user, BankruptcyReport2By reportBy, BankruptcyReport2Option options);
    }
}