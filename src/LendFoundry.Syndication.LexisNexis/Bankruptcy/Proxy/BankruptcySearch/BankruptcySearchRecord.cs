﻿using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.LexisNexis.Bankruptcy
{
    /// <summary>
    /// lexisnexis
    /// </summary>
    public class BankruptcySearchRecord : IBankruptcySearchRecord
    {
        /// <summary>
        /// lexisnexis
        /// </summary>
        public BankruptcySearchRecord() { }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <param name="record"></param>
        public BankruptcySearchRecord(Proxy.BankruptcySearch.ServiceReference.BankruptcySearch3Record record)
        {

            if (record == null)
                return;
            Debtors = record.Debtors != null ? new List<IDebtor>(record.Debtors.Select(debtor => new Debtor(debtor))) : null;
            TMSId = record.TMSId;
            CaseNumber = record.CaseNumber;
        }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        [JsonConverter(typeof(InterfaceListConverter<IDebtor, Debtor>))]
        public List<IDebtor> Debtors { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string CaseNumber { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string TMSId { get; set; }
    }
}
