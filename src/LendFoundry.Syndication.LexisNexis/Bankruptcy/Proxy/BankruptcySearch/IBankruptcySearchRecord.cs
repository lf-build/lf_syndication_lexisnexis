﻿using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace LendFoundry.Syndication.LexisNexis.Bankruptcy
{
    /// <summary>
    /// lexisnexis
    /// </summary>
    public interface IBankruptcySearchRecord
    {
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        [JsonConverter(typeof(InterfaceListConverter<IDebtor,Debtor>))]
        List<IDebtor> Debtors { get; set; }
        
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string TMSId { get; set; }
    }
}