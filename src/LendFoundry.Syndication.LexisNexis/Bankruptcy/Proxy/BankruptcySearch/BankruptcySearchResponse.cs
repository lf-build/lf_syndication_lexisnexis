﻿using System.Collections.Generic;
using LendFoundry.Syndication.LexisNexis.Bankruptcy.Proxy.BankruptcySearch.ServiceReference;
using Newtonsoft.Json;
using LendFoundry.Foundation.Client;
using System.Linq;

namespace LendFoundry.Syndication.LexisNexis.Bankruptcy.Proxy
{
    /// <summary>
    /// lexisnexis
    /// </summary>
    public class BankruptcySearchResponse : IBankruptcySearchResponse
    {
        /// <summary>
        /// lexisnexis
        /// </summary>
        public BankruptcySearchResponse()
        {
        }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <param name="searchResponse"></param>
        public BankruptcySearchResponse(FcraBankruptcySearch3Response searchResponse)
        {
            if (searchResponse == null)
                return;
            Header = new ResponseHeader(searchResponse.Header);
            RecordCount = searchResponse.RecordCount;
            if (searchResponse.Records != null)
            {
                Records = new List<IBankruptcySearchRecord>(searchResponse.Records.Select(record => new BankruptcySearchRecord(record)));
            }
        }
        
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        [JsonConverter(typeof(InterfaceConverter<IResponseHeader, ResponseHeader>))]
        public IResponseHeader Header { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public int RecordCount { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        [JsonConverter(typeof(InterfaceListConverter<IBankruptcySearchRecord, BankruptcySearchRecord>))]
        public List<IBankruptcySearchRecord> Records { get; set; }
    }
}