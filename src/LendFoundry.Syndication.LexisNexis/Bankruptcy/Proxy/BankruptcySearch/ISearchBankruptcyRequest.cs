﻿namespace LendFoundry.Syndication.LexisNexis.Bankruptcy
{
    /// <summary>
    /// lexisnexis
    /// </summary>
    public interface ISearchBankruptcyRequest
    {
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string Ssn { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string FirstName { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string LastName { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string AddressLine1 { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string AddressLine2 { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string City { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string State { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string Zip4 { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string Zip5 { get; set; }
    }
}
