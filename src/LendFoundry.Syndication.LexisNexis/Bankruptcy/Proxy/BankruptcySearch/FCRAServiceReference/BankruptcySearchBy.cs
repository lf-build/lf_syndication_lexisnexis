﻿namespace LendFoundry.Syndication.LexisNexis.Bankruptcy.Proxy.BankruptcySearch.ServiceReference
{
    /// <summary>
    /// lexisnexis
    /// </summary>
    public partial class FcraBankruptcySearch3By
    {
        /// <summary>
        /// lexisnexis
        /// </summary>
        public FcraBankruptcySearch3By()
        { }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <param name="searchBy"></param>
        public FcraBankruptcySearch3By(ISearchBankruptcyRequest searchBy)
        {
            if (searchBy == null)
                return;

            SSN = searchBy.Ssn;
            Name = new Name
            {
                First = searchBy.FirstName,
                Last = searchBy.LastName
            };
            Address = new Address
            {
                StreetAddress1 = searchBy.AddressLine1,
                StreetAddress2 = searchBy.AddressLine2,
                City = searchBy.City,
                State = searchBy.State,
                Zip4 = searchBy.Zip4,
                Zip5 = searchBy.Zip5
            };
        }
    }
}

