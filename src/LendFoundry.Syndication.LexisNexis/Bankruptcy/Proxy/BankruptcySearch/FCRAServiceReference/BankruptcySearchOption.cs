﻿namespace LendFoundry.Syndication.LexisNexis.Bankruptcy.Proxy.BankruptcySearch.ServiceReference
{
    /// <summary>
    /// lexisnexis
    /// </summary>
    public partial class FcraBankruptcySearch3Option
    {      
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <param name="searchOption"></param>
        public FcraBankruptcySearch3Option(ISearchOption searchOption)
        {
            if (searchOption == null)
                return;
            ReturnCount = searchOption.ReturnCount;
            StartingRecord = searchOption.StartingRecord;
            FCRAPurpose = searchOption.FCRAPurpose;
            FCRAPurposeSpecified = true;           
        }
    }
}
