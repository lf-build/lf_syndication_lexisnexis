﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.LexisNexis.Bankruptcy
{
    /// <summary>
    /// lexisnexis
    /// </summary>
    public interface IBankruptcySearchResponse
    {
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        IResponseHeader Header { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        int RecordCount { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        List<IBankruptcySearchRecord> Records { get; set; }
    }
}
