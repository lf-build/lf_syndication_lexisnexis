﻿using System;
using System.Net;
using System.Threading.Tasks;
using LendFoundry.Syndication.LexisNexis.Bankruptcy.Proxy.BankruptcyReport.NonFCRAServiceReference;
using LendFoundry.Syndication.LexisNexis.Bankruptcy.Proxy.BankruptcyReport.ServiceReference;
using LendFoundry.Syndication.LexisNexis.Bankruptcy.Proxy.BankruptcySearch.NonFCRAServiceReference;
using LendFoundry.Syndication.LexisNexis.Bankruptcy.Proxy.BankruptcySearch.ServiceReference;

namespace LendFoundry.Syndication.LexisNexis.Bankruptcy.Proxy {
    /// <summary>
    /// lexisnexis
    /// </summary>
    public class BankruptcyProxy : IBankruptcyProxy {
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <param name="configuration"></param>
        public BankruptcyProxy (LexisNexisConfiguration configuration) {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            if (configuration.Bankruptcy == null)
                throw new ArgumentNullException (nameof (configuration.Bankruptcy));

            if (string.IsNullOrWhiteSpace (configuration.Bankruptcy.BankruptcyFCRAUrl))
                throw new ArgumentNullException (nameof (configuration.Bankruptcy.BankruptcyFCRAUrl));

            if (string.IsNullOrWhiteSpace (configuration.Bankruptcy.FCRAUserName))
                throw new ArgumentNullException (nameof (configuration.Bankruptcy.FCRAUserName));

            if (string.IsNullOrWhiteSpace (configuration.Bankruptcy.FCRAPassword))
                throw new ArgumentNullException (nameof (configuration.Bankruptcy.FCRAPassword));

            if (configuration.Bankruptcy.EndUser == null)
                throw new ArgumentNullException (nameof (configuration.Bankruptcy.EndUser));

            if (configuration.Bankruptcy.Options == null)
                throw new ArgumentNullException (nameof (configuration.Bankruptcy.Options));
            Configuration = configuration;
        }

        private LexisNexisConfiguration Configuration { get; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <param name="user"></param>
        /// <param name="searchBy"></param>
        /// <param name="options"></param>
        /// <returns></returns>
        public async Task<FcraBankruptcySearch3Response> BankruptcySearch (BankruptcySearch.ServiceReference.User user, FcraBankruptcySearch3By searchBy, FcraBankruptcySearch3Option options) {
            return await Task.Run (() => {
                if (searchBy == null)
                    throw new ArgumentNullException (nameof (searchBy));
                var soapClient = GetBankruptcySearchServiceClient ();
                var soapResponse = soapClient.BankruptcySearch3Async (new LendFoundry.Syndication.LexisNexis.Bankruptcy.Proxy.BankruptcySearch.ServiceReference.BankruptcySearch3Request (user, searchBy, options)).Result;
                return soapResponse.response;
            });
        }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <param name="user"></param>
        /// <param name="reportBy"></param>
        /// <param name="options"></param>
        /// <returns></returns>
        public async Task<FcraBankruptcyReport3Response> BankruptcyReport (BankruptcyReport.ServiceReference.User user, FcraBankruptcyReport3By reportBy, FcraBankruptcyReport3Option options) {
            if (reportBy == null)
                throw new ArgumentNullException (nameof (reportBy));
            var soapClient = GetBankruptcyReportServiceClient ();
            var soapResponse = await Task.Run(() => soapClient.BankruptcyReport3Async (new BankruptcyReport.ServiceReference.BankruptcyReport3Request (user, options, reportBy)).Result);
            return soapResponse.response;
        }
        private BankruptcySearch.ServiceReference.WsAccurintFCRAServiceSoap GetBankruptcySearchServiceClient () {
            BankruptcySearch.ServiceReference.WsAccurintFCRAServiceSoapClient BankruptcySearchSC = new BankruptcySearch.ServiceReference.WsAccurintFCRAServiceSoapClient (Proxy.BankruptcySearch.ServiceReference.WsAccurintFCRAServiceSoapClient.EndpointConfiguration.WsAccurintFCRAServiceSoap, Configuration.Bankruptcy.BankruptcyFCRAUrl);
            //BankruptcySearchSC.ClientCredentials.Windows.ClientCredential = new NetworkCredential (Configuration.Bankruptcy.FCRAUserName, Configuration.Bankruptcy.FCRAPassword);
            BankruptcySearchSC.ClientCredentials.UserName.UserName = Configuration.Bankruptcy.FCRAUserName;
            BankruptcySearchSC.ClientCredentials.UserName.Password = Configuration.Bankruptcy.FCRAPassword;
            return BankruptcySearchSC;
        }
        private BankruptcyReport.ServiceReference.WsAccurintFCRAServiceSoap GetBankruptcyReportServiceClient () {
            BankruptcyReport.ServiceReference.WsAccurintFCRAServiceSoapClient BankruptcyReportSC = new BankruptcyReport.ServiceReference.WsAccurintFCRAServiceSoapClient (Proxy.BankruptcyReport.ServiceReference.WsAccurintFCRAServiceSoapClient.EndpointConfiguration.WsAccurintFCRAServiceSoap, Configuration.Bankruptcy.BankruptcyFCRAUrl);
            //BankruptcyReportSC.ClientCredentials.Windows.ClientCredential = new NetworkCredential (Configuration.Bankruptcy.FCRAUserName, Configuration.Bankruptcy.FCRAPassword);
            BankruptcyReportSC.ClientCredentials.UserName.UserName = Configuration.Bankruptcy.FCRAUserName;
            BankruptcyReportSC.ClientCredentials.UserName.Password = Configuration.Bankruptcy.FCRAPassword;
            return BankruptcyReportSC;
        }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <param name="user"></param>
        /// <param name="searchBy"></param>
        /// <param name="options"></param>
        /// <returns></returns>
        public async Task<BankruptcySearch.NonFCRAServiceReference.BankruptcySearch2Response> NonFCRABankruptcySearch (BankruptcySearch.NonFCRAServiceReference.User user, BankruptcySearch2By searchBy, BankruptcySearch2Option options) {
            return await Task.Run (() => {
                if (searchBy == null)
                    throw new ArgumentNullException (nameof (searchBy));
                var soapClient = GetBankruptcyNonFCRASearchServiceClient ();
                var soapResponse = soapClient.BankruptcySearch2Async (new BankruptcySearch.NonFCRAServiceReference.BankruptcySearch2Request (user, searchBy, options)).Result;
                return soapResponse.response;
            });
        }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <param name="user"></param>
        /// <param name="reportBy"></param>
        /// <param name="options"></param>
        /// <returns></returns>
        public async Task<BankruptcyReport.NonFCRAServiceReference.BankruptcyReport2Response> NonFCRABankruptcyReport (BankruptcyReport.NonFCRAServiceReference.User user, BankruptcyReport2By reportBy, BankruptcyReport2Option options) {
            return await Task.Run (() => {
                if (reportBy == null)
                    throw new ArgumentNullException (nameof (reportBy));

                var soapClient = GetBankruptcyNonFCRAReportServiceClient ();

                var soapResponse = soapClient.BankruptcyReport2Async (new BankruptcyReport.NonFCRAServiceReference.BankruptcyReport2Request (user, options, reportBy)).Result;
                return soapResponse.response;
            });
        }
        private LendFoundry.Syndication.LexisNexis.Bankruptcy.Proxy.BankruptcySearch.NonFCRAServiceReference.WsAccurintServiceSoap GetBankruptcyNonFCRASearchServiceClient () {
            Proxy.BankruptcySearch.NonFCRAServiceReference.WsAccurintServiceSoapClient BankruptcyNonFCRASearchSC = new Proxy.BankruptcySearch.NonFCRAServiceReference.WsAccurintServiceSoapClient (Proxy.BankruptcySearch.NonFCRAServiceReference.WsAccurintServiceSoapClient.EndpointConfiguration.WsAccurintServiceSoap, Configuration.Bankruptcy.BankruptcyNonFCRAUrl);
            //BankruptcyNonFCRASearchSC.ClientCredentials.Windows.ClientCredential = new NetworkCredential (Configuration.Bankruptcy.NonFCRAUserName, Configuration.Bankruptcy.NonFCRAPassword);
            BankruptcyNonFCRASearchSC.ClientCredentials.UserName.UserName = Configuration.Bankruptcy.NonFCRAUserName;
            BankruptcyNonFCRASearchSC.ClientCredentials.UserName.Password = Configuration.Bankruptcy.NonFCRAPassword;
            return BankruptcyNonFCRASearchSC;
        }
        private LendFoundry.Syndication.LexisNexis.Bankruptcy.Proxy.BankruptcyReport.NonFCRAServiceReference.WsAccurintServiceSoap GetBankruptcyNonFCRAReportServiceClient () {
            BankruptcyReport.NonFCRAServiceReference.WsAccurintServiceSoapClient BankruptcyNonFCRAReportSC = new BankruptcyReport.NonFCRAServiceReference.WsAccurintServiceSoapClient (Proxy.BankruptcyReport.NonFCRAServiceReference.WsAccurintServiceSoapClient.EndpointConfiguration.WsAccurintServiceSoap, Configuration.Bankruptcy.BankruptcyNonFCRAUrl);
          //  BankruptcyNonFCRAReportSC.ClientCredentials.Windows.ClientCredential = new NetworkCredential (Configuration.Bankruptcy.NonFCRAUserName, Configuration.Bankruptcy.NonFCRAPassword);
            BankruptcyNonFCRAReportSC.ClientCredentials.UserName.UserName = Configuration.Bankruptcy.NonFCRAUserName;
            BankruptcyNonFCRAReportSC.ClientCredentials.UserName.Password = Configuration.Bankruptcy.NonFCRAPassword;
            return BankruptcyNonFCRAReportSC;
        }
    }
}