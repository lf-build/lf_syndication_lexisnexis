﻿
namespace LendFoundry.Syndication.LexisNexis.Bankruptcy
{
    /// <summary>
    /// lexisnexis
    /// </summary>
    public class Phone : IPhone
    {
        /// <summary>
        /// lexisnexis
        /// </summary>
        public Phone() { }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <param name="phone"></param>
        public Phone(Proxy.BankruptcyReport.ServiceReference.PhoneTimeZone phone)
        {
            if (phone == null)
                return;
            Phone10 = phone.Phone10;
            Fax = phone.Fax;
            TimeZone = phone.TimeZone;
        }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string Phone10 { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string Fax { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string TimeZone { get; set; }

    }
}
