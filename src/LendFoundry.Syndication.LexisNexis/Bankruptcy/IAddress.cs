﻿namespace LendFoundry.Syndication.LexisNexis.Bankruptcy
{
    /// <summary>
    /// lexisnexis
    /// </summary>
    public interface IAddress
    {
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string StreetNumber { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string StreetPreDirection { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string StreetName { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string StreetSuffix { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string StreetPostDirection { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string UnitDesignation { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string UnitNumber { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string StreetAddress1 { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string StreetAddress2 { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string City { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string State { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string Zip5 { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string Zip4 { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string County { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string PostalCode { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string StateCityZip { get; set; }
    }
}
