﻿using LendFoundry.Foundation.Client;
using LendFoundry.Syndication.LexisNexis.Bankruptcy.Proxy.BankruptcySearch.ServiceReference;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.LexisNexis.Bankruptcy
{
    /// <summary>
    /// lexisnexis
    /// </summary>
    public class Debtor : IDebtor
    {
        /// <summary>
        /// lexisnexis
        /// </summary>
        public Debtor()
        {
        }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <param name="debtorRedord"></param>
        public Debtor(BankruptcySearch3Debtor debtorRedord)
        {
            if (debtorRedord == null)
                return;

            TaxId = debtorRedord.TaxId;
            AppendedTaxId = debtorRedord.AppendedTaxId;
            UniqueId = debtorRedord.UniqueId;
            BusinessId = debtorRedord.BusinessId;
            SSN = debtorRedord.SSN;
            AppendedSSN = debtorRedord.AppendedSSN;
            AppendedTaxId = debtorRedord.AppendedTaxId;
            Names = debtorRedord.Names != null ? new List<IName>(debtorRedord.Names.Select(name => new Name(name))) : null;
            Addresses = debtorRedord.Addresses != null ? new List<IAddress>(debtorRedord.Addresses.Select(add => new Address(add))) : null;
        }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string TaxId { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string AppendedTaxId { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string UniqueId { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string BusinessId { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string SSN { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string AppendedSSN { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        [JsonConverter(typeof(InterfaceListConverter<IName, Name>))]
        public List<IName> Names { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        [JsonConverter(typeof(InterfaceListConverter<IAddress, Address>))]
        public List<IAddress> Addresses { get; set; }
    }
}