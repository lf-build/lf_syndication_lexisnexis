﻿using LendFoundry.Syndication.LexisNexis.Bankruptcy.Proxy.BankruptcySearch.ServiceReference;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace LendFoundry.Syndication.LexisNexis.Bankruptcy.Response
{
    /// <summary>
    /// lexisnexis
    /// </summary>
    [XmlRoot(ElementName = "Debtors")]
    public class BsDbtor
    {
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        [XmlElement(ElementName = "Debtors")]
        List<Debtor> Debtors { get; set; }
    }
    /// <summary>
    /// lexisnexis
    /// </summary>
    [XmlRoot(ElementName = "Debtor")]
    public class Debtor
    {
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        [XmlElement(ElementName = "TaxId")]
        public string TaxId { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        [XmlElement(ElementName = "AppendedTaxId")]
        public string AppendedTaxId { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        [XmlElement(ElementName = "UniqueId")]
        public string UniqueId { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        [XmlElement(ElementName = "BusinessId")]
        public string BusinessId { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        [XmlElement(ElementName = "SSN")]
        public string SSN { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        [XmlElement(ElementName = "AppendedSSN")]
        public string AppendedSSN { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        [XmlElement(ElementName = "Names")]
        public List<Name> Names { get; set; }
    }
    /// <summary>
    /// lexisnexis
    /// </summary>
    [XmlRoot(ElementName = "Name")]
    public class Name
    {
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        [XmlElement(ElementName = "Full")]
        public string Full { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        [XmlElement(ElementName = "First")]
        public string First { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        [XmlElement(ElementName = "Middle")]
        public string Middle { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        [XmlElement(ElementName = "Last")]
        public string Last { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        [XmlElement(ElementName = "Suffix")]
        public string Suffix { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        [XmlElement(ElementName = "Prefix")]
        public string Prefix { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        [XmlElement(ElementName = "CompanyName")]
        public string CompanyName { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        [XmlElement(ElementName = "Type")]
        public string Type { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        [XmlElement(ElementName = "UniqueId")]
        public string UniqueId { get; set; }
    }
    /// <summary>
    /// lexisnexi
    /// </summary>
    public class BankruptcySearchRecord
    {
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public List<Debtor> Debtors { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public bool AlsoFound { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string TMSId { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string AssetsForUnsecured { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string CaseNumber { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string CourtLocation { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string CourtName { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string Chapter { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string CourtCode { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string OriginalChapter { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string FilerType { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string FilingStatus { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string Disposition { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public List<IPhone> Phones { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public Date FilingDate { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public Date OriginalFilingDate { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public MatchedParty MatchedParty { get; set; }
    }

    /// <summary>
    /// lexisnexis
    /// </summary>
    [XmlRoot(ElementName = "Exceptions")]
    public class Exception
    {
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        [XmlElement(ElementName = "Item")]
        List<BSItem> Item { get; set; }
    }
    /// <summary>
    /// lexisnexi
    /// </summary>
    [XmlRoot(ElementName = "Item")]
    public class BSItem
    {
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        [XmlElement(ElementName = "Source")]
        public string Source { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        [XmlElement(ElementName = "Code")]
        public int Code { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        [XmlElement(ElementName = "Location")]
        public string Location { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        [XmlElement(ElementName = "Message")]
        public string Message { get; set; }

    }
    /// <summary>
    /// lexisnexis
    /// </summary>
    [XmlRoot(ElementName = "Header")]
    public class BSResponseHeader
    {
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        [XmlElement(ElementName = "Status")]
        public int Status { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        [XmlElement(ElementName = "Message")]
        public string Message { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        [XmlElement(ElementName = "QueryId")]
        public string QueryId { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        [XmlElement(ElementName = "TransactionId")]
        public string TransactionId { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        [XmlElement(ElementName = "Exceptions")]
        public List<Exception> Exceptions { get; set; }

    }
    /// <summary>
    /// lexisnexis
    /// </summary>
    [XmlRoot(ElementName = "response")]
    public class BSResponse
    {
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        [XmlElement(ElementName = "Header")]
        public BSResponseHeader Header { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        [XmlElement(ElementName = "RecordCount")]
        public int RecordCount { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        [XmlElement(ElementName = "Record")]
        public List<BankruptcySearchRecord> Records { get; set; }
    }
}
