﻿namespace LendFoundry.Syndication.LexisNexis.Bankruptcy
{
    /// <summary>
    /// lexisnexis
    /// </summary>
    public interface IMatchedParty
    {
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string PartyType { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string UniqueId { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        IName ParsedParty { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        IAddress Address { get; set; }
    }
}
