﻿using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.LexisNexis.Bankruptcy
{
    /// <summary>
    /// lexisnexis
    /// </summary>
    public class ResponseHeader : IResponseHeader
    {
        /// <summary>
        /// lexisnexis
        /// </summary>
        public ResponseHeader()
        {
        }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <param name="responseHeader"></param>
        public ResponseHeader(Proxy.BankruptcySearch.ServiceReference.ResponseHeader responseHeader)
        {
            if (responseHeader == null)
                return;
            Status = responseHeader.Status;
            Message = responseHeader.Message;
            QueryId = responseHeader.QueryId;
            TransactionId = responseHeader.TransactionId;
            if (responseHeader.Exceptions != null)
            {
                Exceptions = new List<IException>(responseHeader.Exceptions.Select(exception => new Exception(exception)));
            }
        }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <param name="responseHeader"></param>
        public ResponseHeader(Proxy.BankruptcyReport.ServiceReference.ResponseHeader responseHeader)
        {
            if (responseHeader == null)
                return;
            Status = responseHeader.Status;
            Message = responseHeader.Message;
            QueryId = responseHeader.QueryId;
            TransactionId = responseHeader.TransactionId;
            if (responseHeader.Exceptions != null)
            {
                Exceptions = new List<IException>(responseHeader.Exceptions.Select(exception => new Exception(exception)));
            }
        }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <param name="responseHeader"></param>
        public ResponseHeader(Proxy.BankruptcySearch.NonFCRAServiceReference.ResponseHeader responseHeader)
        {
            if (responseHeader == null)
                return;
            Status = responseHeader.Status;
            Message = responseHeader.Message;
            QueryId = responseHeader.QueryId;
            TransactionId = responseHeader.TransactionId;
            if (responseHeader.Exceptions != null)
            {
                Exceptions = new List<IException>(responseHeader.Exceptions.Select(exception => new Exception(exception)));
            }
        }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <param name="responseHeader"></param>
        public ResponseHeader(Proxy.BankruptcyReport.NonFCRAServiceReference.ResponseHeader responseHeader)
        {
            if (responseHeader == null)
                return;
            Status = responseHeader.Status;
            Message = responseHeader.Message;
            QueryId = responseHeader.QueryId;
            TransactionId = responseHeader.TransactionId;
            if (responseHeader.Exceptions != null)
            {
                Exceptions = new List<IException>(responseHeader.Exceptions.Select(exception => new Exception(exception)));
            }
        }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public int Status { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string Message { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string QueryId { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string TransactionId { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        [JsonConverter(typeof(InterfaceListConverter<IException, Exception>))]
        public List<IException> Exceptions { get; set; }
    }
}