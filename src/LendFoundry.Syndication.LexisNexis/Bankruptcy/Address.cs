﻿namespace LendFoundry.Syndication.LexisNexis.Bankruptcy
{
    /// <summary>
    /// lexisnexis
    /// </summary>
    public class Address : IAddress
    {
        /// <summary>
        /// lexisnexis
        /// </summary>
        public Address()
        { }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <param name="address"></param>
        public Address(Proxy.BankruptcySearch.ServiceReference.Address address)
        {
            if (address == null)
                return;
            StreetNumber = address.StreetNumber;
            StreetPreDirection = address.StreetPreDirection;
            StreetName = address.StreetName;
            StreetSuffix = address.StreetSuffix;
            StreetPostDirection = address.StreetPostDirection;
            UnitDesignation = address.UnitDesignation;
            UnitNumber = address.UnitNumber;
            StreetAddress1 = address.StreetAddress1;
            StreetAddress2 = address.StreetAddress2;
            City = address.City;
            State = address.State;
            Zip5 = address.Zip5;
            Zip4 = address.Zip4;
            County = address.County;
            PostalCode = address.PostalCode;
            StateCityZip = address.StateCityZip;
        }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <param name="address"></param>
        public Address(Proxy.BankruptcyReport.ServiceReference.Address address)
        {
            if (address == null)
                return;
            StreetNumber = address.StreetNumber;
            StreetPreDirection = address.StreetPreDirection;
            StreetName = address.StreetName;
            StreetSuffix = address.StreetSuffix;
            StreetPostDirection = address.StreetPostDirection;
            UnitDesignation = address.UnitDesignation;
            UnitNumber = address.UnitNumber;
            StreetAddress1 = address.StreetAddress1;
            StreetAddress2 = address.StreetAddress2;
            City = address.City;
            State = address.State;
            Zip5 = address.Zip5;
            Zip4 = address.Zip4;
            County = address.County;
            PostalCode = address.PostalCode;
            StateCityZip = address.StateCityZip;
        }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <param name="address"></param>
        public Address(Proxy.BankruptcyReport.NonFCRAServiceReference.AddressWithRawInfo address)
        {
            if (address == null)
                return;
            StreetNumber = address.StreetNumber;
            StreetPreDirection = address.StreetPreDirection;
            StreetName = address.StreetName;
            StreetSuffix = address.StreetSuffix;
            StreetPostDirection = address.StreetPostDirection;
            UnitDesignation = address.UnitDesignation;
            UnitNumber = address.UnitNumber;
            StreetAddress1 = address.StreetAddress1;
            StreetAddress2 = address.StreetAddress2;
            City = address.City;
            State = address.State;
            Zip5 = address.Zip5;
            Zip4 = address.Zip4;
            County = address.County;
            PostalCode = address.PostalCode;
            StateCityZip = address.StateCityZip;
        }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string StreetNumber { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
            public string StreetPreDirection { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string StreetName { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string StreetSuffix { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string StreetPostDirection { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string UnitDesignation { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string UnitNumber { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string StreetAddress1 { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string StreetAddress2 { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string City { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string State { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string Zip5 { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string Zip4 { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string County { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string PostalCode { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string StateCityZip { get; set; }
    }
}
