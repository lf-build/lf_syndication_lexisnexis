﻿namespace LendFoundry.Syndication.LexisNexis.Bankruptcy
{
    /// <summary>
    /// lexisnexis
    /// </summary>
    public class MatchedParty : IMatchedParty
    {
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string PartyType { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string UniqueId { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public IName ParsedParty { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public IAddress Address { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string OriginName { get; set; }
    }
}
