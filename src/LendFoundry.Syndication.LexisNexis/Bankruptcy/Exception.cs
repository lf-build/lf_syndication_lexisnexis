﻿namespace LendFoundry.Syndication.LexisNexis.Bankruptcy
{
    /// <summary>
    /// lexisnexis
    /// </summary>
    public class Exception : IException
    {
        /// <summary>
        /// lexisnexis
        /// </summary>
        public Exception()
        { }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <param name="wsException"></param>
        public Exception(Proxy.BankruptcySearch.ServiceReference.WsException wsException)
        {
            if (wsException == null)
                return;
            Source = wsException.Source;
            Code = wsException.Code;
            Location = wsException.Location;
            Message = wsException.Message;
        }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <param name="wsException"></param>
        public Exception(Proxy.BankruptcyReport.ServiceReference.WsException wsException)
        {
            if (wsException == null)
                return;
            Source = wsException.Source;
            Code = wsException.Code;
            Location = wsException.Location;
            Message = wsException.Message;
        }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <param name="wsException"></param>
        public Exception(Proxy.BankruptcySearch.NonFCRAServiceReference.WsException wsException)
        {
            if (wsException == null)
                return;
            Source = wsException.Source;
            Code = wsException.Code;
            Location = wsException.Location;
            Message = wsException.Message;
        }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <param name="wsException"></param>
        public Exception(Proxy.BankruptcyReport.NonFCRAServiceReference.WsException wsException)
        {
            if (wsException == null)
                return;
            Source = wsException.Source;
            Code = wsException.Code;
            Location = wsException.Location;
            Message = wsException.Message;
        }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string Source { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public int Code { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string Location { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string Message { get; set; }
    }
}
