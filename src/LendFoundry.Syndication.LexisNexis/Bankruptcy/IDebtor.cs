﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.LexisNexis.Bankruptcy
{
    /// <summary>
    /// lexisnexis
    /// </summary>
    public interface IDebtor
    {
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string TaxId { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string AppendedTaxId { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string UniqueId { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string BusinessId { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string SSN { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string AppendedSSN { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        List<IName> Names { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        List<IAddress> Addresses { get; set; }
    }
}
