﻿
namespace LendFoundry.Syndication.LexisNexis.Bankruptcy
{
    /// <summary>
    /// lexisnexis
    /// </summary>
    public class User : IUser
    {
        /// <summary>
        /// lexisnexis
        /// </summary>
        public User() { }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <param name="user"></param>
        public User(Proxy.BankruptcySearch.ServiceReference.User user)
        {
            if (user == null)
                return;

            ReferenceCode = user.ReferenceCode;
            BillingCode = user.BillingCode;
            QueryId = user.QueryId;
            GLBPurpose = user.GLBPurpose;
            DLPurpose = user.DLPurpose;
            //EndUser = new EndUserInfo(user.EndUser);
            MaxWaitSeconds = user.MaxWaitSeconds;
            AccountNumber = user.AccountNumber;
        }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string ReferenceCode { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string BillingCode { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string QueryId { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string GLBPurpose { get; set; } = "1";
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string DLPurpose { get; set; } = "3";
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public IEndUserInfo EndUser { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public int MaxWaitSeconds { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string AccountNumber { get; set; }
    }
}
