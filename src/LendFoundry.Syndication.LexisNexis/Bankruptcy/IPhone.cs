﻿namespace LendFoundry.Syndication.LexisNexis.Bankruptcy
{
    /// <summary>
    /// lexisnexis
    /// </summary>
    public interface IPhone
    {
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string Phone10 { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string Fax { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string TimeZone { get; set; }
    }
}
