﻿namespace LendFoundry.Syndication.LexisNexis.Bankruptcy
{
    /// <summary>
    /// lexisnexis
    /// </summary>
    public interface ISearchOption
    {
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        int FCRAPurpose { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        int ReturnCount { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        int StartingRecord { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        bool IncludeAllBankruptcies { get; set; }
    }
}
