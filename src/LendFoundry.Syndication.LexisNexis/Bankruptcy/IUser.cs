﻿using LendFoundry.Foundation.Client;
using Newtonsoft.Json;

namespace LendFoundry.Syndication.LexisNexis.Bankruptcy
{
    /// <summary>
    /// lexisnexis
    /// </summary>
    public interface IUser
    {
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string ReferenceCode { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string BillingCode { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string QueryId { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string GLBPurpose { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string DLPurpose { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        [JsonConverter(typeof(InterfaceConverter<IEndUserInfo,EndUserInfo>))]
        IEndUserInfo EndUser { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        int MaxWaitSeconds { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string AccountNumber { get; set; }
    }
}
