﻿
using LendFoundry.Syndication.LexisNexis.Bankruptcy.Proxy.BankruptcyReport.ServiceReference;
using LendFoundry.Syndication.LexisNexis.Bankruptcy.Proxy.BankruptcyReport.NonFCRAServiceReference;

namespace LendFoundry.Syndication.LexisNexis.Bankruptcy
{
    /// <summary>
    /// lexisnexis
    /// </summary>
    public class Name :IName
    {
        //commented as not used by Name class
        //private BankruptcySearch2Name name;

        /// <summary>
        /// lexisnexis
        /// </summary>
        public Name() { }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <param name="name"></param>
        public Name(Proxy.BankruptcySearch.ServiceReference.BankruptcySearch3Name name)
        {
            if (name == null)
                return;
            Full = name.Full;
            First = name.First;
            Middle = name.Middle;
            Last = name.Last;
            Suffix = name.Suffix;
            Prefix = name.Prefix;
            CompanyName = name.CompanyName;
            Type = name.Type;
            UniqueId = name.UniqueId;
        }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <param name="name"></param>
        public Name(Proxy.BankruptcyReport.ServiceReference.Name name)
        {
            if (name == null)
                return;
            Full = name.Full;
            First = name.First;
            Middle = name.Middle;
            Last = name.Last;
            Suffix = name.Suffix;
            Prefix = name.Prefix;
            
        }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <param name="name"></param>
        public Name(Proxy.BankruptcyReport.ServiceReference.BankruptcySearch3Name name)
        {
            if (name == null)
                return;
            Full = name.Full;
            First = name.First;
            Middle = name.Middle;
            Last = name.Last;
            Suffix = name.Suffix;
            Prefix = name.Prefix;
            CompanyName = name.CompanyName;
            Type = name.Type;
            UniqueId = name.UniqueId;

        }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <param name="name"></param>
        public Name(BankruptcyReport3Name name)
        {
            Full = name.Full;
            First = name.First;
            Middle = name.Middle;
            Last = name.Last;
            Suffix = name.Suffix;
            Prefix = name.Prefix;
            CompanyName = name.CompanyName;
            Type = name.Type;
            UniqueId = name.UniqueId;

        }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <param name="name"></param>
        public Name(Proxy.BankruptcyReport.NonFCRAServiceReference.BankruptcySearch2Name name)
        {
            Full = name.Full;
            First = name.First;
            Middle = name.Middle;
            Last = name.Last;
            Suffix = name.Suffix;
            Prefix = name.Prefix;
            CompanyName = name.CompanyName;
            Type = name.Type;
            //UniqueId = name.UniqueId;

        }
        
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string Full { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string First { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string Middle { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string Last { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string Suffix { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string Prefix { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string CompanyName { get; set; }

       
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string Type { get; set; }

       
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string UniqueId { get; set; }
    }
}
