﻿
namespace LendFoundry.Syndication.LexisNexis.Bankruptcy
{
    /// <summary>
    /// lexisnexis
    /// </summary>
    public class SearchOption : ISearchOption
    {
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public int FCRAPurpose { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public int ReturnCount { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public int StartingRecord { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public bool IncludeAllBankruptcies { get; set; }
    }
}
