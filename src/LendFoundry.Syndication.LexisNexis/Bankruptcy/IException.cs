﻿namespace LendFoundry.Syndication.LexisNexis.Bankruptcy
{
    /// <summary>
    /// lexisnexis
    /// </summary>
    public interface IException
    {
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string Source { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        int Code { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string Location { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string Message { get; set; }
    }
}
