﻿using LendFoundry.Syndication.LexisNexis;

namespace LendFoundry.Syndication.LexisNexis.LienJudgment
{
    /// <summary>
    /// lexisnexis
    /// </summary>
    public class User : IUser
    {
        /// <summary>
        /// lexisnexis
        /// </summary>
        public User() { }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <param name="user"></param>
        public User(LendFoundry.Syndication.LexisNexis.LienJudgment.Search.User user)
        {
            if (user == null)
                return;

            ReferenceCode = user.ReferenceCode;
            BillingCode = user.BillingCode;
            QueryId = user.QueryId;
           // GLBPurpose = user.GLBPurpose;
           //DLPurpose = user.DLPurpose;
            //EndUser = new EndUserInfo(user.EndUser);
            MaxWaitSeconds = user.MaxWaitSeconds;
            AccountNumber = user.AccountNumber;
        }

        /// <summary>
        /// lexisnexis
        /// </summary>
        public string ReferenceCode { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        public string BillingCode { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        public string QueryId { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        public string GLBPurpose { get; set; } = "1";
        /// <summary>
        /// lexisnexis
        /// </summary>
        public string DLPurpose { get; set; } = "3";
        /// <summary>
        /// lexisnexis
        /// </summary>
        public IEndUserInfo EndUser { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        public int MaxWaitSeconds { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        public string AccountNumber { get; set; }

        
    }
}
