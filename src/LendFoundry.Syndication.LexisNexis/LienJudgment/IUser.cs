﻿using LendFoundry.Foundation.Client;
using Newtonsoft.Json;

namespace LendFoundry.Syndication.LexisNexis.LienJudgment
{
    /// <summary>
    /// lexisnexis
    /// </summary>
    public interface IUser
    {
        /// <summary>
        /// lexisnexis
        /// </summary>
        string ReferenceCode { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        string BillingCode { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        string QueryId { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        string GLBPurpose { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        string DLPurpose { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        [JsonConverter(typeof(InterfaceConverter<IEndUserInfo, EndUserInfo>))]
        IEndUserInfo EndUser { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        int MaxWaitSeconds { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        string AccountNumber { get; set; }
    }
}
