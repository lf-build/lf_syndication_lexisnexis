﻿namespace LendFoundry.Syndication.LexisNexis.LienJudgment
{
    /// <summary>
    /// lexisnexis
    /// </summary>
    public class Name :IName
    {
        
        /// <summary>
        /// lexisnexis
        /// </summary>
        public Name() { }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <param name="name"></param>
        public Name(Search.Name name)
        {
            if (name == null)
                return;
            Full = name.Full;
            First = name.First;
            Middle = name.Middle;
            Last = name.Last;
            Suffix = name.Suffix;
            Prefix = name.Prefix;
           
        }
      
        

        /// <summary>
        /// lexisnexis
        /// </summary>
        public string Full { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        public string First { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        public string Middle { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        public string Last { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        public string Suffix { get; set; }
       
        /// <summary>
        /// lexisnexis
        /// </summary>
        public string Prefix { get; set; }

       
      
    }
}
