﻿

namespace LendFoundry.Syndication.LexisNexis.LienJudgment.Response.Report
{
    /// <summary>
    /// lexisnexis
    /// </summary>
    public class LienJudgmentFiling : ILienJudgmentFiling
    {
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <param name="filing"></param>
        public LienJudgmentFiling(LendFoundry.Syndication.LexisNexis.LienJudgment.Report.LienJudgmentFiling filing)
        {
            if (filing != null)
            {
                Number = filing.Number;
                Type = filing.Type;
                Date = new Date(filing.Date);
                OriginFilingDate = new Date(filing.OriginFilingDate);
                Book = filing.Book;
                Page = filing.Page;
                Agency = filing.Agency;
                AgencyCity = filing.AgencyCity;
                AgencyCounty = filing.AgencyCounty;
                AgencyState = filing.AgencyState;
                FilingStatus = filing.FilingStatus;
                FilingDescription = filing.FilingDescription;
                FilingTime = filing.FilingTime;
            }
        }
        
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string Number { get; set; }


        
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string Type { get; set; }

        
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public IDate Date { get; set; }


        
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public IDate OriginFilingDate { get; set; }


        
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string Book { get; set; }


        
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string Page { get; set; }


        
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string Agency { get; set; }


        
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string AgencyCity { get; set; }

        
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string AgencyState { get; set; }

        
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string AgencyCounty { get; set; }

        
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string FilingStatus { get; set; }


        
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string FilingDescription { get; set; }


        
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string FilingTime { get; set; }
    }
}
