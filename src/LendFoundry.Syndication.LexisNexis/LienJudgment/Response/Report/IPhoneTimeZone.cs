﻿

namespace LendFoundry.Syndication.LexisNexis.LienJudgment.Response.Report
{
    /// <summary>
    /// lexisnexis
    /// </summary>
    public interface IPhoneTimeZone
    {
        
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string Phone10 { get; set; }

        
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string Fax { get; set; }

        
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string TimeZone { get; set; }
    }
}
