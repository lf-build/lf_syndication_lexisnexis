﻿
using System.Collections.Generic;

namespace LendFoundry.Syndication.LexisNexis.LienJudgment.Response.Report
{
    /// <summary>
    /// lexisnexis
    /// </summary>
    public class LienJudgmentReportResponse : IFcraLienJudgmentReportResponse
    {
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <param name="response"></param>
        public LienJudgmentReportResponse(LendFoundry.Syndication.LexisNexis.LienJudgment.Report.LienJudgmentReportResponse response)
        {
            if (response == null)
                return;

            Header = new ResponseHeader(response.Header);
            if (response.LienJudgments != null)
            {
                List<ILienJudgmentReportRecord> models = new List<ILienJudgmentReportRecord>();
                foreach (LendFoundry.Syndication.LexisNexis.LienJudgment.Report.LienJudgmentReportRecord model in response.LienJudgments)
                {
                    models.Add(new LienJudgmentReportRecord(model));
                }
                LienJudgments = models.ToArray();
            }
           

        }

        /// <summary>
        /// lexisnexis
        /// </summary>
        public IResponseHeader Header { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        public ILienJudgmentReportRecord[] LienJudgments { get; set; }

    }
}
