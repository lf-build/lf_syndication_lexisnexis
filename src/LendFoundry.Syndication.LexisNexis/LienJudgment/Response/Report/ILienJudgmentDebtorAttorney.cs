﻿

namespace LendFoundry.Syndication.LexisNexis.LienJudgment.Response.Report
{
    /// <summary>
    /// lexisnexis
    /// </summary>
    public interface ILienJudgmentDebtorAttorney
    {

        
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string Name { get; set; }


        
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        IAddress Address { get; set; }


        
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string Phone { get; set; }


        
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string TimeZone { get; set; }


        

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        IAddress[] Addresses { get; set; }


        

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        IPhoneTimeZone[] Phones { get; set; }


        

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        ILienJudgmentParty[] ParsedParties { get; set; }
    }
}
