﻿

namespace LendFoundry.Syndication.LexisNexis.LienJudgment.Response.Report
{
    /// <summary>
    /// lexisnexis
    /// </summary>
    public class PhoneTimeZone : IPhoneTimeZone
    {
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <param name="phone"></param>
        public PhoneTimeZone(LendFoundry.Syndication.LexisNexis.LienJudgment.Report.PhoneTimeZone phone)
        {
            if (phone != null)
            {
                Phone10 = phone.Phone10;
                Fax = phone.Fax;
                TimeZone = phone.TimeZone;
            }

        }
        
        /// <summary>
        /// lexisnexis
        /// </summary>
        public string Phone10 { get; set; }
        
        
        /// <summary>
        /// lexisnexis
        /// </summary>
        public string Fax { get; set; }
        
        
        /// <summary>
        /// lexisnexis
        /// </summary>
        public string TimeZone { get; set; }
       
    }
}
