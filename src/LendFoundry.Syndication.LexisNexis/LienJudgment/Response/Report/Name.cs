﻿

namespace LendFoundry.Syndication.LexisNexis.LienJudgment.Response.Report
{
    /// <summary>
    /// lexisnexis
    /// </summary>
    public class Name : IName
    {
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <param name="name"></param>
        public Name(LendFoundry.Syndication.LexisNexis.LienJudgment.Report.Name name)
        {
            if (name != null)
            {
                Full = name.Full;
                First = name.First;
                Middle = name.Middle;
                Last = name.Last;
                Suffix = name.Suffix;
                Prefix = name.Prefix;
            }

        }

        /// <summary>
        /// lexisnexis
        /// </summary>
        public string Full { get; set; }


        /// <summary>
        /// lexisnexis
        /// </summary>
        public string First { get; set; }


        /// <summary>
        /// lexisnexis
        /// </summary>
        public string Middle { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        public string Last { get; set; }


        /// <summary>
        /// lexisnexis
        /// </summary>
        public string Suffix { get; set; }


        /// <summary>
        /// lexisnexis
        /// </summary>
        public string Prefix { get; set; }

    }
}
