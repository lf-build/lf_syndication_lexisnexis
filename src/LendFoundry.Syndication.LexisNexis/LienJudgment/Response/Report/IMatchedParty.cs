﻿

namespace LendFoundry.Syndication.LexisNexis.LienJudgment.Response.Report
{
    /// <summary>
    /// lexisnexis
    /// </summary>
    public interface IMatchedParty
    {

        
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string PartyType { get; set; }


        
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string UniqueId { get; set; }


        
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string OriginName { get; set; }


        
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        INameAndCompany ParsedParty { get; set; }


        
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        IAddress Address { get; set; }
    }
}
