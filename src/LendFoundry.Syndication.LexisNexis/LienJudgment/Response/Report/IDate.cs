﻿
namespace LendFoundry.Syndication.LexisNexis.LienJudgment.Response.Report
{
    /// <summary>
    /// lexisnexis
    /// </summary>
    public interface IDate
    {
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        short Year { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        bool YearSpecified { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        short Month { get; set; }


        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        bool MonthSpecified { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        short Day { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        bool DaySpecified { get; set; }
    }
}
