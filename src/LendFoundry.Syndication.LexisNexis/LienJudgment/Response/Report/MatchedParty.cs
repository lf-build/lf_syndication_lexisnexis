﻿

namespace LendFoundry.Syndication.LexisNexis.LienJudgment.Response.Report
{
    /// <summary>
    /// lexisnexis
    /// </summary>
    public class MatchedParty : IMatchedParty
    {
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <param name="matchedParty"></param>
        public MatchedParty(LendFoundry.Syndication.LexisNexis.LienJudgment.Report.MatchedParty matchedParty)
        {
            if (matchedParty != null)
            {
                PartyType = matchedParty.PartyType;
                UniqueId = matchedParty.UniqueId;
                OriginName = matchedParty.OriginName;
                ParsedParty = new NameAndCompany(matchedParty.ParsedParty);
                Address = new Address(matchedParty.Address);
            }
        }
        
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string PartyType { get; set; }
       

        
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string UniqueId { get; set; }
       

        
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string OriginName { get; set; } 
      

        
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public INameAndCompany ParsedParty { get; set; }  
        

        
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public IAddress Address { get; set; }
    }
}
