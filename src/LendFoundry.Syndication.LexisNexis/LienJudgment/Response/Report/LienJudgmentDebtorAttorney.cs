﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.LexisNexis.LienJudgment.Response.Report
{
    /// <summary>
    /// lexisnexis
    /// </summary>
    public class LienJudgmentDebtorAttorney: ILienJudgmentDebtorAttorney
    {
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <param name="debtorAttorney"></param>
        public LienJudgmentDebtorAttorney(LendFoundry.Syndication.LexisNexis.LienJudgment.Report.LienJudgmentDebtorAttorney debtorAttorney)
        {
            if (debtorAttorney != null)
            {
                Name = debtorAttorney.Name;
                Phone = debtorAttorney.Phone;
                TimeZone = debtorAttorney.TimeZone;
                
                Address = new Address(debtorAttorney.Address);
                if (debtorAttorney.Addresses != null)
                {
                    List<IAddress> creditorAddress = new List<IAddress>();
                    foreach (LendFoundry.Syndication.LexisNexis.LienJudgment.Report.Address address in debtorAttorney.Addresses)
                    {
                        creditorAddress.Add(new Address(address));
                    }
                    Addresses = creditorAddress.ToArray();
                }
                if (debtorAttorney.Phones != null)
                {
                    List<IPhoneTimeZone> phoneTimeZone = new List<IPhoneTimeZone>();
                    foreach (LendFoundry.Syndication.LexisNexis.LienJudgment.Report.PhoneTimeZone phone in debtorAttorney.Phones)
                    {
                        phoneTimeZone.Add(new PhoneTimeZone(phone));
                    }
                    Phones = phoneTimeZone.ToArray();
                }
                if (debtorAttorney.ParsedParties != null)
                {
                    List<ILienJudgmentParty> lienJudgmentParties = new List<ILienJudgmentParty>();
                    foreach (LendFoundry.Syndication.LexisNexis.LienJudgment.Report.LienJudgmentParty lienparty in debtorAttorney.ParsedParties)
                    {
                        lienJudgmentParties.Add(new LienJudgmentParty(lienparty));
                    }
                    ParsedParties = lienJudgmentParties.ToArray();
                }
            }
        }
        
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string Name { get; set; }
        

        
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public IAddress Address { get; set; }
       

        
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string Phone { get; set; }
        

        
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string TimeZone { get; set; }
        

        
       
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public IAddress[] Addresses { get; set; }
        

        
        
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public IPhoneTimeZone[] Phones { get; set; }
       

        
        
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public ILienJudgmentParty[] ParsedParties { get; set; }
    }
}
