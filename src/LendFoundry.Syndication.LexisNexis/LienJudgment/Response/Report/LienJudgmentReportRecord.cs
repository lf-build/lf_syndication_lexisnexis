﻿
using System.Collections.Generic;

namespace LendFoundry.Syndication.LexisNexis.LienJudgment.Response.Report
{
    /// <summary>
    /// lexisnexis
    /// </summary>
    public class LienJudgmentReportRecord : ILienJudgmentReportRecord
    {
        /// <summary>
        /// leixsnexis
        /// </summary>
        /// <param name="model"></param>
        public LienJudgmentReportRecord(LendFoundry.Syndication.LexisNexis.LienJudgment.Report.LienJudgmentReportRecord model)
        {

            if (model != null)
            {
                BusinessIds = new BusinessIdentity(model.BusinessIds);
                IdValue = model.IdValue;
                RMSId = model.RMSId;
                OriginFilingTime = model.OriginFilingTime;
                TaxCode = model.TaxCode;
                Judge = model.Judge;
                ExternalKey = model.ExternalKey;
                TMSId = model.TMSId;
                MatchedParty = new MatchedParty(model.MatchedParty);
                IRSSerialNumber = model.IRSSerialNumber;
                OriginFilingNumber = model.OriginFilingNumber;
                OriginFilingType = model.OriginFilingType;
                OriginFilingDate = new Date(model.OriginFilingDate);
                CaseNumber = model.CaseNumber;
                Eviction = model.Eviction;
                Amount = model.Amount;
                FilingJurisdiction = model.FilingJurisdiction;
                FilingJurisdictionName = model.FilingJurisdictionName;
                FilingState = model.FilingState;
                FilingStatus = model.FilingStatus;
                CertificateNumber = model.CertificateNumber;
                MultipleDefendant = model.MultipleDefendant;
                JudgeSatisfiedDate = new Date(model.JudgeSatisfiedDate);
                SuitDate = new Date(model.SuitDate);
                JudgeVacatedDate = new Date(model.JudgeVacatedDate);
                ReleaseDate = new Date(model.ReleaseDate);
                LegalLot = model.LegalLot;
                LegalBlock = model.LegalBlock;
                if (model.Debtors != null)
                {
                    List<ILienJudgmentDebtor> debtorResponse = new List<ILienJudgmentDebtor>();
                    foreach (LendFoundry.Syndication.LexisNexis.LienJudgment.Report.LienJudgmentDebtor debtor in model.Debtors)
                    {
                        debtorResponse.Add(new LienJudgmentDebtor(debtor));
                    }
                    Debtors = debtorResponse.ToArray();
                }
                if (model.ThirdParties != null)
                {
                    List<ILienJudgmentThirdParty> thirdPartiesResponse = new List<ILienJudgmentThirdParty>();
                    foreach (LendFoundry.Syndication.LexisNexis.LienJudgment.Report.LienJudgmentThirdParty thirdParty in model.ThirdParties)
                    {
                        thirdPartiesResponse.Add(new LienJudgmentThirdParty(thirdParty));
                    }
                    ThirdParties = thirdPartiesResponse.ToArray();
                }
                if (model.Creditors != null)
                {
                    List<ILienJudgmentCreditor> creditorssResponse = new List<ILienJudgmentCreditor>();
                    foreach (LendFoundry.Syndication.LexisNexis.LienJudgment.Report.LienJudgmentCreditor creditor in model.Creditors)
                    {
                        creditorssResponse.Add(new LienJudgmentCreditor(creditor));
                    }
                    Creditors = creditorssResponse.ToArray();
                }
                if (model.DebtorAttorneys != null)
                {
                    List<ILienJudgmentDebtorAttorney> debtorAttorneyResponse = new List<ILienJudgmentDebtorAttorney>();
                    foreach (LendFoundry.Syndication.LexisNexis.LienJudgment.Report.LienJudgmentDebtorAttorney debtorAttorney in model.DebtorAttorneys)
                    {
                        debtorAttorneyResponse.Add(new LienJudgmentDebtorAttorney(debtorAttorney));
                    }
                    DebtorAttorneys = debtorAttorneyResponse.ToArray();
                }
                if (model.Filings != null)
                {
                    List<ILienJudgmentFiling> filingsResponse = new List<ILienJudgmentFiling>();
                    foreach (LendFoundry.Syndication.LexisNexis.LienJudgment.Report.LienJudgmentFiling filing in model.Filings)
                    {
                        filingsResponse.Add(new LienJudgmentFiling(filing));
                    }
                    Filings = filingsResponse.ToArray();
                }



            }

        }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public IBusinessIdentity BusinessIds { get; set; }

        
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string IdValue { get; set; }

        
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string RMSId { get; set; }

        
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string OriginFilingTime { get; set; }

        
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string TaxCode { get; set; }


        
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string Judge { get; set; }


        
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string ExternalKey { get; set; }


        
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string TMSId { get; set; }


        
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public IMatchedParty MatchedParty { get; set; }

        
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string IRSSerialNumber { get; set; }


        
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string OriginFilingNumber { get; set; }

        
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string OriginFilingType { get; set; }


        
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public IDate OriginFilingDate { get; set; }


        
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string CaseNumber { get; set; }


        
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string Eviction { get; set; }


        
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string Amount { get; set; }


        
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string FilingJurisdiction { get; set; }


        
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string FilingJurisdictionName { get; set; }


        
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string FilingState { get; set; }


        
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string FilingStatus { get; set; }


        
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string CertificateNumber { get; set; }


        
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string MultipleDefendant { get; set; }


        
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public IDate JudgeSatisfiedDate { get; set; }


        
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public IDate SuitDate { get; set; }

        
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public IDate JudgeVacatedDate { get; set; }


        
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public IDate ReleaseDate { get; set; }


        
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string LegalLot { get; set; }


        
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string LegalBlock { get; set; }


        

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public ILienJudgmentDebtor[] Debtors { get; set; }


        

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public ILienJudgmentThirdParty[] ThirdParties { get; set; }


        

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public ILienJudgmentCreditor[] Creditors { get; set; }


        

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public ILienJudgmentDebtorAttorney[] DebtorAttorneys { get; set; }

        

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public ILienJudgmentFiling[] Filings { get; set; }

    }
}
