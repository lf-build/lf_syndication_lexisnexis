﻿

namespace LendFoundry.Syndication.LexisNexis.LienJudgment.Response.Report
{   
    /// <summary>
    /// lexisnexis
    /// </summary>
    public class BusinessIdentity : IBusinessIdentity
    {
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <param name="businessIds"></param>
        public BusinessIdentity(LendFoundry.Syndication.LexisNexis.LienJudgment.Report.BusinessIdentity businessIds)
        {
            DotID = businessIds.DotID;
            DotIDSpecified = businessIds.DotIDSpecified;
            EmpID = businessIds.EmpID;
            EmpIDSpecified = businessIds.EmpIDSpecified;
            POWID = businessIds.POWID;
            POWIDSpecified = businessIds.POWIDSpecified;
            ProxID = businessIds.ProxID;
            ProxIDSpecified = businessIds.ProxIDSpecified;
            SeleID = businessIds.SeleID;
            SeleIDSpecified = businessIds.SeleIDSpecified;
            OrgID = businessIds.OrgID;
            OrgIDSpecified = businessIds.OrgIDSpecified;
            UltID = businessIds.UltID;
            UltIDSpecified = businessIds.UltIDSpecified;
        }

        /// <summary>
        /// lexisnexis
        /// </summary>
        public long DotID { get; set; }
       

        /// <summary>
        /// lexisnexis
        /// </summary>
       
        public bool DotIDSpecified { get; set; }
        

        /// <summary>
        /// lexisnexis
        /// </summary>
        public long EmpID { get; set; }
    

        /// <summary>
        /// lexisnexis
        /// </summary>
        public bool EmpIDSpecified { get; set; }
       

        /// <summary>
        /// lexisnexis
        /// </summary>
        public long POWID { get; set; }
      
        /// <summary>
        /// lexisnexis
        /// </summary>
        public bool POWIDSpecified { get; set; }
       

        /// <summary>
        /// lexisnexis
        /// </summary>
        public long ProxID { get; set; }
       

        /// <summary>
        /// lexisnexis
        /// </summary>
        public bool ProxIDSpecified { get; set; }
        

        /// <summary>
        /// lexisnexis
        /// </summary>
        public long SeleID { get; set; }
       
        /// <summary>
        /// lexisnexis
        /// </summary>
        public bool SeleIDSpecified { get; set; }
       
        /// <summary>
        /// lexisnexis
        /// </summary>
        public long OrgID { get; set; }
      
        /// <summary>
        /// lexisnexis
        /// </summary>
        public bool OrgIDSpecified { get; set; }
       
        /// <summary>
        /// lexisnexis
        /// </summary>
        public long UltID { get; set; }
        

        /// <summary>
        /// lexisnexis
        /// </summary>
        public bool UltIDSpecified { get; set; }
    }
}
