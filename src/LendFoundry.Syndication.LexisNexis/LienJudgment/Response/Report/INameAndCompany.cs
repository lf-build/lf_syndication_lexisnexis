﻿

namespace LendFoundry.Syndication.LexisNexis.LienJudgment.Response.Report
{
    /// <summary>
    /// lexisnexis
    /// </summary>
    public interface INameAndCompany
    {
        
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string Full { get; set; }

        
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string First { get; set; }


        
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string Middle { get; set; }


        
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string Last { get; set; }


        
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string Suffix { get; set; }


        
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string Prefix { get; set; }


        
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string CompanyName { get; set; }
    }
}
