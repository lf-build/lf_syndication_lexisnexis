﻿

using System.Collections.Generic;

namespace LendFoundry.Syndication.LexisNexis.LienJudgment.Response.Report
{
    /// <summary>
    /// lexisnexis
    /// </summary>
    public class LienJudgmentCreditor : ILienJudgmentCreditor
    {
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <param name="creditor"></param>
        public LienJudgmentCreditor(LendFoundry.Syndication.LexisNexis.LienJudgment.Report.LienJudgmentCreditor creditor)
        {
            if (creditor != null)
            {
                Name = creditor.Name;
                Address = new Address(creditor.Address);
                if (creditor.Addresses != null)
                {
                    List<IAddress> creditorAddress = new List<IAddress>();
                    foreach (LendFoundry.Syndication.LexisNexis.LienJudgment.Report.Address address in creditor.Addresses)
                    {
                        creditorAddress.Add(new Address(address));
                    }
                    Addresses = creditorAddress.ToArray();
                }
                if (creditor.Phones != null)
                {
                    List<IPhoneTimeZone> phoneTimeZone = new List<IPhoneTimeZone>();
                    foreach (LendFoundry.Syndication.LexisNexis.LienJudgment.Report.PhoneTimeZone phone in creditor.Phones)
                    {
                        phoneTimeZone.Add(new PhoneTimeZone(phone));
                    }
                    Phones = phoneTimeZone.ToArray();
                }
                if (creditor.ParsedParties != null)
                {
                    List<ILienJudgmentParty> lienJudgmentParties = new List<ILienJudgmentParty>();
                    foreach (LendFoundry.Syndication.LexisNexis.LienJudgment.Report.LienJudgmentParty lienparty in creditor.ParsedParties)
                    {
                        lienJudgmentParties.Add(new LienJudgmentParty(lienparty));
                    }
                    ParsedParties = lienJudgmentParties.ToArray();
                }
            }

        }
        
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string Name { get; set; }


        
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public IAddress Address { get; set; }


        

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public IAddress[] Addresses { get; set; }


        

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public IPhoneTimeZone[] Phones { get; set; }


        

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public ILienJudgmentParty[] ParsedParties { get; set; }
    }
}
