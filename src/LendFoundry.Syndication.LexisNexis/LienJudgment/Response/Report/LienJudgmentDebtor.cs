﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.LexisNexis.LienJudgment.Response.Report
{
    /// <summary>
    /// lexisnexis
    /// </summary>
    public class LienJudgmentDebtor : ILienJudgmentDebtor
    {
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <param name="debtor"></param>
        public LienJudgmentDebtor(LendFoundry.Syndication.LexisNexis.LienJudgment.Report.LienJudgmentDebtor debtor)
        {
            if (debtor != null)
            {
                OriginName = debtor.OriginName;
                if (debtor.Addresses != null)
                {
                    List<IAddress> debtorAddress = new List<IAddress>();
                    foreach (LendFoundry.Syndication.LexisNexis.LienJudgment.Report.Address address in debtor.Addresses)
                    {
                        debtorAddress.Add(new Address(address));
                    }
                    Addresses = debtorAddress.ToArray();
                }
                if (debtor.Phones != null)
                {
                    List<IPhoneTimeZone> phoneTimeZone = new List<IPhoneTimeZone>();
                    foreach (LendFoundry.Syndication.LexisNexis.LienJudgment.Report.PhoneTimeZone phone in debtor.Phones)
                    {
                        phoneTimeZone.Add(new PhoneTimeZone(phone));
                    }
                    Phones = phoneTimeZone.ToArray();
                }
                if (debtor.ParsedParties != null)
                {
                    List<ILienJudgmentParty> lienJudgmentParties = new List<ILienJudgmentParty>();
                    foreach (LendFoundry.Syndication.LexisNexis.LienJudgment.Report.LienJudgmentParty lienparty in debtor.ParsedParties)
                    {
                        lienJudgmentParties.Add(new LienJudgmentParty(lienparty));
                    }
                    ParsedParties = lienJudgmentParties.ToArray();
                }
            }
        }

        
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string OriginName { get; set; }

        

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public IAddress[] Addresses { get; set; }


        

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public IPhoneTimeZone[] Phones { get; set; }


        

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public ILienJudgmentParty[] ParsedParties { get; set; }
    }
}
