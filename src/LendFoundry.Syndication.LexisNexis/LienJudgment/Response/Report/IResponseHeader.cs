﻿

namespace LendFoundry.Syndication.LexisNexis.LienJudgment.Response.Report
{
    /// <summary>
    /// lexisnexis
    /// </summary>
    public interface IResponseHeader
    {

        
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        int Status { get; set; }

        

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        bool StatusSpecified { get; set; }


        
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string Message { get; set; }


        
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string QueryId { get; set; }


        
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string TransactionId { get; set; }


               
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        IWsException[] Exceptions { get; set; }
    }
}
