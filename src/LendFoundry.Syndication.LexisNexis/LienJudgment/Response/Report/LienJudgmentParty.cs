﻿

namespace LendFoundry.Syndication.LexisNexis.LienJudgment.Response.Report
{
    /// <summary>
    /// lexisnexis
    /// </summary>
    public class LienJudgmentParty: ILienJudgmentParty
    {
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <param name="lienparty"></param>
        public LienJudgmentParty(LendFoundry.Syndication.LexisNexis.LienJudgment.Report.LienJudgmentParty lienparty)
        {
            if (lienparty != null)
            {
                BusinessIds = new BusinessIdentity(lienparty.BusinessIds);
                IdValue = lienparty.IdValue;
                Name = new Name(lienparty.Name);
                CompanyName = lienparty.CompanyName;
                UniqueId = lienparty.UniqueId;
                BusinessId = lienparty.BusinessId;
                SSN = lienparty.SSN;
                FEIN = lienparty.FEIN;
                PersonFilterId = lienparty.PersonFilterId;
                TaxId = lienparty.TaxId;
            }
        }

        
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public IBusinessIdentity BusinessIds { get; set; }
       

        
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string IdValue { get; set; }
        

        
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public IName Name { get; set; }
        
        
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string CompanyName { get; set; }
       

        
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string UniqueId { get; set; }
        

        
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string BusinessId { get; set; }       
        

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string SSN { get; set; }
        

        
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string FEIN { get; set; }
       
        
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string PersonFilterId { get; set; }
        

        
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string TaxId { get; set; }
       
    }
}
