﻿

namespace LendFoundry.Syndication.LexisNexis.LienJudgment.Response.Report
{
    /// <summary>
    /// lexisnexis
    /// </summary>
    public interface IBusinessIdentity
    {

        /// <summary>
        /// lexisnexis
        /// </summary>
        long DotID { get; set; }


        /// <summary>
        /// lexisnexis
        /// </summary>
        bool DotIDSpecified { get; set; }


        /// <summary>
        /// lexisnexis
        /// </summary>
        long EmpID { get; set; }


        /// <summary>
        /// lexisnexis
        /// </summary>
        bool EmpIDSpecified { get; set; }


        /// <summary>
        /// lexisnexis
        /// </summary>
        long POWID { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        bool POWIDSpecified { get; set; }


        /// <summary>
        /// lexisnexis
        /// </summary>
        long ProxID { get; set; }


        /// <summary>
        /// lexisnexis
        /// </summary>

        bool ProxIDSpecified { get; set; }


        /// <summary>
        /// lexisnexis
        /// </summary>
        long SeleID { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        bool SeleIDSpecified { get; set; }


        /// <summary>
        /// lexisnexis
        /// </summary>
        long OrgID { get; set; }


        /// <summary>
        /// lexisnexis
        /// </summary>
        bool OrgIDSpecified { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        long UltID { get; set; }


        /// <summary>
        /// lexisnexis
        /// </summary>
        bool UltIDSpecified { get; set; }
    }
}
