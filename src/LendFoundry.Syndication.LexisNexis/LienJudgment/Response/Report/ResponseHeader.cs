﻿
using System.Collections.Generic;

namespace LendFoundry.Syndication.LexisNexis.LienJudgment.Response.Report
{
    /// <summary>
    /// lexisnexis
    /// </summary>
    public class ResponseHeader : IResponseHeader
    {
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <param name="header"></param>
        public ResponseHeader(LendFoundry.Syndication.LexisNexis.LienJudgment.Report.ResponseHeader header)
        {
            if (header == null)
                return;

            Status = header.Status;
            Message = header.Message;
            QueryId = header.QueryId;
            TransactionId = header.TransactionId;

            if (header.Exceptions != null)
            {
                List<IWsException> exceptions = new List<IWsException>();
                foreach (LendFoundry.Syndication.LexisNexis.LienJudgment.Report.WsException exception in header.Exceptions)
                {
                    exceptions.Add(new WsException(exception));
                }

                Exceptions = exceptions.ToArray();
            }

        }
        
        /// <summary>
        /// lexisnexis
        /// </summary>
        public int Status { get; set; }

        

        /// <summary>
        /// lexisnexis
        /// </summary>
        public bool StatusSpecified { get; set; }


        
        /// <summary>
        /// lexisnexis
        /// </summary>
        public string Message { get; set; }


        
        /// <summary>
        /// lexisnexis
        /// </summary>
        public string QueryId { get; set; }


        
        /// <summary>
        /// lexisnexis
        /// </summary>
        public string TransactionId { get; set; }


               
        /// <summary>
        /// lexisnexis
        /// </summary>
        public IWsException[] Exceptions { get; set; }

    }
}
