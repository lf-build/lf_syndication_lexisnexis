﻿

namespace LendFoundry.Syndication.LexisNexis.LienJudgment.Response.Report
{
    /// <summary>
    /// lexisnexis
    /// </summary>
    public interface IWsException
    {
        
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string Source { get; set; }


        
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        int Code { get; set; }


        

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        bool CodeSpecified { get; set; }


        
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string Location { get; set; }


        
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string Message { get; set; }
    }
}
