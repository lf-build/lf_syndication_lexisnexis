﻿
using System.Collections.Generic;

namespace LendFoundry.Syndication.LexisNexis.LienJudgment.Response.Report
{
    /// <summary>
    /// lexisnexis
    /// </summary>
    public class LienJudgmentThirdParty : ILienJudgmentThirdParty
    {
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <param name="thirdParty"></param>
        public LienJudgmentThirdParty(LendFoundry.Syndication.LexisNexis.LienJudgment.Report.LienJudgmentThirdParty thirdParty)
        {

            if (thirdParty != null)
            {
                OriginName = thirdParty.OriginName;
                Address = new Address(thirdParty.Address);
                if (thirdParty.Addresses != null)
                {
                    List<IAddress> thirdPartyAddress = new List<IAddress>();
                    foreach (LendFoundry.Syndication.LexisNexis.LienJudgment.Report.Address address in thirdParty.Addresses)
                    {
                        thirdPartyAddress.Add(new Address(address));
                    }
                    Addresses = thirdPartyAddress.ToArray();
                }
                if (thirdParty.Phones != null)
                {
                    List<IPhoneTimeZone> phoneTimeZone = new List<IPhoneTimeZone>();
                    foreach (LendFoundry.Syndication.LexisNexis.LienJudgment.Report.PhoneTimeZone phone in thirdParty.Phones)
                    {
                        phoneTimeZone.Add(new PhoneTimeZone(phone));
                    }
                    Phones = phoneTimeZone.ToArray();
                }
                if (thirdParty.ParsedParties != null)
                {
                    List<ILienJudgmentParty> lienJudgmentParties = new List<ILienJudgmentParty>();
                    foreach (LendFoundry.Syndication.LexisNexis.LienJudgment.Report.LienJudgmentParty lienparty in thirdParty.ParsedParties)
                    {
                        lienJudgmentParties.Add(new LienJudgmentParty(lienparty));
                    }
                    ParsedParties = lienJudgmentParties.ToArray();
                }
            }
        }
        
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string OriginName { get; set; }


        
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public IAddress Address { get; set; }


        

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public IAddress[] Addresses { get; set; }


        

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public IPhoneTimeZone[] Phones { get; set; }


        

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public ILienJudgmentParty[] ParsedParties { get; set; }
    }
}
