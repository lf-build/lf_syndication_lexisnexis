﻿

namespace LendFoundry.Syndication.LexisNexis.LienJudgment.Response.Report
{
    /// <summary>
    /// lexisnexis
    /// </summary>
    public class Address : IAddress
    {
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <param name="address"></param>
        public Address(LendFoundry.Syndication.LexisNexis.LienJudgment.Report.Address address)
        {
            if (address != null)
            {
                StreetNumber = address.StreetNumber;
                StreetPreDirection = address.StreetPreDirection;
                StreetName = address.StreetName;
                StreetSuffix = address.StreetSuffix;
                StreetPostDirection = address.StreetPostDirection;
                UnitDesignation = address.UnitDesignation;
                UnitNumber = address.UnitNumber;
                StreetAddress1 = address.StreetAddress1;
                StreetAddress2 = address.StreetAddress2;
                City = address.City;
                State = address.State;
                Zip5 = address.Zip5;
                Zip4 = address.Zip4;
                County = address.County;
                PostalCode = address.PostalCode;
                StateCityZip = address.StateCityZip;
                Latitude = address.Latitude;
                Longitude = address.Longitude;

            }
        }

        /// <summary>
        /// lexisnexis
        /// </summary>

        public string StreetNumber { get; set; }


        /// <summary>
        /// lexisnexis
        /// </summary>

        public string StreetPreDirection { get; set; }


        /// <summary>
        /// lexisnexis
        /// </summary>

        public string StreetName { get; set; }


        /// <summary>
        /// lexisnexis
        /// </summary>
        public string StreetSuffix { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>

        public string StreetPostDirection { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        public string UnitDesignation { get; set; }


        /// <summary>
        /// lexisnexis
        /// </summary>
        public string UnitNumber { get; set; }


        /// <summary>
        /// lexisnexis
        /// </summary>
        public string StreetAddress1 { get; set; }


        /// <summary>
        /// lexisnexis
        /// </summary>
        public string StreetAddress2 { get; set; }


        /// <summary>
        /// lexisnexis
        /// </summary>
        public string City { get; set; }


        /// <summary>
        /// lexisnexis
        /// </summary>
        public string State { get; set; }


        /// <summary>
        /// lexisnexis
        /// </summary>
        public string Zip5 { get; set; }


        /// <summary>
        /// lexisnexis
        /// </summary>
        public string Zip4 { get; set; }


        /// <summary>
        /// lexisnexis
        /// </summary>
        public string County { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        public string PostalCode { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        public string StateCityZip { get; set; }


        /// <summary>
        /// lexisnexis
        /// </summary>
        public string Latitude { get; set; }


        /// <summary>
        /// lexisnexis
        /// </summary>
        public string Longitude { get; set; }
    }
}
