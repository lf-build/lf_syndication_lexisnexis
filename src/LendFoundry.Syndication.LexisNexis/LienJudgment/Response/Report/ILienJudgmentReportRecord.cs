﻿

namespace LendFoundry.Syndication.LexisNexis.LienJudgment.Response.Report
{
    /// <summary>
    /// lexisnexis
    /// </summary>
    public interface ILienJudgmentReportRecord
    {
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        IBusinessIdentity BusinessIds { get; set; }

        
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string IdValue { get; set; }

        
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string RMSId { get; set; }

        
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string OriginFilingTime { get; set; }

        
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string TaxCode { get; set; }


        
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string Judge { get; set; }


        
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string ExternalKey { get; set; }


        
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string TMSId { get; set; }


        
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        IMatchedParty MatchedParty { get; set; }

        
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string IRSSerialNumber { get; set; }


        
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string OriginFilingNumber { get; set; }

        
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string OriginFilingType { get; set; }


        
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        IDate OriginFilingDate { get; set; }


        
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string CaseNumber { get; set; }


        
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string Eviction { get; set; }


        
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string Amount { get; set; }


        
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string FilingJurisdiction { get; set; }


        
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string FilingJurisdictionName { get; set; }


        
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string FilingState { get; set; }


        
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string FilingStatus { get; set; }


        
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string CertificateNumber { get; set; }


        
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string MultipleDefendant { get; set; }


        
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        IDate JudgeSatisfiedDate { get; set; }


        
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        IDate SuitDate { get; set; }

        
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        IDate JudgeVacatedDate { get; set; }


        
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        IDate ReleaseDate { get; set; }


        
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string LegalLot { get; set; }


        
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string LegalBlock { get; set; }


        

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        ILienJudgmentDebtor[] Debtors { get; set; }


        

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        ILienJudgmentThirdParty[] ThirdParties { get; set; }


        

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        ILienJudgmentCreditor[] Creditors { get; set; }


        

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        ILienJudgmentDebtorAttorney[] DebtorAttorneys { get; set; }

        

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        ILienJudgmentFiling[] Filings { get; set; }
    }
}
