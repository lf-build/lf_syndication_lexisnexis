﻿

namespace LendFoundry.Syndication.LexisNexis.LienJudgment.Response.Report
{
    /// <summary>
    /// lexisnexis
    /// </summary>
    public interface IFcraLienJudgmentReportResponse
    {
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        IResponseHeader Header { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        ILienJudgmentReportRecord[] LienJudgments { get; set; }
    }
}
