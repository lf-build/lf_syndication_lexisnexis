﻿

namespace LendFoundry.Syndication.LexisNexis.LienJudgment.Response.Search
{
    /// <summary>
    /// lexisnexis
    /// </summary>
    public class LienJudgmentFiling : ILienJudgmentFiling
    {
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <param name="filing"></param>
        public LienJudgmentFiling(LienJudgment.Search.LienJudgmentFiling filing)
        {
            if (filing != null)
            {
                Number = filing.Number;
                Type = filing.Type;
                Date = new Date(filing.Date);
                OriginFilingDate = new Date(filing.OriginFilingDate);
                Book = filing.Book;
                Page = filing.Page;
                Agency = filing.Agency;
                AgencyCity = filing.AgencyCity;
                AgencyCounty = filing.AgencyCounty;
                AgencyState = filing.AgencyState;
                FilingStatus = filing.FilingStatus;
                FilingDescription = filing.FilingDescription;
                FilingTime = filing.FilingTime;
            }
        }
        
        /// <summary>
        /// lexisnexis
        /// </summary>
        public string Number { get; set; }


        
        /// <summary>
        /// lexisnexis
        /// </summary>
        public string Type { get; set; }

        
        /// <summary>
        /// lexisnexis
        /// </summary>
        public IDate Date { get; set; }


        
        /// <summary>
        /// lexisnexis
        /// </summary>
        public IDate OriginFilingDate { get; set; }


        
        /// <summary>
        /// lexisnexis
        /// </summary>
        public string Book { get; set; }


        
        /// <summary>
        /// lexisnexis
        /// </summary>
        public string Page { get; set; }


        
        /// <summary>
        /// lexisnexis
        /// </summary>
        public string Agency { get; set; }


        
        /// <summary>
        /// lexisnexis
        /// </summary>
        public string AgencyCity { get; set; }

        
        /// <summary>
        /// lexisnexis
        /// </summary>
        public string AgencyState { get; set; }

        
        /// <summary>
        /// lexisnexis
        /// </summary>
        public string AgencyCounty { get; set; }

        
        /// <summary>
        /// lexisnexis
        /// </summary>
        public string FilingStatus { get; set; }


        
        /// <summary>
        /// lexisnexis
        /// </summary>
        public string FilingDescription { get; set; }


        
        /// <summary>
        /// lexisnexis
        /// </summary>
        public string FilingTime { get; set; }
    }
}
