﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.LexisNexis.LienJudgment.Response.Search
{
    /// <summary>
    /// lexisnexis
    /// </summary>
    public class LienJudgmentDebtor : ILienJudgmentDebtor
    {
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <param name="debtor"></param>
        public LienJudgmentDebtor(LienJudgment.Search.LienJudgmentDebtor debtor)
        {
            if (debtor != null)
            {
                OriginName = debtor.OriginName;
                if (debtor.Addresses != null)
                {
                    List<IAddress> debtorAddress = new List<IAddress>();
                    foreach (LienJudgment.Search.Address address in debtor.Addresses)
                    {
                        //debtorAddress.Add(new Address(address));
                    }
                    Addresses = debtorAddress.ToArray();
                }
                if (debtor.Phones != null)
                {
                    List<IPhoneTimeZone> phoneTimeZone = new List<IPhoneTimeZone>();
                    foreach (LienJudgment.Search.PhoneTimeZone phone in debtor.Phones)
                    {
                        phoneTimeZone.Add(new PhoneTimeZone(phone));
                    }
                    Phones = phoneTimeZone.ToArray();
                }
                if (debtor.ParsedParties != null)
                {
                    List<ILienJudgmentParty> lienJudgmentParties = new List<ILienJudgmentParty>();
                    foreach (LienJudgment.Search.LienJudgmentParty lienparty in debtor.ParsedParties)
                    {
                        lienJudgmentParties.Add(new LienJudgmentParty(lienparty));
                    }
                    ParsedParties = lienJudgmentParties.ToArray();
                }
            }
        }

        
        /// <summary>
        /// lexisnexis
        /// </summary>
        public string OriginName { get; set; }

        

        /// <summary>
        /// lexisnexis
        /// </summary>
        public IAddress[] Addresses { get; set; }


        

        /// <summary>
        /// lexisnexis
        /// </summary>
        public IPhoneTimeZone[] Phones { get; set; }


        

        /// <summary>
        /// lexisnexis
        /// </summary>
        public ILienJudgmentParty[] ParsedParties { get; set; }
    }
}
