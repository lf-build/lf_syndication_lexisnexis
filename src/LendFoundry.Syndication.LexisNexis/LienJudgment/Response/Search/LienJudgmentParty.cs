﻿

namespace LendFoundry.Syndication.LexisNexis.LienJudgment.Response.Search
{
    /// <summary>
    /// lexisnexis
    /// </summary>
    public class LienJudgmentParty : ILienJudgmentParty
    {
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <param name="lienparty"></param>
        public LienJudgmentParty(LienJudgment.Search.LienJudgmentParty lienparty)
        {
            if (lienparty != null)
            {
                BusinessIds = new BusinessIdentity(lienparty.BusinessIds);
                IdValue = lienparty.IdValue;
                Name = new Name(lienparty.Name);
                CompanyName = lienparty.CompanyName;
                UniqueId = lienparty.UniqueId;
                BusinessId = lienparty.BusinessId;
                SSN = lienparty.SSN;
                FEIN = lienparty.FEIN;
                PersonFilterId = lienparty.PersonFilterId;
                TaxId = lienparty.TaxId;
            }
        }

        
        /// <summary>
        /// lexisnexis
        /// </summary>
        public IBusinessIdentity BusinessIds { get; set; }


        
        /// <summary>
        /// lexisnexis
        /// </summary>
        public string IdValue { get; set; }


        
        /// <summary>
        /// lexisnexis
        /// </summary>
        public IName Name { get; set; }

        
        /// <summary>
        /// lexisnexis
        /// </summary>
        public string CompanyName { get; set; }


        
        /// <summary>
        /// lexisnexis
        /// </summary>
        public string UniqueId { get; set; }


        
        /// <summary>
        /// lexisnexis
        /// </summary>
        public string BusinessId { get; set; }
        
        /// <summary>
        /// lexisnexis
        /// </summary>
        public string SSN { get; set; }


        
        /// <summary>
        /// lexisnexis
        /// </summary>
        public string FEIN { get; set; }

        
        /// <summary>
        /// lexisnexis
        /// </summary>
        public string PersonFilterId { get; set; }


        
        /// <summary>
        /// lexisnexis
        /// </summary>
        public string TaxId { get; set; }

    }
}
