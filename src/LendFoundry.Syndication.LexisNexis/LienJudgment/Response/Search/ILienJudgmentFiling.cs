﻿

namespace LendFoundry.Syndication.LexisNexis.LienJudgment.Response.Search
{
    /// <summary>
    /// lexisnexis
    /// </summary>
    public interface ILienJudgmentFiling
    {

        
        /// <summary>
        /// lexisnexis
        /// </summary>
        string Number { get; set; }


        
        /// <summary>
        /// lexisnexis
        /// </summary>
        string Type { get; set; }

        
        /// <summary>
        /// lexisnexis
        /// </summary>
        IDate Date { get; set; }


        
        /// <summary>
        /// lexisnexis
        /// </summary>
        IDate OriginFilingDate { get; set; }


        
        /// <summary>
        /// lexisnexis
        /// </summary>
        string Book { get; set; }


        
        /// <summary>
        /// lexisnexis
        /// </summary>
        string Page { get; set; }


        
        /// <summary>
        /// lexisnexis
        /// </summary>
        string Agency { get; set; }


        
        /// <summary>
        /// lexisnexis
        /// </summary>
        string AgencyCity { get; set; }

        
        /// <summary>
        /// lexisnexis
        /// </summary>
        string AgencyState { get; set; }

        
        /// <summary>
        /// lexisnexis
        /// </summary>
        string AgencyCounty { get; set; }

        
        /// <summary>
        /// lexisnexis
        /// </summary>
        string FilingStatus { get; set; }


        
        /// <summary>
        /// lexisnexis
        /// </summary>
        string FilingDescription { get; set; }


        
        /// <summary>
        /// lexisnexis
        /// </summary>
        string FilingTime { get; set; }
    }
}
