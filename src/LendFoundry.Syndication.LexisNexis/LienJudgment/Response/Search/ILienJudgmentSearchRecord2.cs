﻿

namespace LendFoundry.Syndication.LexisNexis.LienJudgment.Response.Search
{
    /// <summary>
    /// lexisnexis
    /// </summary>
    public interface ILienJudgmentSearchRecord2
    {

        /// <summary>
        /// lexisnexis
        /// </summary>
        string TMSId { get; set; }


        
        /// <summary>
        /// lexisnexis
        /// </summary>
        IMatchedParty MatchedParty { get; set; }


        
        /// <summary>
        /// lexisnexis
        /// </summary>
        string IRSSerialNumber { get; set; }


        
        /// <summary>
        /// lexisnexis
        /// </summary>
        string OriginFilingNumber { get; set; }


        
        /// <summary>
        /// lexisnexis
        /// </summary>
        string OriginFilingType { get; set; }



        /// <summary>
        /// lexisnexis
        /// </summary>
        IDate OriginFilingDate { get; set; }



        /// <summary>
        /// lexisnexis
        /// </summary>
        string CaseNumber { get; set; }



        /// <summary>
        /// lexisnexis
        /// </summary>
        string Eviction { get; set; }



        /// <summary>
        /// lexisnexis
        /// </summary>
        string Amount { get; set; }



        /// <summary>
        /// lexisnexis
        /// </summary>
        string FilingJurisdiction { get; set; }



        /// <summary>
        /// lexisnexis
        /// </summary>
        string FilingJurisdictionName { get; set; }



        /// <summary>
        /// lexisnexis
        /// </summary>
        string FilingState { get; set; }



        /// <summary>
        /// lexisnexis
        /// </summary>
        string FilingStatus { get; set; }


        /// <summary>
        /// lexisnexis
        /// </summary>
        string CertificateNumber { get; set; }


        /// <summary>
        /// lexisnexis
        /// </summary>
        string MultipleDefendant { get; set; }



        /// <summary>
        /// lexisnexis
        /// </summary>
        IDate JudgeSatisfiedDate { get; set; }



        /// <summary>
        /// lexisnexis
        /// </summary>
        IDate SuitDate { get; set; }


        /// <summary>
        /// lexisnexis
        /// </summary>
        IDate JudgeVacatedDate { get; set; }



        /// <summary>
        /// lexisnexis
        /// </summary>
        IDate ReleaseDate { get; set; }



        /// <summary>
        /// lexisnexis
        /// </summary>
        string LegalLot { get; set; }



        /// <summary>
        /// lexisnexis
        /// </summary>
        string LegalBlock { get; set; }



        /// <summary>
        /// lexisnexis
        /// </summary>
        ILienJudgmentDebtor[] Debtors { get; set; }


        /// <summary>
        /// lexisnexis
        /// </summary>
        ILienJudgmentThirdParty[] ThirdParties { get; set; }



        /// <summary>
        /// lexisnexis
        /// </summary>
        ILienJudgmentCreditor[] Creditors { get; set; }



        /// <summary>
        /// lexisnexis
        /// </summary>
        ILienJudgmentDebtorAttorney[] DebtorAttorneys { get; set; }



        /// <summary>
        /// lexisnexis
        /// </summary>
        ILienJudgmentFiling[] Filings { get; set; }



        /// <summary>
        /// lexisnexis
        /// </summary>
        bool AlsoFound { get; set; }




        /// <summary>
        /// lexisnexis
        /// </summary>
        bool AlsoFoundSpecified { get; set; }
    }
}
