﻿

using System.Collections.Generic;

namespace LendFoundry.Syndication.LexisNexis.LienJudgment.Response.Search
{
    /// <summary>
    /// lexisnexis
    /// </summary>
    public class LienJudgmentSearchRecord2 : ILienJudgmentSearchRecord2
    {
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <param name="model"></param>
        public LienJudgmentSearchRecord2(LienJudgment.Search.LienJudgmentSearchRecord2 model)
        {
            if (model != null)
            {
                BusinessIds = new BusinessIdentity(model.BusinessIds);
                IdValue = model.IdValue;
                RMSId = model.RMSId;
                OriginFilingTime = model.RMSId;
                TaxCode = model.TaxCode;
                Judge = model.Judge;
                ExternalKey = model.ExternalKey;
                TMSId = model.TMSId;
                MatchedParty = new MatchedParty(model.MatchedParty);
                IRSSerialNumber = model.IRSSerialNumber;
                OriginFilingNumber = model.OriginFilingNumber;
                OriginFilingType = model.OriginFilingType;
                OriginFilingDate = new Date(model.OriginFilingDate);
                CaseNumber = model.CaseNumber;
                Eviction = model.Eviction;
                Amount = model.Amount;
                FilingJurisdiction = model.FilingJurisdiction;
                FilingJurisdictionName = model.FilingJurisdictionName;
                FilingState = model.FilingState;
                FilingStatus = model.FilingStatus;
                CertificateNumber = model.CertificateNumber;
                MultipleDefendant = model.MultipleDefendant;
                JudgeSatisfiedDate = new Date(model.JudgeSatisfiedDate);
                SuitDate = new Date(model.SuitDate);
                JudgeVacatedDate = new Date(model.JudgeVacatedDate);
                ReleaseDate = new Date(model.ReleaseDate);
                LegalLot = model.FilingJurisdictionName;
                LegalBlock = model.FilingJurisdictionName;
                AlsoFound = model.AlsoFound;
                AlsoFoundSpecified = model.AlsoFoundSpecified;
                if (model.Debtors != null)
                {
                    List<ILienJudgmentDebtor> debtorResponse = new List<ILienJudgmentDebtor>();
                    foreach (LienJudgment.Search.LienJudgmentDebtor debtor in model.Debtors)
                    {
                        debtorResponse.Add(new LienJudgmentDebtor(debtor));
                    }
                    Debtors = debtorResponse.ToArray();
                }
                if (model.ThirdParties != null)
                {
                    List<ILienJudgmentThirdParty> thirdPartiesResponse = new List<ILienJudgmentThirdParty>();
                    foreach (LienJudgment.Search.LienJudgmentThirdParty thirdParty in model.ThirdParties)
                    {
                        thirdPartiesResponse.Add(new LienJudgmentThirdParty(thirdParty));
                    }
                    ThirdParties = thirdPartiesResponse.ToArray();
                }
                if (model.Creditors != null)
                {
                    List<ILienJudgmentCreditor> creditorssResponse = new List<ILienJudgmentCreditor>();
                    foreach (LienJudgment.Search.LienJudgmentCreditor creditor in model.Creditors)
                    {
                        creditorssResponse.Add(new LienJudgmentCreditor(creditor));
                    }
                    Creditors = creditorssResponse.ToArray();
                }
                if (model.DebtorAttorneys != null)
                {
                    List<ILienJudgmentDebtorAttorney> debtorAttorneyResponse = new List<ILienJudgmentDebtorAttorney>();
                    foreach (LienJudgment.Search.LienJudgmentDebtorAttorney debtorAttorney in model.DebtorAttorneys)
                    {
                        debtorAttorneyResponse.Add(new LienJudgmentDebtorAttorney(debtorAttorney));
                    }
                    DebtorAttorneys = debtorAttorneyResponse.ToArray();
                }
                if (model.Filings != null)
                {
                    List<ILienJudgmentFiling> filingsResponse = new List<ILienJudgmentFiling>();
                    foreach (LienJudgment.Search.LienJudgmentFiling filing in model.Filings)
                    {
                        filingsResponse.Add(new LienJudgmentFiling(filing));
                    }
                    Filings = filingsResponse.ToArray();
                }
            }
        }


        
        /// <summary>
        /// lexisnexis
        /// </summary>
        public IBusinessIdentity BusinessIds { get; set; }


        
        /// <summary>
        /// lexisnexis
        /// </summary>
        public string IdValue { get; set; }

        
        /// <summary>
        /// lexisnexis
        /// </summary>
        public string RMSId { get; set; }


        
        /// <summary>
        /// lexisnexis
        /// </summary>
        public string OriginFilingTime { get; set; }


        
        /// <summary>
        /// lexisnexis
        /// </summary>
        public string TaxCode { get; set; }


        
        /// <summary>
        /// lexisnexis
        /// </summary>
        public string Judge { get; set; }


        
        /// <summary>
        /// lexisnexis
        /// </summary>
        public string ExternalKey { get; set; }


        
        /// <summary>
        /// lexisnexis
        /// </summary>
        public string TMSId { get; set; }


        
        /// <summary>
        /// lexisnexis
        /// </summary>
        public IMatchedParty MatchedParty { get; set; }


        
        /// <summary>
        /// lexisnexis
        /// </summary>
        public string IRSSerialNumber { get; set; }


        
        /// <summary>
        /// lexisnexis
        /// </summary>
        public string OriginFilingNumber { get; set; }


        
        /// <summary>
        /// lexisnexis
        /// </summary>
        public string OriginFilingType { get; set; }



        /// <summary>
        /// lexisnexis
        /// </summary>
        public IDate OriginFilingDate { get; set; }



        /// <summary>
        /// lexisnexis
        /// </summary>
        public string CaseNumber { get; set; }



        /// <summary>
        /// lexisnexis
        /// </summary>
        public string Eviction { get; set; }


        
        /// <summary>
        /// lexisnexis
        /// </summary>
        public string Amount { get; set; }



        /// <summary>
        /// lexisnexis
        /// </summary>
        public string FilingJurisdiction { get; set; }



        /// <summary>
        /// lexisnexis
        /// </summary>
        public string FilingJurisdictionName { get; set; }



        /// <summary>
        /// lexisnexis
        /// </summary>
        public string FilingState { get; set; }


        
        /// <summary>
        /// lexisnexis
        /// </summary>
        public string FilingStatus { get; set; }


        /// <summary>
        /// lexisnexis
        /// </summary>
        public string CertificateNumber { get; set; }


        
        /// <summary>
        /// lexisnexis
        /// </summary>
        public string MultipleDefendant { get; set; }



        /// <summary>
        /// lexisnexis
        /// </summary>
        public IDate JudgeSatisfiedDate { get; set; }


        
        /// <summary>
        /// lexisnexis
        /// </summary>
        public IDate SuitDate { get; set; }


        /// <summary>
        /// lexisnexis
        /// </summary>
        public IDate JudgeVacatedDate { get; set; }



        /// <summary>
        /// lexisnexis
        /// </summary>
        public IDate ReleaseDate { get; set; }



        /// <summary>
        /// lexisnexis
        /// </summary>
        public string LegalLot { get; set; }



        /// <summary>
        /// lexisnexis
        /// </summary>
        public string LegalBlock { get; set; }



        /// <summary>
        /// lexisnexis
        /// </summary>
        public ILienJudgmentDebtor[] Debtors { get; set; }


        /// <summary>
        /// lexisnexis
        /// </summary>
        public ILienJudgmentThirdParty[] ThirdParties { get; set; }



        /// <summary>
        /// lexisnexis
        /// </summary>
        public ILienJudgmentCreditor[] Creditors { get; set; }



        /// <summary>
        /// lexisnexis
        /// </summary>
        public ILienJudgmentDebtorAttorney[] DebtorAttorneys { get; set; }



        /// <summary>
        /// lexisnexis
        /// </summary>
        public ILienJudgmentFiling[] Filings { get; set; }


        
        /// <summary>
        /// lexisnexis
        /// </summary>
        public bool AlsoFound { get; set; }


        

        /// <summary>
        /// lexisnexis
        /// </summary>
        public bool AlsoFoundSpecified { get; set; }

    }
}
