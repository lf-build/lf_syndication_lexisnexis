﻿

namespace LendFoundry.Syndication.LexisNexis.LienJudgment.Response.Search
{
    /// <summary>
    /// lexisnexis
    /// </summary>
    public interface IDate
    {
        
        /// <summary>
        /// lexisnexis
        /// </summary>
        short Year { get; set; }

        
        /// <summary>
        /// lexisnexis
        /// </summary>
        bool YearSpecified { get; set; }

        
        /// <summary>
        /// lexisnexis
        /// </summary>
        short Month { get; set; }


        

        /// <summary>
        /// lexisnexis
        /// </summary>
        bool MonthSpecified { get; set; }

        
        /// <summary>
        /// lexisnexis
        /// </summary>
        short Day { get; set; }

        

        /// <summary>
        /// lexisnexis
        /// </summary>
        bool DaySpecified { get; set; }
    }
}
