﻿

namespace LendFoundry.Syndication.LexisNexis.LienJudgment.Response.Search
{
    /// <summary>
    /// lexisnexis
    /// </summary>
    public interface IResponseHeader
    {

        
        /// <summary>
        /// lexisnexis
        /// </summary>
        int Status { get; set; }

        

        /// <summary>
        /// lexisnexis
        /// </summary>
        bool StatusSpecified { get; set; }


        
        /// <summary>
        /// lexisnexis
        /// </summary>
        string Message { get; set; }


        
        /// <summary>
        /// lexisnexis
        /// </summary>
        string QueryId { get; set; }


        
        /// <summary>
        /// lexisnexis
        /// </summary>
        string TransactionId { get; set; }


               
        /// <summary>
        /// lexisnexis
        /// </summary>
        IWsException[] Exceptions { get; set; }
    }
}
