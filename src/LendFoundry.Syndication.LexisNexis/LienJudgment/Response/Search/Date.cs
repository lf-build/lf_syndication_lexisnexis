﻿

namespace LendFoundry.Syndication.LexisNexis.LienJudgment.Response.Search
{
    /// <summary>
    /// lexisnxis
    /// </summary>
    public class Date : IDate
    {
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <param name="date"></param>
        public Date(LienJudgment.Search.Date date)
        {
            if (date != null)
            {
                Year = date.Year;
                YearSpecified = date.YearSpecified;
                Month = date.Month;
                MonthSpecified = date.MonthSpecified;
                Day = date.Day;
                DaySpecified = date.DaySpecified;
            }

        }
        
        /// <summary>
        /// lexisnexis
        /// </summary>
        public short Year { get; set; }

        
        /// <summary>
        /// lexisnexis
        /// </summary>
        public bool YearSpecified { get; set; }

        
        /// <summary>
        /// lexisnexis
        /// </summary>
        public short Month { get; set; }


        

        /// <summary>
        /// lexisnexis
        /// </summary>
        public bool MonthSpecified { get; set; }

        
        /// <summary>
        /// lexisnexis
        /// </summary>
        public short Day { get; set; }

        

        /// <summary>
        /// lexisnexis
        /// </summary>
        public bool DaySpecified { get; set; }

    }
}
