﻿

namespace LendFoundry.Syndication.LexisNexis.LienJudgment.Response.Search
{
    /// <summary>
    /// lexisnexis
    /// </summary>
    public interface IName
    {

        /// <summary>
        /// lexisnexis
        /// </summary>
        string Full { get; set; }


        /// <summary>
        /// lexisnexis
        /// </summary>
        string First { get; set; }


        /// <summary>
        /// lexisnexis
        /// </summary>
        string Middle { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        string Last { get; set; }


        /// <summary>
        /// lexisnexis
        /// </summary>
        string Suffix { get; set; }


        /// <summary>
        /// lexisnexis
        /// </summary>
        string Prefix { get; set; }
    }
}
