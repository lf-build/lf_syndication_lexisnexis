﻿

using System.Collections.Generic;

namespace LendFoundry.Syndication.LexisNexis.LienJudgment.Response.Search
{
    /// <summary>
    /// lexisnexis
    /// </summary>
    public class LienJudgmentSearchResponse  :ILienJudgmentSearchResponse
    {
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <param name="response"></param>
        public LienJudgmentSearchResponse(LienJudgment.Search.LienJudgmentSearchResponse response)
        {
            if (response == null)
                return;

            Header = new ResponseHeader(response.Header);
            RecordCount = response.RecordCount;
            RecordCountSpecified = response.RecordCountSpecified;
            if (response.Records != null)
            {
                List<ILienJudgmentSearchRecord2> models = new List<ILienJudgmentSearchRecord2>();
                foreach (LienJudgment.Search.LienJudgmentSearchRecord2 model in response.Records)
                {
                    models.Add(new LienJudgmentSearchRecord2(model));
                }
                Records = models.ToArray();
            }
        }
       
        /// <summary>
        /// lexisnexis
        /// </summary>
        public IResponseHeader Header { get; set; }
        

      
        /// <summary>
        /// lexisnexis
        /// </summary>
        public int RecordCount { get; set; }        

       
        /// <summary>
        /// lexisnexis
        /// </summary>
        public bool RecordCountSpecified { get; set; }   
       

        
        
        /// <summary>
        /// lexisnexis
        /// </summary>
        public ILienJudgmentSearchRecord2[] Records { get; set; }   
       
    }
}
