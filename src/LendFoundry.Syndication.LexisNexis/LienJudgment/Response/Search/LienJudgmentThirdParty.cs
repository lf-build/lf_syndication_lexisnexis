﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.LexisNexis.LienJudgment.Response.Search
{
    /// <summary>
    /// lexisnexis
    /// </summary>
    public class LienJudgmentThirdParty : ILienJudgmentThirdParty
    {
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <param name="thirdParty"></param>
        public LienJudgmentThirdParty(LienJudgment.Search.LienJudgmentThirdParty thirdParty)
        {

            if (thirdParty != null)
            {
                OriginName = thirdParty.OriginName;
                Address = new Address(thirdParty.Address);
                if (thirdParty.Addresses != null)
                {
                    List<IAddress> thirdPartyAddress = new List<IAddress>();
                    foreach (LienJudgment.Search.Address address in thirdParty.Addresses)
                    {
                        thirdPartyAddress.Add(new Address(address));
                    }
                    Addresses = thirdPartyAddress.ToArray();
                }
                if (thirdParty.Phones != null)
                {
                    List<IPhoneTimeZone> phoneTimeZone = new List<IPhoneTimeZone>();
                    foreach (LienJudgment.Search.PhoneTimeZone phone in thirdParty.Phones)
                    {
                        phoneTimeZone.Add(new PhoneTimeZone(phone));
                    }
                    Phones = phoneTimeZone.ToArray();
                }
                if (thirdParty.ParsedParties != null)
                {
                    List<ILienJudgmentParty> lienJudgmentParties = new List<ILienJudgmentParty>();
                    foreach (LienJudgment.Search.LienJudgmentParty lienparty in thirdParty.ParsedParties)
                    {
                        lienJudgmentParties.Add(new LienJudgmentParty(lienparty));
                    }
                    ParsedParties = lienJudgmentParties.ToArray();
                }
            }
        }
        
        /// <summary>
        /// lexisnexis
        /// </summary>
        public string OriginName { get; set; }


        
        /// <summary>
        /// lexisnexis
        /// </summary>
        public IAddress Address { get; set; }


        

        /// <summary>
        /// lexisnexis
        /// </summary>
        public IAddress[] Addresses { get; set; }


        

        /// <summary>
        /// lexisnexis
        /// </summary>
        public IPhoneTimeZone[] Phones { get; set; }


        

        /// <summary>
        /// lexisnexis
        /// </summary>
        public ILienJudgmentParty[] ParsedParties { get; set; }

    }
}
