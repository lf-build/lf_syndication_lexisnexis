﻿

namespace LendFoundry.Syndication.LexisNexis.LienJudgment.Response.Search
{
    /// <summary>
    /// lexisnexis
    /// </summary>
    public interface IWsException
    {
        
        /// <summary>
        /// lexisnexis
        /// </summary>
        string Source { get; set; }


        
        /// <summary>
        /// lexisnexis
        /// </summary>
        int Code { get; set; }


        

        /// <summary>
        /// lexisnexis
        /// </summary>
        bool CodeSpecified { get; set; }


        
        /// <summary>
        /// lexisnexis
        /// </summary>
        string Location { get; set; }


        
        /// <summary>
        /// lexisnexis
        /// </summary>
        string Message { get; set; }
    }
}
