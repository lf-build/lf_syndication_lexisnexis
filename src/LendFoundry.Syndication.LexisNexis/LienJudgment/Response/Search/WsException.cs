﻿

namespace LendFoundry.Syndication.LexisNexis.LienJudgment.Response.Search
{
    /// <summary>
    /// lexisnexis
    /// </summary>
    public class WsException : IWsException
    {
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <param name="exception"></param>
        public WsException(LienJudgment.Search.WsException exception)
        {
            if (exception == null)
                return;

            Source = exception.Source;
            Code = exception.Code;
            Location = exception.Location;
            Message = exception.Message;
        }

        
        /// <summary>
        /// lexisnexis
        /// </summary>
        public string Source { get; set; }


        
        /// <summary>
        /// lexisnexis
        /// </summary>
        public int Code { get; set; }


        

        /// <summary>
        /// lexisnexis
        /// </summary>
        public bool CodeSpecified { get; set; }


        
        /// <summary>
        /// lexisnexis
        /// </summary>
        public string Location { get; set; }


        
        /// <summary>
        /// lexisnexis
        /// </summary>
        public string Message { get; set; }
    }
}
