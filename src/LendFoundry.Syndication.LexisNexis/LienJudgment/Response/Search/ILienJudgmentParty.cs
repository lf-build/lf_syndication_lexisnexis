﻿

namespace LendFoundry.Syndication.LexisNexis.LienJudgment.Response.Search
{
    /// <summary>
    /// lexisnexis
    /// </summary>
    public interface ILienJudgmentParty
    {
        
        /// <summary>
        /// lexisnexis
        /// </summary>
        IBusinessIdentity BusinessIds { get; set; }


        
        /// <summary>
        /// lexisnexis
        /// </summary>
        string IdValue { get; set; }


        
        /// <summary>
        /// lexisnexis
        /// </summary>
        IName Name { get; set; }

        
        /// <summary>
        /// lexisnexis
        /// </summary>
        string CompanyName { get; set; }


        
        /// <summary>
        /// lexisnexis
        /// </summary>
        string UniqueId { get; set; }


        
        /// <summary>
        /// lexisnexis
        /// </summary>
        string BusinessId { get; set; }
        
        /// <summary>
        /// lexisnexis
        /// </summary>
        string SSN { get; set; }


        
        /// <summary>
        /// lexisnexis
        /// </summary>
        string FEIN { get; set; }

        
        /// <summary>
        /// lexisnexis
        /// </summary>
        string PersonFilterId { get; set; }


        
        /// <summary>
        /// lexisnexis
        /// </summary>
        string TaxId { get; set; }
    }
}
