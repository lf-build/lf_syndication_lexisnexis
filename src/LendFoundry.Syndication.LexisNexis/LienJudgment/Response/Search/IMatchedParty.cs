﻿

namespace LendFoundry.Syndication.LexisNexis.LienJudgment.Response.Search
{
    /// <summary>
    /// lexisnexis
    /// </summary>
    public interface IMatchedParty
    {

        
        /// <summary>
        /// lexisnexis
        /// </summary>
        string PartyType { get; set; }


        
        /// <summary>
        /// lexisnexis
        /// </summary>
        string UniqueId { get; set; }


        
        /// <summary>
        /// lexisnexis
        /// </summary>
        string OriginName { get; set; }


        
        /// <summary>
        /// lexisnexis
        /// </summary>
        INameAndCompany ParsedParty { get; set; }


        
        /// <summary>
        /// lexisnexis
        /// </summary>
        IAddress Address { get; set; }
    }
}
