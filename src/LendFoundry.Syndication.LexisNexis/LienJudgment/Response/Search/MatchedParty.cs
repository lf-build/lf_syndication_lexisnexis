﻿

namespace LendFoundry.Syndication.LexisNexis.LienJudgment.Response.Search
{
    /// <summary>
    /// lexisnexis
    /// </summary>
    public class MatchedParty : IMatchedParty
    {
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <param name="matchedParty"></param>
        public MatchedParty(LienJudgment.Search.MatchedParty matchedParty)
        {
            if (matchedParty != null)
            {
                PartyType = matchedParty.PartyType;
                UniqueId = matchedParty.UniqueId;
                OriginName = matchedParty.OriginName;
                ParsedParty = new NameAndCompany(matchedParty.ParsedParty);
                Address = new Address(matchedParty.Address);
            }
        }
        
        /// <summary>
        /// lexisnexis
        /// </summary>
        public string PartyType { get; set; }
       

        
        /// <summary>
        /// lexisnexis
        /// </summary>
        public string UniqueId { get; set; }
       

        
        /// <summary>
        /// lexisnexis
        /// </summary>
        public string OriginName { get; set; } 
      

        
        /// <summary>
        /// lexisnexis
        /// </summary>
        public INameAndCompany ParsedParty { get; set; }  
        

        
        /// <summary>
        /// lexisnexis
        /// </summary>
        public IAddress Address { get; set; }
        
    }
}
