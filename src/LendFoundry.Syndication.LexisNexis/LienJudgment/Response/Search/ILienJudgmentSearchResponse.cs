﻿

namespace LendFoundry.Syndication.LexisNexis.LienJudgment.Response.Search
{
    /// <summary>
    /// lexisnexis
    /// </summary>
    public interface ILienJudgmentSearchResponse
    {
        /// <summary>
        /// lexisnexis
        /// </summary>
        IResponseHeader Header { get; set; }



        /// <summary>
        /// lexisnexis
        /// </summary>
        int RecordCount { get; set; }


        /// <summary>
        /// lexisnexis
        /// </summary>
        bool RecordCountSpecified { get; set; }


        /// <summary>
        /// lexisnexis
        /// </summary>
        ILienJudgmentSearchRecord2[] Records { get; set; }
    }
}
