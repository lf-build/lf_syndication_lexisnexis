﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.LexisNexis.LienJudgment.Response.Search
{
    /// <summary>
    /// lexisnexis
    /// </summary>
    public class LienJudgmentDebtorAttorney : ILienJudgmentDebtorAttorney
    {
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <param name="debtorAttorney"></param>
        public LienJudgmentDebtorAttorney(LienJudgment.Search.LienJudgmentDebtorAttorney debtorAttorney)
        {
            if (debtorAttorney != null)
            {
                Name = debtorAttorney.Name;
                Phone = debtorAttorney.Phone;
                TimeZone = debtorAttorney.TimeZone;

                Address = new Address(debtorAttorney.Address);
                if (debtorAttorney.Addresses != null)
                {
                    List<IAddress> creditorAddress = new List<IAddress>();
                    foreach (LienJudgment.Search.Address address in debtorAttorney.Addresses)
                    {
                        creditorAddress.Add(new Address(address));
                    }
                    Addresses = creditorAddress.ToArray();
                }
                if (debtorAttorney.Phones != null)
                {
                    List<IPhoneTimeZone> phoneTimeZone = new List<IPhoneTimeZone>();
                    foreach (LienJudgment.Search.PhoneTimeZone phone in debtorAttorney.Phones)
                    {
                        phoneTimeZone.Add(new PhoneTimeZone(phone));
                    }
                    Phones = phoneTimeZone.ToArray();
                }
                if (debtorAttorney.ParsedParties != null)
                {
                    List<ILienJudgmentParty> lienJudgmentParties = new List<ILienJudgmentParty>();
                    foreach (LienJudgment.Search.LienJudgmentParty lienparty in debtorAttorney.ParsedParties)
                    {
                        lienJudgmentParties.Add(new LienJudgmentParty(lienparty));
                    }
                    ParsedParties = lienJudgmentParties.ToArray();
                }
            }
        }
        
        /// <summary>
        /// lexisnexis
        /// </summary>
        public string Name { get; set; }


        
        /// <summary>
        /// lexisnexis
        /// </summary>
        public IAddress Address { get; set; }


        
        /// <summary>
        /// lexisnexis
        /// </summary>
        public string Phone { get; set; }


        
        /// <summary>
        /// lexisnexis
        /// </summary>
        public string TimeZone { get; set; }


        

        /// <summary>
        /// lexisnexis
        /// </summary>
        public IAddress[] Addresses { get; set; }


        

        /// <summary>
        /// lexisnexis
        /// </summary>
        public IPhoneTimeZone[] Phones { get; set; }


        

        /// <summary>
        /// lexisnexis
        /// </summary>
        public ILienJudgmentParty[] ParsedParties { get; set; }

    }
}
