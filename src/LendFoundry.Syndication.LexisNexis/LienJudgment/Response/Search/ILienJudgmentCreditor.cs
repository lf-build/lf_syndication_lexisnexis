﻿
namespace LendFoundry.Syndication.LexisNexis.LienJudgment.Response.Search
{
    /// <summary>
    /// lexisnexis
    /// </summary>
    public interface ILienJudgmentCreditor
    {

        
        /// <summary>
        /// lexisnexis
        /// </summary>
        string Name { get; set; }


        
        /// <summary>
        /// lexisnexis
        /// </summary>
        IAddress Address { get; set; }


        

        /// <summary>
        /// lexisnexis
        /// </summary>
        IAddress[] Addresses { get; set; }


        

        /// <summary>
        /// lexisnexis
        /// </summary>
        IPhoneTimeZone[] Phones { get; set; }


        

        /// <summary>
        /// lexisnexis
        /// </summary>
        ILienJudgmentParty[] ParsedParties { get; set; }
    }
}
