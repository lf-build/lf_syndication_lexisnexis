﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.LexisNexis.LienJudgment.Response.Search
{
    /// <summary>
    /// lexisnexis
    /// </summary>
    public class LienJudgmentCreditor : ILienJudgmentCreditor
    {
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <param name="creditor"></param>
        public LienJudgmentCreditor(LienJudgment.Search.LienJudgmentCreditor creditor)
        {
            if (creditor != null)
            {
                Name = creditor.Name;
                Address = new Address(creditor.Address);
                if (creditor.Addresses != null)
                {
                    List<IAddress> creditorAddress = new List<IAddress>();
                    foreach (LienJudgment.Search.Address address in creditor.Addresses)
                    {
                        creditorAddress.Add(new Address(address));
                    }
                    Addresses = creditorAddress.ToArray();
                }
                if (creditor.Phones != null)
                {
                    List<IPhoneTimeZone> phoneTimeZone = new List<IPhoneTimeZone>();
                    foreach (LienJudgment.Search.PhoneTimeZone phone in creditor.Phones)
                    {
                        phoneTimeZone.Add(new PhoneTimeZone(phone));
                    }
                    Phones = phoneTimeZone.ToArray();
                }
                if (creditor.ParsedParties != null)
                {
                    List<ILienJudgmentParty> lienJudgmentParties = new List<ILienJudgmentParty>();
                    foreach (LienJudgment.Search.LienJudgmentParty lienparty in creditor.ParsedParties)
                    {
                        lienJudgmentParties.Add(new LienJudgmentParty(lienparty));
                    }
                    ParsedParties = lienJudgmentParties.ToArray();
                }
            }

        }
        
        /// <summary>
        /// lexisnexis
        /// </summary>
        public string Name { get; set; }


        
        /// <summary>
        /// lexisnexis
        /// </summary>
        public IAddress Address { get; set; }


        

        /// <summary>
        /// lexisnexis
        /// </summary>
        public IAddress[] Addresses { get; set; }


        

        /// <summary>
        /// lexisnexis
        /// </summary>
        public IPhoneTimeZone[] Phones { get; set; }


        

        /// <summary>
        /// lexisnexis
        /// </summary>
        public ILienJudgmentParty[] ParsedParties { get; set; }

    }
}
