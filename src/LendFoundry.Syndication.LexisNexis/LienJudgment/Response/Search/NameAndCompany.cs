﻿namespace LendFoundry.Syndication.LexisNexis.LienJudgment.Response.Search
{
    /// <summary>
    /// lexisnexis
    /// </summary>
    public class NameAndCompany : INameAndCompany
    {
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <param name="nameAndCompany"></param>
        public NameAndCompany(LienJudgment.Search.NameAndCompany nameAndCompany)
        {
            if (nameAndCompany != null)
            {
                Full = nameAndCompany.Full;
                First = nameAndCompany.First;
                Middle = nameAndCompany.Middle;
                Last = nameAndCompany.Last;
                Suffix = nameAndCompany.Suffix;
                Prefix = nameAndCompany.Prefix;
                CompanyName = nameAndCompany.CompanyName;
            }
        }
        
        /// <summary>
        /// lexisnexis
        /// </summary>
        public string Full { get; set; }

        
        /// <summary>
        /// lexisnexis
        /// </summary>
        public string First { get; set; }


        
        /// <summary>
        /// lexisnexis
        /// </summary>
        public string Middle { get; set; }


        
        /// <summary>
        /// lexisnexis
        /// </summary>
        public string Last { get; set; }


        
        /// <summary>
        /// lexisnexis
        /// </summary>
        public string Suffix { get; set; }


        
        /// <summary>
        /// lexisnexis
        /// </summary>
        public string Prefix { get; set; }


        
        /// <summary>
        /// lexisnexis
        /// </summary>
        public string CompanyName { get; set; }

    }
}
