﻿

namespace LendFoundry.Syndication.LexisNexis.LienJudgment.Response.Search
{
    /// <summary>
    /// lexisnexis
    /// </summary>
    public interface IPhoneTimeZone
    {
        
        /// <summary>
        /// lexisnexis
        /// </summary>
        string Phone10 { get; set; }

        
        /// <summary>
        /// lexisnexis
        /// </summary>
        string Fax { get; set; }

        
        /// <summary>
        /// lexisnexis
        /// </summary>
        string TimeZone { get; set; }
    }
}
