﻿using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using LendFoundry.Syndication.LexisNexis.LienJudgment.Search;

namespace LendFoundry.Syndication.LexisNexis.LienJudgment
{
    /// <summary>
    /// lexisnexis
    /// </summary>
    public class LienJudgmentSearchRequest : ILienJudgmentSearchRequest
    {

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string Ssn { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string TaxId { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        [JsonConverter(typeof(InterfaceConverter<IName, LienJudgment.Name>))]
        public IName Name { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        [JsonConverter(typeof(InterfaceConverter<IAddress, LienJudgment.Address>))]
        public IAddress Address { get; set; }
    }
     
}
