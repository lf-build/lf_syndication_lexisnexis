﻿using LendFoundry.Syndication.LexisNexis.LienJudgment.Search;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.LexisNexis.LienJudgment
{
    /// <summary>
    /// lexisnexis
    /// </summary>
    public interface ILienJudgmentClient
    {
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <param name="search"></param>
        /// <returns></returns>
        Task<Response.Search.ILienJudgmentSearchResponse> Search(ILienJudgmentSearchRequest search);
    }
}
