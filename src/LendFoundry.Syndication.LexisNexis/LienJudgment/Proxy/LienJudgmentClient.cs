﻿using LendFoundry.Syndication.LexisNexis.LienJudgment.Search;
using System;
using System.Net;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.LexisNexis.LienJudgment
{
    /// <summary>
    /// lexisnexis
    /// </summary>
    public class LienJudgmentClient : ILienJudgmentClient
    {
        private LexisNexisConfiguration Configuration { get; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <param name="configuration"></param>
        public LienJudgmentClient(LexisNexisConfiguration configuration)
        {
            Configuration = configuration;
        }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<Response.Search.ILienJudgmentSearchResponse> Search(ILienJudgmentSearchRequest request)
        {
            if (Configuration == null)
                throw new ArgumentNullException(nameof(Configuration));

            if (Configuration.LienJudgment == null)
                throw new ArgumentNullException(nameof(Configuration.LienJudgment));

            if (Configuration.LienJudgment.UserName == null)
                throw new ArgumentNullException(nameof(Configuration.LienJudgment.UserName));

            if (Configuration.LienJudgment.Password == null)
                throw new ArgumentNullException(nameof(Configuration.LienJudgment.Password));


            return await Task.Run(() =>
            {
                try
                {
                    if (request == null)
                        throw new ArgumentNullException($"The {nameof(request)} cannot be null");

                    //if (string.IsNullOrWhiteSpace(request.Ssn) && string.IsNullOrWhiteSpace(request.TaxId))
                    //    throw new ArgumentNullException($"The {nameof(request.Ssn)} or {nameof(request.TaxId)} cannot be null");

                    WsAccurintServiceSoap soapClient = GetLienJudgmentSearchServiceClient();
                    //.Url = Configuration.LienJudgment.Url;
                    // soapClient.Credentials = new NetworkCredential(Configuration.LienJudgment.UserName, Configuration.LienJudgment.Password);
                    var user = new Search.User(Configuration.LienJudgment.EndUser);
                    var options = new LienJudgmentSearchOption(Configuration.LienJudgment.Options);
                    LienJudgmentSearchBy searchParam = new LienJudgmentSearchBy(request);

                    var soapResponse = soapClient.LienJudgmentSearchAsync(new LendFoundry.Syndication.LexisNexis.LienJudgment.Search.LienJudgmentSearchRequest(user, searchParam, options)).Result;

                    return new Response.Search.LienJudgmentSearchResponse(soapResponse.response);
                }
                catch (Exception ex)
                {
                    throw new LexisNexisException($"The method Report({request}) raised an error:{ex.Message}", ex);
                }

            });

        }

        private LienJudgment.Search.WsAccurintServiceSoap GetLienJudgmentSearchServiceClient()
        {
            LienJudgment.Search.WsAccurintServiceSoapClient LienJudgementSearch = new LienJudgment.Search.WsAccurintServiceSoapClient(LienJudgment.Search.WsAccurintServiceSoapClient.EndpointConfiguration.WsAccurintServiceSoap, Configuration.LienJudgment.Url);
            //BankruptcySearchSC.ClientCredentials.Windows.ClientCredential = new NetworkCredential (Configuration.Bankruptcy.FCRAUserName, Configuration.Bankruptcy.FCRAPassword);
            LienJudgementSearch.ClientCredentials.UserName.UserName = Configuration.LienJudgment.UserName;
            LienJudgementSearch.ClientCredentials.UserName.Password = Configuration.LienJudgment.Password;
            return LienJudgementSearch;
        }
    }
}
