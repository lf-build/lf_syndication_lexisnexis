﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.LexisNexis.LienJudgment
{
    /// <summary>
    /// lexisnexis
    /// </summary>
    public interface ISearchOption
    {

        /// <summary>
        /// lexisnexis
        /// </summary>
        bool IncludeAlsoFound { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        int ReturnCount { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        int StartingRecord { get; set; }
    }
}
