﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.LexisNexis.LienJudgment
{
    /// <summary>
    /// lexisnexis
    /// </summary>
    public class SearchOption : ISearchOption
    {
        /// <summary>
        /// lexisnexis
        /// </summary>
        public bool UseNicknames { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        public bool IncludeAlsoFound { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        public bool UsePhonetics { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        public int ReturnCount { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        public int StartingRecord { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        public bool ExcludeForeclosures { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        public bool ExcludeForeclosuresSpecified { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        public bool IncludeNoticeOfDefaults { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        public bool IncludeNoticeOfDefaultsSpecified { get; set; }
    }
}
