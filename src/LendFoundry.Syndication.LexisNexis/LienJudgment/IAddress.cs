﻿namespace LendFoundry.Syndication.LexisNexis.LienJudgment
{
    /// <summary>
    /// lexisnexis
    /// </summary>
    public interface IAddress
    {
        /// <summary>
        /// lexisnexis
        /// </summary>
        string StreetNumber { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        string StreetPreDirection { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        string StreetName { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        string StreetSuffix { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        string StreetPostDirection { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        string UnitDesignation { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        string UnitNumber { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        string StreetAddress1 { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        string StreetAddress2 { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        string City { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <summary>
        /// lexisnexis
        /// </summary>
        string State { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        string Zip5 { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        string Zip4 { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        string County { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        string PostalCode { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        string StateCityZip { get; set; }
    }
}
