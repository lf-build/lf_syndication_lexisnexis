﻿using LendFoundry.Syndication.LexisNexis.LienJudgment.Search;

namespace LendFoundry.Syndication.LexisNexis.LienJudgment.Search
{
    /// <summary>
    /// lexisnexis
    /// </summary>
    public partial class LienJudgmentSearchBy
    {
        /// <summary>
        /// lexisnexis
        /// </summary>
        public LienJudgmentSearchBy()
        {

        }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <param name="searchBy"></param>
        public LienJudgmentSearchBy(ILienJudgmentSearchRequest searchBy)
        {
            if (searchBy == null)
                return;

            SSN = searchBy.Ssn;
            FEIN = searchBy.TaxId;
            IncludeJudgments = true;
            IncludeLiens = true;

            if (searchBy.Name != null)
            {
                Name = new Name
                {
                    First = searchBy.Name.First,
                    Full = searchBy.Name.Full,
                    Last = searchBy.Name.Last,
                    Middle = searchBy.Name.Middle,
                    Prefix = searchBy.Name.Prefix,
                    Suffix = searchBy.Name.Suffix
                };
            }
            if (searchBy.Address != null)
            {
                Address = new Address
                {
                    City = searchBy.Address.City,
                    County = searchBy.Address.County,
                    PostalCode = searchBy.Address.PostalCode,
                    State = searchBy.Address.State,
                    StreetAddress1 = searchBy.Address.StreetAddress1,
                    StreetAddress2 = searchBy.Address.StreetAddress2,
                    StateCityZip = searchBy.Address.StateCityZip,
                    StreetName = searchBy.Address.StreetName,
                    StreetNumber = searchBy.Address.StreetNumber,
                    Zip4 = searchBy.Address.Zip4,
                    Zip5 = searchBy.Address.Zip5,
                    StreetSuffix = searchBy.Address.StreetSuffix,
                    StreetPostDirection = searchBy.Address.StreetPostDirection,
                    StreetPreDirection = searchBy.Address.StreetPreDirection,
                    UnitDesignation = searchBy.Address.UnitDesignation,
                    UnitNumber = searchBy.Address.UnitNumber
                };
            }
        }
    }
}
