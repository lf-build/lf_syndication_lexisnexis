﻿namespace LendFoundry.Syndication.LexisNexis.LienJudgment.Search
{
    /// <summary>
    /// lexisnexis
    /// </summary>
    public partial class LienJudgmentSearchOption
    {
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <param name="option"></param>
        public LienJudgmentSearchOption(ISearchOption option)
        {
            IncludeAlsoFound = option.IncludeAlsoFound;
            ReturnCount = option.ReturnCount;
            StartingRecord = option.StartingRecord;
        }
    }
}
