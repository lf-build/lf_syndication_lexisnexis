﻿using System;
using System.Runtime.Serialization;

namespace LendFoundry.Syndication.LexisNexis
{
    /// <summary>
    /// lexisnexis
    /// </summary>
    public class LexisNexisException : Exception
    {
        /// <summary>
        /// lexisnexis
        /// </summary>
        public LexisNexisException()
        {
        }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        public LexisNexisException(string message) : base(message)
        {
        }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <param name="message"></param>
        /// <param name="innerException"></param>
        /// <returns></returns>
        public LexisNexisException(string message, Exception innerException) : base(message, innerException)
        {
        }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <param name="info"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        protected LexisNexisException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
