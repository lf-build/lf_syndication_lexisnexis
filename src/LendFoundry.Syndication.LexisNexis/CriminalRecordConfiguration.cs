﻿using LendFoundry.Foundation.Client;
using LendFoundry.Syndication.LexisNexis.Bankruptcy;
using Newtonsoft.Json;

namespace LendFoundry.Syndication.LexisNexis
{
    /// <summary>
    /// lexisnexis
    /// </summary>
    public class CriminalRecordConfiguration
    {
        /// <summary>
        /// lexisnexis
        /// </summary>
        public string CriminalRecordUrl { get; set; }// = "https://wsonline.seisint.com/Demo/WsAccurintFCRA/?ver_=1.89";
        /// <summary>
        /// lexisnexis
        /// </summary>
        public string UserName { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        [JsonConverter(typeof(InterfaceConverter<CriminalRecord.IUser, CriminalRecord.User>))]
        public CriminalRecord.IUser EndUser { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        [JsonConverter(typeof(InterfaceConverter<ISearchOption, SearchOption>))]
        public ISearchOption Options { get; set; }
     
    }
}
