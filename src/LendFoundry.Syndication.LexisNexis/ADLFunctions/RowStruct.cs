
using LendFoundry.Foundation.Client;
using Newtonsoft.Json;


namespace LendFoundry.Syndication.LexisNexis.ADLFunctions
{
    /// <summary>
    /// lexsnexis
    /// </summary>
     public class RowStruct : IRowStruct
     {
         /// <summary>
         /// lexsnexis
         /// </summary>
        public RowStruct()
        {
        }
        /// <summary>
        /// lexsnexis
        /// </summary>
        /// <param name="rowStruct"></param>
        public RowStruct(Proxy.AllPossiblesADL.ServiceReference.AllPossiblesADLRowStruct rowStruct)
        {
            if(rowStruct?.Result!=null)
            Result = new InfoStruct(rowStruct?.Result);
            if(rowStruct?.Best!=null)
            Best = new BestDataStruct(rowStruct?.Best);
            if(rowStruct?.Verified!=null)
            Verified = new VerifiedItemsStruct(rowStruct.Verified);
        }
        /// <summary>
        /// lexsnexis
        /// </summary>
        /// <value></value>
         [JsonConverter(typeof(InterfaceConverter<IInfoStruct, InfoStruct>))]
        public IInfoStruct Result { get; set; }
        /// <summary>
        /// lexsnexis
        /// </summary>
        /// <value></value>
         [JsonConverter(typeof(InterfaceConverter<IBestDataStruct, BestDataStruct>))]
        public IBestDataStruct Best { get; set; }
        /// <summary>
        /// lexsnexis
        /// </summary>
        /// <value></value>
         [JsonConverter(typeof(InterfaceConverter<IVerifiedItemsStruct, VerifiedItemsStruct>))]
        public IVerifiedItemsStruct Verified { get; set; }
    }
}