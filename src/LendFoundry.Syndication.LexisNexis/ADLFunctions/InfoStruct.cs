using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
namespace LendFoundry.Syndication.LexisNexis.ADLFunctions
{
    /// <summary>
    /// InfoStruct
    /// </summary>
    public class InfoStruct : IInfoStruct
    {
        /// <summary>
        /// InfoStruct
        /// </summary>
        public InfoStruct()
        {
        }
         
        /// <summary>
        /// InfoStruct
        /// </summary>
        /// <param name="infoStruct"></param>
        public InfoStruct(Proxy.AllPossiblesADL.ServiceReference.AdlInfoStruct infoStruct)
        {
            Adl = infoStruct.ADL;
            Score = infoStruct.Score;
            Headercount = infoStruct.HeaderCount;
            Ssn = infoStruct.SSN;
            if(infoStruct?.DOB!=null)
            Dob= new DateStruct(infoStruct.DOB.Year,infoStruct.DOB.Month,infoStruct.DOB.Day);
            Phone = infoStruct.Phone;
            Title = infoStruct.Title;
            Firstname = infoStruct.FirstName;
            Middlename = infoStruct.MiddleName;
            Lastname = infoStruct.LastName;
            Suffix = infoStruct.Suffix;
            Primrange = infoStruct.PrimName;
            PreDir = infoStruct.PreDir;
            Addrsuffix = infoStruct.AddrSuffix;
            Unit = infoStruct.Unit;
            Secrange = infoStruct.SecRange;
            City = infoStruct.City;
            State = infoStruct.State;
            Zip5 = infoStruct.Zip5;
            Zip4 = infoStruct.Zip4;
        }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string Adl { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public int Score { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public int Headercount { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string Ssn { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        [JsonConverter(typeof(InterfaceConverter<IDateStruct, DateStruct>))]
        public IDateStruct Dob { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string Phone { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string Title { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string Firstname { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string Middlename { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string Lastname { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string Suffix { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string Primrange { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string PreDir { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string Primname { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string PostDir { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string Addrsuffix { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string Unit { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string Secrange { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string City { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string State { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string Zip5 { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string Zip4 { get; set; }
    }
}