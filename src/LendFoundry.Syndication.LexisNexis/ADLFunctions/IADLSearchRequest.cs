using System;

namespace LendFoundry.Syndication.LexisNexis.ADLFunctions
{
    /// <summary>
    /// lexinexis
    /// </summary>
    public interface IADLSearchRequest
    {
        /// <summary>
        /// lexinexis
        /// </summary>
        string Ssn { get; set; }
        /// <summary>
        /// lexinexis
        /// </summary>
        string Firstname { get; set; }
        /// <summary>
        /// lexinexis
        /// </summary>
        string Lastname { get; set; }
        /// <summary>
        /// lexinexis
        /// </summary>
        string Middlename { get; set; }
        /// <summary>
        /// lexinexis
        /// </summary>
        string Namesuffix { get; set; }
        /// <summary>
        /// lexinexis
        /// </summary>
        string Streetaddress1 { get; set; }
        /// <summary>
        /// lexinexis
        /// </summary>
        string City { get; set; }
        /// <summary>
        /// lexinexis
        /// </summary>
        string State { get; set; }
        /// <summary>
        /// lexinexis
        /// </summary>
        string Zip5 { get; set; }
        /// <summary>
        /// lexinexis
        /// </summary>
        string Phone { get; set; }
        /// <summary>
        /// lexinexis
        /// </summary>
        DateTimeOffset? DOB { get; set; }
    }
}