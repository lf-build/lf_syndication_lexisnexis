namespace LendFoundry.Syndication.LexisNexis.ADLFunctions
{
    /// <summary>
    /// lexisnexis
    /// </summary>
    public interface IVerifiedItemsStruct
    {
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        int Phone { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        int SSN { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        int Address { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        int Name { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        int DOB { get; set; }
    }
}