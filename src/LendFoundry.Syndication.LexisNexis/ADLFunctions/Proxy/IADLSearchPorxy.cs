﻿using System.Threading.Tasks;
using LendFoundry.Syndication.LexisNexis.ADLFunctions.Proxy.AllPossiblesADL.ServiceReference;

namespace LendFoundry.Syndication.LexisNexis.ADLFunctions.Proxy
{
    /// <summary>
    /// IADLSearchPorxy
    /// </summary>
    public interface IADLSearchPorxy
    {
        /// <summary>
        /// AllPossiblesADLSearch
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        Task<AllPossiblesADLResponse> AllPossiblesADLSearch(AllPossiblesADLRequest request);
    }
}