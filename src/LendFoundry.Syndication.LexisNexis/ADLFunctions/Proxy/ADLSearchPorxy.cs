﻿using System.Net;
using System;
using System.Threading.Tasks;

using LendFoundry.Syndication.LexisNexis.ADLFunctions.Proxy.AllPossiblesADL.ServiceReference;

namespace LendFoundry.Syndication.LexisNexis.ADLFunctions.Proxy
{
    /// <summary>
    /// ADLSearchPorxy
    /// </summary>
    public class ADLSearchPorxy : IADLSearchPorxy
    {
        /// <summary>
        /// ADLSearchPorxy
        /// </summary>
        /// <param name="configuration"></param>
        public ADLSearchPorxy(LexisNexisConfiguration configuration)
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            if (configuration?.AdlConfiguration == null)
                throw new ArgumentNullException("AdlConfiguration");
            if (string.IsNullOrEmpty(configuration?.AdlConfiguration?.AllPossiblesADLSerchUrl))
                throw new ArgumentNullException("AllPossiblesADLSerchUrl");
            if (string.IsNullOrEmpty(configuration?.AdlConfiguration?.UserName))
                throw new ArgumentNullException("UserName");
            if (string.IsNullOrEmpty(configuration?.AdlConfiguration?.Password))
                throw new ArgumentNullException("Password");
            Configuration = configuration;
        }
        private LexisNexisConfiguration Configuration { get; }
        /// <summary>
        /// AllPossiblesADLSearch
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<AllPossiblesADLResponse> AllPossiblesADLSearch(AllPossiblesADLRequest request)
        {
            try
            {
                if (request == null)
                    throw new ArgumentNullException(nameof(request));
           
                var soapClient = GetAllPossiblesADLSearchServiceClient();
                var soapResponse = await Task.Run(() => soapClient.AllPossiblesADLAsync(request).Result);
                return soapResponse;
            }
            catch (LexisNexisException ex)
            {
               
                throw new LexisNexisException(ex.Message);
            }
           
        }
        private WsADLServiceSoap GetAllPossiblesADLSearchServiceClient()
        {
          WsADLServiceSoapClient ADLServiceSC =
             new WsADLServiceSoapClient
             (WsADLServiceSoapClient.EndpointConfiguration.WsADLServiceSoap, Configuration.AdlConfiguration.AllPossiblesADLSerchUrl);
            ADLServiceSC.ClientCredentials.UserName.UserName = Configuration.AdlConfiguration.UserName;
            ADLServiceSC.ClientCredentials.UserName.Password = Configuration.AdlConfiguration.Password;
            return ADLServiceSC;
        }
    }
}