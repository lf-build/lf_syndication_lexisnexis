using System;


namespace LendFoundry.Syndication.LexisNexis.ADLFunctions.Proxy.AllPossiblesADL.ServiceReference
{
    /// <summary>
    /// LexisNexis
    /// </summary>
    public partial class AllPossiblesADLRequest
    {
        /// <summary>
        /// LexisNexis
        /// </summary>
        public AllPossiblesADLRequest(IADLSearchRequest request,ADLConfiguration configuraiton)
        {
            if (request == null)
                throw new ArgumentException("request paramert is empty");
            SSN = request.Ssn;
            FirstName = request.Firstname;
            LastName = request.Lastname;
            MiddleName = request.Middlename;
            Addr1 = request.Streetaddress1;
            Addr2 = request.City + ' ' + request.State + ' ' + request.Zip5;
            Phone = request.Phone;
            if (request.DOB!=null)
            {
                DOB = new AdlDateStruct()
                {
                    Year = (short)request.DOB.Value.Year,
                    Month = (short)request.DOB.Value.Month,
                    Day = (short)request.DOB.Value.Day
                };
            }

            DlPurpose = configuraiton.DLPurpose;
            GlbPurpose = configuraiton.GLBPurpose;
            NameIsOrdered = configuraiton.NameIsOrdered;
            Best = new BestIncludesStruct()
            {
                SSN = configuraiton.BestSSN,
                SSNSpecified = true,
                DOB = configuraiton.BestDOB,
                DOBSpecified = true,
                Name = configuraiton.BestName,
                NameSpecified = true,
                Addr = configuraiton.BestAddr,
                AddrSpecified = true,
                Phone = configuraiton.BestPhone,
                PhoneSpecified = true,
                DOD = configuraiton.BestDOD,
                DODSpecified = true
               
            };
            Verify = new VerifyIncludesStruct()
            {
                SSN = configuraiton.VerifySSN,
                SSNSpecified = true,
                DOB = configuraiton.VerifyDOB,
                DOBSpecified = true,
                Name = configuraiton.VerifyName,
                NameSpecified = true,
                Addr = configuraiton.VerifyAddr,
                AddrSpecified = true,
                 SSN4 = configuraiton.VerifySSN4,
                SSN4Specified = true,
                Phone = configuraiton.VerifyPhone,
                PhoneSpecified = true
               

            };
        }
    }
}