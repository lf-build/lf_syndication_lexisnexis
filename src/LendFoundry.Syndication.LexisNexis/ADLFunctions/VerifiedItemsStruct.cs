namespace LendFoundry.Syndication.LexisNexis.ADLFunctions
{
    /// <summary>
    /// lexsnexis
    /// </summary>
     public class VerifiedItemsStruct : IVerifiedItemsStruct
     {
         /// <summary>
         /// lexsnexis
         /// </summary>
        public VerifiedItemsStruct()
        {
        }
        /// <summary>
        /// lexsnexis
        /// </summary>
        /// <param name="verifiedItemsStruct"></param>
        public VerifiedItemsStruct(Proxy.AllPossiblesADL.ServiceReference.VerifiedItemsStruct verifiedItemsStruct)
        {
            Phone = verifiedItemsStruct.Phone;
            SSN = verifiedItemsStruct.SSN;
            Address = verifiedItemsStruct.Address;
            Name = verifiedItemsStruct.Name;
            DOB = verifiedItemsStruct.DOB;
        }
        /// <summary>
        /// lexsnexis
        /// </summary>
        /// <value></value>
        public int Phone { get; set; }
        
        /// <summary>
        /// lexsnexis
        /// </summary>
        /// <value></value>
        public int SSN { get; set; }

        /// <summary>
        /// lexsnexis
        /// </summary>
        /// <value></value>
        public int Address { get; set; }

        /// <summary>
        /// lexsnexis
        /// </summary>
        /// <value></value>
        public int Name { get; set; }

        /// <summary>
        /// lexsnexis
        /// </summary>
        /// <value></value>
        public int DOB { get; set; }

    }
}