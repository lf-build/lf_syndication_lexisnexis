namespace LendFoundry.Syndication.LexisNexis.ADLFunctions
{
    /// <summary>
    /// lexisnexis
    /// </summary>
    public interface IRowStruct
    {
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        IInfoStruct Result { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        IBestDataStruct Best { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        IVerifiedItemsStruct Verified { get; set; }
    }
}