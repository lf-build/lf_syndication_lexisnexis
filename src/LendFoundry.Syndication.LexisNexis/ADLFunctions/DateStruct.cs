namespace LendFoundry.Syndication.LexisNexis.ADLFunctions
{
    /// <summary>
    /// lexinexis
    /// </summary>
     public class DateStruct : IDateStruct
     {
         /// <summary>
         /// lexinexis
         /// </summary>
        public DateStruct()
        {
        }
        /// <summary>
        /// lexinexis
        /// </summary>
        /// <param name="year"></param>
        /// <param name="month"></param>
        /// <param name="day"></param>
        public DateStruct(short year,short month,short day)
        {
            Year = year;
            Month = month;
            Day = day;
        }
        /// <summary>
        /// lexinexis
        /// </summary>
        /// <value></value>
        public short Year { get; set; }
        /// <summary>
        /// lexinexis
        /// </summary>
        /// <value></value>
        public short Month { get; set; }
        /// <summary>
        /// lexinexis
        /// </summary>
        /// <value></value>
        public short Day { get; set; }
    }
}