using System.Collections.Generic;
using System.Linq;
using LendFoundry.Foundation.Client;
using Newtonsoft.Json;

namespace LendFoundry.Syndication.LexisNexis.ADLFunctions
{
    /// <summary>
    /// lexisnexis
    /// </summary>
    public class ADLSearchResponse : IADLSearchResponse
    {
        /// <summary>
        /// lexisnexis
        /// </summary>
        public ADLSearchResponse()
        {
            
        }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <param name="possiblesAdlResponse"></param>
        public ADLSearchResponse(Proxy.AllPossiblesADL.ServiceReference.AllPossiblesADLResponse possiblesAdlResponse)
        {
            if(possiblesAdlResponse?.Exceptions!=null)
             Exceptions = new SearchException(possiblesAdlResponse?.Exceptions);
            if(possiblesAdlResponse?.PossibleADLs!=null)
            PossibleADLs = new List<IRowStruct>(possiblesAdlResponse.PossibleADLs.Select(i => new RowStruct(i)).ToList());
        }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public SearchException Exceptions { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        [JsonConverter(typeof(InterfaceListConverter<IRowStruct, RowStruct>))]
        public List<IRowStruct> PossibleADLs { get; set; }
    }
   
   
  
   
    
   
}