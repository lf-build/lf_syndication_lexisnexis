namespace LendFoundry.Syndication.LexisNexis.ADLFunctions
{
    /// <summary>
    /// lexinexis
    /// </summary>
    public interface IDateStruct
    {
        
        /// <summary>
        /// lexinexis
        /// </summary>
        /// <value></value>
        short Year { get; set; }
        
        /// <summary>
        /// lexinexis
        /// </summary>
        /// <value></value>
        short Month { get; set; }
        
        /// <summary>
        /// lexinexis
        /// </summary>
        /// <value></value>
        short Day { get; set; }
    }
}