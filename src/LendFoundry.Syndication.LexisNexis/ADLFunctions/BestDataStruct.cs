using LendFoundry.Foundation.Client;
using Newtonsoft.Json;

namespace LendFoundry.Syndication.LexisNexis.ADLFunctions
{
    /// <summary>
    /// lexisnexis
    /// </summary>
    public class BestDataStruct : IBestDataStruct
    {
        /// <summary>
        /// lexisnexis
        /// </summary>
        public BestDataStruct()
        {
        }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <param name="dataStruct"></param>
        public BestDataStruct(Proxy.AllPossiblesADL.ServiceReference.BestDataStruct dataStruct)
        {
            Ssn = dataStruct.SSN;
            Maxssn = dataStruct.MaxSSN;
            Name = dataStruct.Name;
            Title = dataStruct.Title;
            Firstname = dataStruct.FirstName;
            Middlename = dataStruct.MiddleName;
            Lastname = dataStruct.LastName;
            Namesuffix = dataStruct.NameSuffix;
            Addr1 = dataStruct.Addr1;
            Addr2 = dataStruct.Addr2;
            City = dataStruct.City;
            State = dataStruct.State;
            Zip5 = dataStruct.Zip5;
            Zip4 = dataStruct.Zip4;
            if (dataStruct?.AddressDate != null)
                Addressdate = new DateStruct(dataStruct.AddressDate.Year, dataStruct.AddressDate.Month, dataStruct.AddressDate.Day);
            Phone = dataStruct.Phone;
            if (dataStruct?.DOB != null)
                DOB = new DateStruct(dataStruct.DOB.Year, dataStruct.DOB.Month, dataStruct.DOB.Day);
            if (dataStruct?.DOD != null)
                DOD = new DateStruct(dataStruct.DOD.Year, dataStruct.DOD.Month, dataStruct.DOD.Day); ;
            Verification = dataStruct.Verification;
        }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string Ssn { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string Maxssn { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string Name { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string Title { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string Firstname { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string Middlename { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string Lastname { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string Namesuffix { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string Addr1 { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string Addr2 { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string City { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string State { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string Zip5 { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string Zip4 { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        [JsonConverter(typeof(InterfaceConverter<IDateStruct, DateStruct>))]
        public IDateStruct Addressdate { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string Phone { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        [JsonConverter(typeof(InterfaceConverter<IDateStruct, DateStruct>))]
        public IDateStruct DOB { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        [JsonConverter(typeof(InterfaceConverter<IDateStruct, DateStruct>))]
        public IDateStruct DOD { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string Verification { get; set; }
    }
}