using System.Collections.Generic;
using System.Linq;
using LendFoundry.Foundation.Client;
using Newtonsoft.Json;

namespace LendFoundry.Syndication.LexisNexis.ADLFunctions
{
    /// <summary>
    /// lexsnexis
    /// </summary>
    public class SearchException
    {
        /// <summary>
        /// lexsnexis
        /// </summary>
        public SearchException()
        {
        }
        /// <summary>
        /// lexsnexis
        /// </summary>
        /// <param name="espException"></param>
        public SearchException(Proxy.AllPossiblesADL.ServiceReference.ArrayOfEspException espException)
        {
            Source = espException.Source;
            Exception = new List<IEspException>(espException.Exception.Select(i => new EspException(i.Code, i.Audience, i.Source, i.Message)).ToList());
        }
        /// <summary>
        /// lexsnexis
        /// </summary>
        /// <value></value>
        public string Source { get; set; }
        /// <summary>
        /// lexsnexis
        /// </summary>
        /// <value></value>
        [JsonConverter(typeof(InterfaceListConverter<IEspException, EspException>))]
        public List<IEspException> Exception { get; set; }
    }
}