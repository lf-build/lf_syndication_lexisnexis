namespace LendFoundry.Syndication.LexisNexis.ADLFunctions
{
    /// <summary>
    /// IInfoStruct
    /// </summary>
    public interface IInfoStruct
    {
        
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string Adl { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        int Score { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        int Headercount { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string Ssn { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        IDateStruct Dob { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string Phone { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string Title { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string Firstname { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string Middlename { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string Lastname { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string Suffix { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string Primrange { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string PreDir { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string Primname { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string PostDir { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string Addrsuffix { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string Unit { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string Secrange { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string City { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string State { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string Zip5 { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string Zip4 { get; set; }
    }
}