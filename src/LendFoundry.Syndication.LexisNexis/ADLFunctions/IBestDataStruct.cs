namespace LendFoundry.Syndication.LexisNexis.ADLFunctions
{
    /// <summary>
    /// lexinexis
    /// </summary>
    public interface IBestDataStruct
    {

        /// <summary>
        /// lexinexis
        /// </summary>
        /// <value></value>
        string Ssn { get; set; }
        
        /// <summary>
        /// lexinexis
        /// </summary>
        /// <value></value>
        string Maxssn { get; set; }
        
        /// <summary>
        /// lexinexis
        /// </summary>
        /// <value></value>
        string Name { get; set; }
        
        /// <summary>
        /// lexinexis
        /// </summary>
        /// <value></value>
        string Title { get; set; }
        
        /// <summary>
        /// lexinexis
        /// </summary>
        /// <value></value>
        string Firstname { get; set; }
        
        /// <summary>
        /// lexinexis
        /// </summary>
        /// <value></value>
        string Middlename { get; set; }
        
        /// <summary>
        /// lexinexis
        /// </summary>
        /// <value></value>
        string Lastname { get; set; }
        
        /// <summary>
        /// lexinexis
        /// </summary>
        /// <value></value>
        string Namesuffix { get; set; }
        
        /// <summary>
        /// lexinexis
        /// </summary>
        /// <value></value>
        string Addr1 { get; set; }
        
        /// <summary>
        /// lexinexis
        /// </summary>
        /// <value></value>
        string Addr2 { get; set; }
        
        /// <summary>
        /// lexinexis
        /// </summary>
        /// <value></value>
        string City { get; set; }
        
        /// <summary>
        /// lexinexis
        /// </summary>
        /// <value></value>
        string State { get; set; }
        
        /// <summary>
        /// lexinexis
        /// </summary>
        /// <value></value>
        string Zip5 { get; set; }
        
        /// <summary>
        /// lexinexis
        /// </summary>
        /// <value></value>
        string Zip4 { get; set; }
        
        /// <summary>
        /// lexinexis
        /// </summary>
        /// <value></value>
        IDateStruct Addressdate { get; set; }
        
        /// <summary>
        /// lexinexis
        /// </summary>
        /// <value></value>
        string Phone { get; set; }
        
        /// <summary>
        /// lexinexis
        /// </summary>
        /// <value></value>
        IDateStruct DOB { get; set; }
        
        /// <summary>
        /// lexinexis
        /// </summary>
        /// <value></value>
        IDateStruct DOD { get; set; }
        
        /// <summary>
        /// lexinexis
        /// </summary>
        /// <value></value>
        string Verification { get; set; }
    }
}