using System.Collections.Generic;

namespace LendFoundry.Syndication.LexisNexis.ADLFunctions
{
    /// <summary>
    /// lexinexis
    /// </summary>
    public interface IADLSearchResponse
    {
        /// <summary>
        /// lexinexis
        /// </summary>

        SearchException Exceptions { get; set; }
        /// <summary>
        /// lexinexis
        /// </summary>
        List<IRowStruct> PossibleADLs { get; set; }
    }
}