namespace LendFoundry.Syndication.LexisNexis.ADLFunctions
{
    /// <summary>
    /// IEspException
    /// </summary>
    public interface IEspException
    {
        
        /// <summary>
        /// lexinexis
        /// </summary>
        /// <value></value>
        string Code { get; set; }
                
        /// <summary>
        /// lexinexis
        /// </summary>
        /// <value></value>
        string Audience { get; set; }
                
        /// <summary>
        /// lexinexis
        /// </summary>
        /// <value></value>
        string Source { get; set; }
                
        /// <summary>
        /// lexinexis
        /// </summary>
        /// <value></value>
        string Message { get; set; }
    }
}