namespace LendFoundry.Syndication.LexisNexis.ADLFunctions
{
    /// <summary>
    /// lexinexis
    /// </summary>
      public class EspException : IEspException
      {
          /// <summary>
          /// lexinexis
          /// </summary>
        public EspException()
        {
            
        }
        /// <summary>
        /// lexinexis
        /// </summary>
        /// <param name="code"></param>
        /// <param name="audience"></param>
        /// <param name="source"></param>
        /// <param name="message"></param>
        public EspException(string code,string audience,string source,string message)
        {
            Code = code;
            Audience = audience;
            Source = source;
            Message = message;

        }
        /// <summary>
        /// lexinexis
        /// </summary>
        /// <value></value>
        public string Code { get; set; }
        /// <summary>
        /// lexinexis
        /// </summary>
        /// <value></value>
        public string Audience { get; set; }
        /// <summary>
        /// lexinexis
        /// </summary>
        /// <value></value>
        public string Source { get; set; }
        /// <summary>
        /// lexinexis
        /// </summary>
        /// <value></value>
        public string Message { get; set; }
    }
}