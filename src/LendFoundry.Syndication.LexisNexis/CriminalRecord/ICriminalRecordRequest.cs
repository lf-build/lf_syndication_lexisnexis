﻿namespace LendFoundry.Syndication.LexisNexis.CriminalRecord
{
    /// <summary>
    /// lexisnexis
    /// </summary>
    public interface ICriminalRecordReportRequest
    {
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string UniqueId { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string QueryId { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string OwnerId { get; set; }
    }
}
