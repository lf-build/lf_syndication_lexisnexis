﻿
namespace LendFoundry.Syndication.LexisNexis.CriminalRecord
{
    /// <summary>
    /// lexisnexis
    /// </summary>
    public interface IName
    {
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string Full { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string First { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string Middle { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string Last { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string Suffix { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string Prefix { get; set; }
    }
}
