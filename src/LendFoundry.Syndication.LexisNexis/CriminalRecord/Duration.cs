﻿
namespace LendFoundry.Syndication.LexisNexis.CriminalRecord
{
    /// <summary>
    /// lexisnexis
    /// </summary>
    public class Duration
    {
        /// <summary>
        /// lexisnexis
        /// </summary>
        public Duration() { }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <param name="record"></param>
        public Duration(Proxy.CriminalReport.ServiceReference.Duration record) {
            if (record == null)
                return;

            Years = record.Years;
            YearsSpecified = record.YearsSpecified;
            Months = record.Months;
            MonthsSpecified = record.MonthsSpecified;
            Days = record.Days;
            DaysSpecified = record.DaysSpecified;
        }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public short Years { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public bool YearsSpecified { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public short Months { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public bool MonthsSpecified { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public short Days { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public bool DaysSpecified { get; set; }
    }
}
