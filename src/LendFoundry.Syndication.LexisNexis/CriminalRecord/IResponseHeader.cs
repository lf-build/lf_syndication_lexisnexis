﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.LexisNexis.CriminalRecord
{
   /// <summary>
   /// lexisnexis
   /// </summary>
    public interface IResponseHeader
    {
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        int Status { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string Message { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string QueryId { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string TransactionId { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        List<IException> Exceptions { get; set; }

    }
}
