﻿
namespace LendFoundry.Syndication.LexisNexis.CriminalRecord
{
    /// <summary>
    /// lexisnexis
    /// </summary>
    public class CriminalRecordSearchRequest : ICriminalRecordSearchRequest
    {
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string Ssn { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string FirstName { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string LastName { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string StreetAddress1 { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string City { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string State { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string Zip5 { get; set; }
    }
}
