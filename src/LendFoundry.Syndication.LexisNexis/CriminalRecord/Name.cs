﻿
namespace LendFoundry.Syndication.LexisNexis.CriminalRecord
{
    /// <summary>
    /// lexisnexis
    /// </summary>
    public class Name : IName
    {
        /// <summary>
        /// lexisnexis
        /// </summary>
        public Name() { }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <param name="name"></param>
        public Name(Proxy.CriminalSearch.ServiceReference.Name name)
        {
            if (name == null)
                return;

            Full = name.Full;
            First = name.First;
            Middle = name.Middle;
            Last = name.Last;
            Suffix = name.Suffix;
            Prefix = name.Prefix;
        }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <param name="name"></param>
        public Name(Proxy.CriminalReport.ServiceReference.Name name)
        {
            if (name == null)
                return;

            Full = name.Full;
            First = name.First;
            Middle = name.Middle;
            Last = name.Last;
            Suffix = name.Suffix;
            Prefix = name.Prefix;
        }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string Full { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string First { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string Middle { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string Last { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string Suffix { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string Prefix { get; set; }
    }
}
