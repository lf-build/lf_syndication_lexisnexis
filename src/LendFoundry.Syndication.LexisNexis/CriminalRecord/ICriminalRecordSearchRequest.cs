﻿
namespace LendFoundry.Syndication.LexisNexis.CriminalRecord
{ 
    /// <summary>
    /// lexisnexis
    /// </summary>
    public interface ICriminalRecordSearchRequest
    {
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string Ssn { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string FirstName { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string LastName { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string StreetAddress1 { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string City { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string State { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string Zip5 { get; set; }
    }
}
