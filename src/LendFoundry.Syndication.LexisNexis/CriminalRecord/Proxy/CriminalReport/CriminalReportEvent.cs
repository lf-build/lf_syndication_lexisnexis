﻿using LendFoundry.Foundation.Client;
using LendFoundry.Syndication.LexisNexis.BusinessReport;
using LendFoundry.Syndication.LexisNexis.CriminalRecord.Proxy.CriminalReport.ServiceReference;
using Newtonsoft.Json;

namespace LendFoundry.Syndication.LexisNexis.CriminalRecord.Proxy.CriminalReport
{
    /// <summary>
    /// lexisnexis
    /// </summary>
    public class CriminalReportEvent : ICriminalReportEvent
    {
        /// <summary>
        /// lexisnexis
        /// </summary>
        public CriminalReportEvent()
        {
        }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <param name="record"></param>
        public CriminalReportEvent(CrimReportEvent record)
        {
            if (record == null)
                return;
            Date = record.Date != null ? new BusinessReport.Date { Day = record.Date.Day, Month = record.Date.Month, Year = record.Date.Year } : null;
            Description = record.Description;
        }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        [JsonConverter(typeof(InterfaceConverter<IDate, LendFoundry.Syndication.LexisNexis.BusinessReport.Date>))]
        public IDate Date { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string Description { get; set; }
    }
}