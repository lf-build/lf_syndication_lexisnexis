﻿using LendFoundry.Syndication.LexisNexis.BusinessReport;

namespace LendFoundry.Syndication.LexisNexis.CriminalRecord.Proxy.CriminalReport
{
    /// <summary>
    /// lexisnexis
    /// </summary>
    public interface ICriminalReportParoleOffense
    {
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
         IDate SentenceDate { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
         int Length { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
         bool LengthSpecified { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string OffenseCounty { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
         string CauseNo { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
         string NcicCode { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
         int OffenseCount { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
         bool OffenseCountSpecified { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        IDate OffenseDate { get; set; }
    }
}
