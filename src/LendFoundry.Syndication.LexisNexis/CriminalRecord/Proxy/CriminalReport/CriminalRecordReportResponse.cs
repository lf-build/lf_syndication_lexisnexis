﻿using LendFoundry.Syndication.LexisNexis.CriminalRecord.Proxy.CriminalReport.ServiceReference;
using System.Collections.Generic;
using System.Linq;
using LendFoundry.Foundation.Client;
using Newtonsoft.Json;

namespace LendFoundry.Syndication.LexisNexis.CriminalRecord.Proxy.CriminalReport
{
    /// <summary>
    /// lexisnexis
    /// </summary>
    public class CriminalReportResponse : ICriminalReportResponse
    {
        /// <summary>
        /// lexisnexis
        /// </summary>
        public CriminalReportResponse()
        {
        }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <param name="response"></param>
        public CriminalReportResponse(FcraCriminalReportResponse response)
        {
            if (response == null)
                return;
            Header = new ResponseHeader(response.Header);
            //RecordCount = response.;
            if (response.CriminalRecords != null)
            {
                Records = new List<ICriminalReportRecord>(response.CriminalRecords.Select(record => new CriminalReportRecord(record)));
            }
        }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        [JsonConverter(typeof(InterfaceConverter<IResponseHeader, ResponseHeader>))]
        public IResponseHeader Header { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public int RecordCount { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        [JsonConverter(typeof(InterfaceListConverter<ICriminalReportRecord, CriminalReportRecord>))]
        public List<ICriminalReportRecord> Records { get; set; }
    }
}