﻿using LendFoundry.Foundation.Client;
using LendFoundry.Syndication.LexisNexis.BusinessReport;
using LendFoundry.Syndication.LexisNexis.CriminalRecord.Proxy.CriminalReport.ServiceReference;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.LexisNexis.CriminalRecord.Proxy.CriminalReport
{
    /// <summary>
    /// lexisnexis
    /// </summary>
    public class CriminalReportParoleEx : ICriminalReportParoleEx
    {
        /// <summary>
        /// lexisnexis
        /// </summary>
        public CriminalReportParoleEx()
        {
        }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <param name="record"></param>
        public CriminalReportParoleEx(CrimReportParoleEx record)
        {
            if (record == null)
                return;

            CurrentStatus = record.CurrentStatus;
            County = record.County;
            Length = record.Length;
            PubicSaftyId = record.PubicSaftyId;
            InmateId = record.InmateId;
            ParoleInAbsentiaId = record.ParoleInAbsentiaId;
            Name = new Name(record.Name);
            Race = record.Race;
            Gender = record.Gender;
            CountyOfBirth = record.CountyOfBirth;

            StateOfBirth = record.StateOfBirth;
            HeightFeet = record.HeightFeet;
            HeightFeetSpecified = record.HeightFeetSpecified;
            HeightInches = record.HeightInches;
            HeightInchesSpecified = record.HeightInchesSpecified;
            WeightInPounds = record.WeightInPounds;
            WeightInPoundsSpecified = record.WeightInPoundsSpecified;
            HairColor = record.HairColor;
            SkinColor = record.SkinColor;
            ScarsMarksTattoos = record.ScarsMarksTattoos != null ? new List<string>(record.ScarsMarksTattoos.Select(tattoo => tattoo)) : null;

            PrisonFacilityType = record.PrisonFacilityType;
            PrisonFacilityName = record.PrisonFacilityName;
            PrisonStatus = record.PrisonStatus;
            LastReceiveOrDepartCode = record.LastReceiveOrDepartCode;
            CurrentStatusFlag = record.CurrentStatusFlag;
            ParoleRegion = record.ParoleRegion;
            SupervisingOffice = record.SupervisingOffice;
            SupervisingOfficerName = record.SupervisingOfficerName;
            SupervisingOfficerPhone = record.SupervisingOfficerPhone;
            ReleaseType = record.ReleaseType;

            RecordCreatedTimeStamp = new TimeStamp(record.RecordCreatedTimeStamp);
            LastKnownResidence = new Address(record.LastKnownResidence);

            ReleaseCounty = record.ReleaseCounty;
            LegalResidenceCounty = record.LegalResidenceCounty;
            OffenseCountSpecified = record.OffenseCountSpecified;
            OffenseCount = record.OffenseCount;

            Is3GOffender = record.Is3GOffender;
            Is3GOffenderSpecified = record.Is3GOffenderSpecified;
            IsViolentOffenderSpecified = record.IsViolentOffenderSpecified;
            IsViolentOffender = record.IsViolentOffender;

            IsSexOffender = record.IsSexOffender;
            IsSexOffenderSpecified = record.IsSexOffenderSpecified;
            IsOnViolentOffenderProgram = record.IsOnViolentOffenderProgram;
            IsOnViolentOffenderProgramSpecified = record.IsOnViolentOffenderProgramSpecified;

            LongestTimeToServeDescription = record.LongestTimeToServeDescription;
            LongestTimeToServe = new Duration(record.LongestTimeToServe);

            Offenses = record.Offenses != null ? new List<ICriminalReportParoleOffense>(record.Offenses.Select(offense => new CriminalReportParoleOffense(offense))) : null;

            ActualEndDate = record.ActualEndDate != null ? new BusinessReport.Date { Day = record.ActualEndDate.Day, Month = record.ActualEndDate.Month, Year = record.ActualEndDate.Year } : null;
            ScheduledEndDate = record.ScheduledEndDate != null ? new BusinessReport.Date { Day = record.ScheduledEndDate.Day, Month = record.ScheduledEndDate.Month, Year = record.ScheduledEndDate.Year } : null;
            AdmittedDate = record.AdmittedDate != null ? new BusinessReport.Date { Day = record.AdmittedDate.Day, Month = record.AdmittedDate.Month, Year = record.AdmittedDate.Year } : null;
            StartDate = record.StartDate != null ? new BusinessReport.Date { Day = record.StartDate.Day, Month = record.StartDate.Month, Year = record.StartDate.Year } : null;

            DateReported = record.DateReported != null ? new BusinessReport.Date { Day = record.DateReported.Day, Month = record.DateReported.Month, Year = record.DateReported.Year } : null;
            Dob = record.DOB != null ? new BusinessReport.Date { Day = record.DOB.Day, Month = record.DOB.Month, Year = record.DOB.Year } : null;
            LastReceiveOrDepartCDate = record.LastReceiveOrDepartCDate != null ? new BusinessReport.Date { Day = record.LastReceiveOrDepartCDate.Day, Month = record.LastReceiveOrDepartCDate.Month, Year = record.LastReceiveOrDepartCDate.Year } : null;
            CurrentStatusEffectiveDate = record.CurrentStatusEffectiveDate != null ? new BusinessReport.Date { Day = record.CurrentStatusEffectiveDate.Day, Month = record.CurrentStatusEffectiveDate.Month, Year = record.CurrentStatusEffectiveDate.Year } : null;

            ReleaseArrivalDate = record.ReleaseArrivalDate != null ? new BusinessReport.Date { Day = record.ReleaseArrivalDate.Day, Month = record.ReleaseArrivalDate.Month, Year = record.ReleaseArrivalDate.Year } : null;
            LastParoleReviewDate = record.LastParoleReviewDate != null ? new BusinessReport.Date { Day = record.LastParoleReviewDate.Day, Month = record.LastParoleReviewDate.Month, Year = record.LastParoleReviewDate.Year } : null;
            DischargeDate = record.DischargeDate != null ? new BusinessReport.Date { Day = record.DischargeDate.Day, Month = record.DischargeDate.Month, Year = record.DischargeDate.Year } : null;
        }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public IDate ActualEndDate { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string County { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string CurrentStatus { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string Length { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        [JsonConverter(typeof(InterfaceConverter<IDate, LendFoundry.Syndication.LexisNexis.BusinessReport.Date>))]
        public IDate ScheduledEndDate { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        [JsonConverter(typeof(InterfaceConverter<IDate, LendFoundry.Syndication.LexisNexis.BusinessReport.Date>))]
        public IDate StartDate { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        [JsonConverter(typeof(InterfaceConverter<IDate, LendFoundry.Syndication.LexisNexis.BusinessReport.Date>))]
        public IDate DateReported { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string PubicSaftyId { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string InmateId { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string ParoleInAbsentiaId { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        [JsonConverter(typeof(InterfaceConverter<IName, Name>))]
        public IName Name { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string Race { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string Gender { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        [JsonConverter(typeof(InterfaceConverter<IDate, LendFoundry.Syndication.LexisNexis.BusinessReport.Date>))]
        public IDate Dob { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string CountyOfBirth { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string StateOfBirth { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public int HeightFeet { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public bool HeightFeetSpecified { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public int HeightInches { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public bool HeightInchesSpecified { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public int WeightInPounds { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public bool WeightInPoundsSpecified { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string HairColor { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string SkinColor { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string EyeColor { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public List<string> ScarsMarksTattoos { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string PrisonFacilityType { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string PrisonFacilityName { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        [JsonConverter(typeof(InterfaceConverter<IDate, LendFoundry.Syndication.LexisNexis.BusinessReport.Date>))]
        public IDate AdmittedDate { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string PrisonStatus { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string LastReceiveOrDepartCode { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        [JsonConverter(typeof(InterfaceConverter<IDate, LendFoundry.Syndication.LexisNexis.BusinessReport.Date>))]
        public IDate LastReceiveOrDepartCDate { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public TimeStamp RecordCreatedTimeStamp { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string CurrentStatusFlag { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        [JsonConverter(typeof(InterfaceConverter<IDate, LendFoundry.Syndication.LexisNexis.BusinessReport.Date>))]
        public IDate CurrentStatusEffectiveDate { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string ParoleRegion { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string SupervisingOffice { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string SupervisingOfficerName { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string SupervisingOfficerPhone { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        [JsonConverter(typeof(InterfaceConverter<IAddress, Address>))]
        public IAddress LastKnownResidence { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        [JsonConverter(typeof(InterfaceConverter<IDate, LendFoundry.Syndication.LexisNexis.BusinessReport.Date>))]
        public IDate ReleaseArrivalDate { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string ReleaseType { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string ReleaseCounty { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string LegalResidenceCounty { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        [JsonConverter(typeof(InterfaceConverter<IDate, LendFoundry.Syndication.LexisNexis.BusinessReport.Date>))]
        public IDate LastParoleReviewDate { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public int OffenseCount { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public bool OffenseCountSpecified { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public bool Is3GOffender { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public bool Is3GOffenderSpecified { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public bool IsViolentOffender { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public bool IsViolentOffenderSpecified { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public bool IsSexOffender { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public bool IsSexOffenderSpecified { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public bool IsOnViolentOffenderProgram { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public bool IsOnViolentOffenderProgramSpecified { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public Duration LongestTimeToServe { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string LongestTimeToServeDescription { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        [JsonConverter(typeof(InterfaceConverter<IDate, LendFoundry.Syndication.LexisNexis.BusinessReport.Date>))]
        public IDate DischargeDate { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        [JsonConverter(typeof(InterfaceListConverter<ICriminalReportParoleOffense, CriminalReportParoleOffense>))]
        public List<ICriminalReportParoleOffense> Offenses { get; set; }
    }
}