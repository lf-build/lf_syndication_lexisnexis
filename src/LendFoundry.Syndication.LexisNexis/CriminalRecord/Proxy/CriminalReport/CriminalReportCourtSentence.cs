﻿using LendFoundry.Syndication.LexisNexis.CriminalRecord.Proxy.CriminalReport.ServiceReference;

namespace LendFoundry.Syndication.LexisNexis.CriminalRecord.Proxy.CriminalReport
{
    /// <summary>
    /// lexisnexis
    /// </summary>
    public class CriminalReportCourtSentence: ICriminalReportCourtSentence
    {
        /// <summary>
        /// lexisnexis
        /// </summary>
        public CriminalReportCourtSentence() { }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <param name="record"></param>
        public CriminalReportCourtSentence(CrimReportCourtSentence record) {
            if (record == null)
                return;
            Jail = record.Jail;
            Probation = record.Probation;
            Suspended = record.Suspended;
            
        }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string Jail { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string Probation { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string Suspended { get; set; }
    }
}
