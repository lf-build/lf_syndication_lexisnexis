﻿using LendFoundry.Syndication.LexisNexis.BusinessReport;

namespace LendFoundry.Syndication.LexisNexis.CriminalRecord.Proxy.CriminalReport
{
    /// <summary>
    /// lexisnexis
    /// </summary>
    public interface ICriminalReportPrison
    {
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
         IDate AdmittedDate { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
         string CurrentStatus { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
         string CustodyType { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
         IDate CustodyTypeChangeDate { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
         string GainTimeGranted { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
         IDate LastGainTime { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
         string Location { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
         IDate ScheduledReleaseDate { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
         string Sentence { get; set; }
    }
}
