﻿using LendFoundry.Syndication.LexisNexis.BusinessReport;

namespace LendFoundry.Syndication.LexisNexis.CriminalRecord.Proxy.CriminalReport
{
    /// <summary>
    /// lexisnexis
    /// </summary>
    public interface ICriminalReportOffense
    {
        
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
         string AdjudicationWithheld { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
         string CaseNumber { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
         string CaseType { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
         string CaseTypeDescription { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
         string Count { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
         string County { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
         string Description { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
         string MaximumTerm { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
         string MinimumTerm { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
         string NumberCounts { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
         IDate OffenseDate { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
         string OffenseType { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
         string Sentence { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
         string SentenceLengthDescription { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
         IDate SentenceDate { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
         IDate IncarcerationDate { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
         ICriminalReportAppeal Appeal { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
         ICriminalReportArrest Arrest { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
         ICriminalReportCourt Court { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
         ICriminalReportCourtSentence CourtSentence { get; set; }
    }
}
