﻿using LendFoundry.Syndication.LexisNexis.BusinessReport;

namespace LendFoundry.Syndication.LexisNexis.CriminalRecord.Proxy.CriminalReport
{
    /// <summary>
    /// lexisnexis
    /// </summary>
    public interface ICriminalReportArrest
    {
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string Agency { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string CaseNumber { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        IDate Date { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string Disposition { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        IDate DispositionDate { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string Level { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string Offense { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string Statute { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string Type { get; set; }
    }
}