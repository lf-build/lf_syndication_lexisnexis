﻿using LendFoundry.Foundation.Client;
using LendFoundry.Syndication.LexisNexis.BusinessReport;
using LendFoundry.Syndication.LexisNexis.CriminalRecord.Proxy.CriminalReport.ServiceReference;
using Newtonsoft.Json;

namespace LendFoundry.Syndication.LexisNexis.CriminalRecord.Proxy.CriminalReport
{
    /// <summary>
    /// lexisnexis
    /// </summary>
    public class CriminalReportOffense : ICriminalReportOffense
    {
        /// <summary>
        /// lexisnexis
        /// </summary>
        public CriminalReportOffense()
        {
        }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <param name="offenseRecord"></param>
        public CriminalReportOffense(CrimReportOffense offenseRecord)
        {
            if (offenseRecord == null)
                return;

            AdjudicationWithheld = offenseRecord.AdjudicationWithheld;
            CaseNumber = offenseRecord.CaseNumber;
            CaseType = offenseRecord.CaseType;
            CaseTypeDescription = offenseRecord.CaseTypeDescription;
            Count = offenseRecord.Count;
            County = offenseRecord.County;
            Description = offenseRecord.Description;
            MaximumTerm = offenseRecord.MaximumTerm;
            MinimumTerm = offenseRecord.MinimumTerm;
            NumberCounts = offenseRecord.NumberCounts;
            OffenseType = offenseRecord.OffenseType;
            Sentence = offenseRecord.Sentence;
            SentenceLengthDescription = offenseRecord.SentenceLengthDescription;
            OffenseDate = offenseRecord.OffenseDate != null ? new BusinessReport.Date { Day = offenseRecord.OffenseDate.Day, Month = offenseRecord.OffenseDate.Month, Year = offenseRecord.OffenseDate.Year } : null;
            SentenceDate = offenseRecord.SentenceDate != null ? new BusinessReport.Date { Day = offenseRecord.SentenceDate.Day, Month = offenseRecord.SentenceDate.Month, Year = offenseRecord.SentenceDate.Year } : null;
            Appeal = offenseRecord.Appeal != null ? new CriminalReportAppeal(offenseRecord.Appeal) : null;
            Arrest = offenseRecord.Arrest != null ? new CriminalReportArrest(offenseRecord.Arrest) : null;
            Court = offenseRecord.Court != null ? new CriminalReportCourt(offenseRecord.Court) : null;
            CourtSentence = offenseRecord.CourtSentence != null ? new CriminalReportCourtSentence(offenseRecord.CourtSentence) : null;
        }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string AdjudicationWithheld { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string CaseNumber { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string CaseType { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string CaseTypeDescription { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string Count { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string County { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string Description { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string MaximumTerm { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string MinimumTerm { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string NumberCounts { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        [JsonConverter(typeof(InterfaceConverter<IDate, LendFoundry.Syndication.LexisNexis.BusinessReport.Date>))]
        public IDate OffenseDate { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string OffenseType { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string Sentence { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string SentenceLengthDescription { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        [JsonConverter(typeof(InterfaceConverter<IDate, LendFoundry.Syndication.LexisNexis.BusinessReport.Date>))]
        public IDate SentenceDate { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        [JsonConverter(typeof(InterfaceConverter<IDate, LendFoundry.Syndication.LexisNexis.BusinessReport.Date>))]
        public IDate IncarcerationDate { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        [JsonConverter(typeof(InterfaceConverter<ICriminalReportAppeal, CriminalReportAppeal>))]
        public ICriminalReportAppeal Appeal { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        [JsonConverter(typeof(InterfaceConverter<ICriminalReportArrest, CriminalReportArrest>))]
        public ICriminalReportArrest Arrest { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        [JsonConverter(typeof(InterfaceConverter<ICriminalReportCourt, CriminalReportCourt>))]
        public ICriminalReportCourt Court { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        [JsonConverter(typeof(InterfaceConverter<ICriminalReportCourtSentence, CriminalReportCourtSentence>))]
        public ICriminalReportCourtSentence CourtSentence { get; set; }
    }
}