﻿using LendFoundry.Syndication.LexisNexis.BusinessReport;

namespace LendFoundry.Syndication.LexisNexis.CriminalRecord.Proxy.CriminalReport
{
    /// <summary>
    /// lexisnexis
    /// </summary>
    public interface ICriminalReportAppeal
    {
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        IDate Date { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string Disposition { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string FinalDisposition { get; set; }
    }
}