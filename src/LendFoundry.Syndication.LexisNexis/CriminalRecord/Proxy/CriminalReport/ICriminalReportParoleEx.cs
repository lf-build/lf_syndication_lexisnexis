﻿using LendFoundry.Syndication.LexisNexis.BusinessReport;
using System.Collections.Generic;

namespace LendFoundry.Syndication.LexisNexis.CriminalRecord.Proxy.CriminalReport
{
    /// <summary>
    /// lexisnexis
    /// </summary>
     public interface ICriminalReportParoleEx
    {
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
         IDate ActualEndDate { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
         string County { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
         string CurrentStatus { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
         string Length { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
         IDate ScheduledEndDate { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
         IDate StartDate { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
         IDate DateReported { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
         string PubicSaftyId { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
         string InmateId { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
         string ParoleInAbsentiaId { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
         IName Name { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
         string Race { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
         string Gender { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
         IDate Dob { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
         string CountyOfBirth { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
         string StateOfBirth { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
         int HeightFeet { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
         bool HeightFeetSpecified { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        int HeightInches { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
         bool HeightInchesSpecified { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        int WeightInPounds { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
         bool WeightInPoundsSpecified { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string HairColor { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
         string SkinColor { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
         string EyeColor { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
         List<string> ScarsMarksTattoos { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
         string PrisonFacilityType { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
         string PrisonFacilityName { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
         IDate AdmittedDate { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
         string PrisonStatus { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
         string LastReceiveOrDepartCode { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
         IDate LastReceiveOrDepartCDate { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
         TimeStamp RecordCreatedTimeStamp { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
         string CurrentStatusFlag { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
         IDate CurrentStatusEffectiveDate { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
         string ParoleRegion { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
         string SupervisingOffice { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
         string SupervisingOfficerName { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
         string SupervisingOfficerPhone { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
         IAddress LastKnownResidence { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
         IDate ReleaseArrivalDate { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
         string ReleaseType { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
         string ReleaseCounty { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
         string LegalResidenceCounty { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
         IDate LastParoleReviewDate { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
         int OffenseCount { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
         bool OffenseCountSpecified { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
         bool Is3GOffender { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
         bool Is3GOffenderSpecified { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
         bool IsViolentOffender { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
         bool IsViolentOffenderSpecified { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
         bool IsSexOffender { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
         bool IsSexOffenderSpecified { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
         bool IsOnViolentOffenderProgram { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
         bool IsOnViolentOffenderProgramSpecified { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
         Duration LongestTimeToServe { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
         string LongestTimeToServeDescription { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
         IDate DischargeDate { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
         List<ICriminalReportParoleOffense> Offenses { get; set; }
    }
}
