﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.LexisNexis.CriminalRecord.Proxy.CriminalReport
{
    /// <summary>
    /// lexisnexis
    /// </summary>
    public interface ICriminalReportResponse
    {
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        IResponseHeader Header { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        int RecordCount { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        List<ICriminalReportRecord> Records { get; set; }
    }
}
