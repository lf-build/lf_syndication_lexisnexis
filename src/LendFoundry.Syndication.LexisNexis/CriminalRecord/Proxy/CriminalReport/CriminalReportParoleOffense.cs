﻿using LendFoundry.Foundation.Client;
using LendFoundry.Syndication.LexisNexis.BusinessReport;
using LendFoundry.Syndication.LexisNexis.CriminalRecord.Proxy.CriminalReport.ServiceReference;
using Newtonsoft.Json;

namespace LendFoundry.Syndication.LexisNexis.CriminalRecord.Proxy.CriminalReport
{
    /// <summary>
    /// lexisnexis
    /// </summary>
    public class CriminalReportParoleOffense : ICriminalReportParoleOffense
    {
        /// <summary>
        /// lexisnexis
        /// </summary>
        public CriminalReportParoleOffense()
        {
        }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <param name="record"></param>
        public CriminalReportParoleOffense(CrimReportParoleOffense record)
        {
            if (record == null)
                return;

            Length = record.Length;
            LengthSpecified = record.LengthSpecified;
            OffenseCounty = record.OffenseCounty;
            CauseNo = record.CauseNo;

            NcicCode = record.NCICCode;
            OffenseCount = record.OffenseCount;
            OffenseCountSpecified = record.OffenseCountSpecified;
            CauseNo = record.CauseNo;

            SentenceDate = record.SentenceDate != null ? new BusinessReport.Date { Day = record.SentenceDate.Day, Month = record.SentenceDate.Month, Year = record.SentenceDate.Year } : null;
            OffenseDate = record.OffenseDate != null ? new BusinessReport.Date { Day = record.OffenseDate.Day, Month = record.OffenseDate.Month, Year = record.OffenseDate.Year } : null;
        }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public IDate SentenceDate { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public int Length { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public bool LengthSpecified { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string OffenseCounty { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string CauseNo { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string NcicCode { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public int OffenseCount { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public bool OffenseCountSpecified { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        [JsonConverter(typeof(InterfaceConverter<IDate, LendFoundry.Syndication.LexisNexis.BusinessReport.Date>))]
        public IDate OffenseDate { get; set; }
    }
}