﻿using LendFoundry.Syndication.LexisNexis.Bankruptcy;

namespace LendFoundry.Syndication.LexisNexis.CriminalRecord.Proxy.CriminalReport.ServiceReference
{
    /// <summary>
    /// lexisnexis
    /// </summary>
    public partial class FcraCriminalReportOption
    {
       /// <summary>
       /// lexisnexis
       /// </summary>
       /// <param name="searchOption"></param>
        public FcraCriminalReportOption(ISearchOption searchOption)
        {
            if (searchOption == null)
                return;

            FCRAPurpose = searchOption.FCRAPurpose;
            FCRAPurposeSpecified = true;
        }
    }
}
