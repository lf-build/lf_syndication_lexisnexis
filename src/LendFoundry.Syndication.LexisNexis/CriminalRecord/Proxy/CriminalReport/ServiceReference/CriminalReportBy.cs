﻿namespace LendFoundry.Syndication.LexisNexis.CriminalRecord.Proxy.CriminalReport.ServiceReference
{
    /// <summary>
    /// lexisnexis
    /// </summary>
    public partial class FcraCriminalReportBy
    {
        /// <summary>
        /// lexisnexis
        /// </summary>
        public FcraCriminalReportBy()
        { }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <param name="reportBy"></param>
        public FcraCriminalReportBy(ICriminalRecordReportRequest reportBy)
        {
            if (reportBy == null)
                return;

            UniqueId = reportBy.UniqueId;

        }
    }
}
