﻿using LendFoundry.Foundation.Client;
using LendFoundry.Syndication.LexisNexis.BusinessReport;
using LendFoundry.Syndication.LexisNexis.CriminalRecord.Proxy.CriminalReport.ServiceReference;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.LexisNexis.CriminalRecord.Proxy.CriminalReport
{
    /// <summary>
    /// lexisnexis
    /// </summary>
    public class CriminalReportRecord : ICriminalReportRecord
    {
        /// <summary>
        /// lexisnexis
        /// </summary>
        public CriminalReportRecord()
        {
        }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <param name="record"></param>
        public CriminalReportRecord(CrimReportRecord record)
        {
            if (record == null)
                return;
            OffenderId = record.OffenderId;
            CaseNumber = record.CaseNumber;
            CountyOfOrigin = record.CountyOfOrigin;
            DocNumber = record.DOCNumber;
            CaseFilingDate = record.CaseFilingDate != null ? new BusinessReport.Date { Day = record.CaseFilingDate.Day, Month = record.CaseFilingDate.Month, Year = record.CaseFilingDate.Year } : null;
            Eyes = record.Eyes;
            Hair = record.Hair;
            Height = record.Height;
            Weight = record.Weight;
            Race = record.Race;
            Sex = record.Sex;
            Skin = record.Skin;
            DataSource = record.DataSource;
            Ssn = record.SSN;
            UniqueId = record.UniqueId;
            StateOfBirth = record.StateOfBirth;
            StateOfOrigin = record.StateOfOrigin;
            Dob = record.DOB != null ? new BusinessReport.Date { Day = record.DOB.Day, Month = record.DOB.Month, Year = record.DOB.Year } : null;

            Status = record.Status;
            Address = new Address(record.Address);
            Name = new Name(record.Name);
            CaseTypeDescription = record.CaseTypeDescription;
            Akas = record.AKAs != null ? new List<IName>(record.AKAs.Select(aka => new Name(aka))) : null;

            Offenses = record.Offenses != null ? new List<ICriminalReportOffense>(record.Offenses.Select(offense => new CriminalReportOffense(offense))) : null;
            PrisonSentences = record.PrisonSentences != null ? new List<ICriminalReportPrison>(record.PrisonSentences.Select(sentence => new CriminalReportPrison(sentence))) : null;
            ParoleSentences = record.ParoleSentences != null ? new List<ICriminalReportParoleEx>(record.ParoleSentences.Select(sentence => new CriminalReportParoleEx(sentence))) : null;
            Activities = record.Activities != null ? new List<ICriminalReportEvent>(record.Activities.Select(activity => new CriminalReportEvent(activity))) : null;
        }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string OffenderId { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string CaseNumber { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string CountyOfOrigin { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string DocNumber { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        [JsonConverter(typeof(InterfaceConverter<IDate, LendFoundry.Syndication.LexisNexis.BusinessReport.Date>))]
        public IDate CaseFilingDate { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string Eyes { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string Hair { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string Height { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string Weight { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string Race { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string Sex { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string Skin { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string DataSource { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string Ssn { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string UniqueId { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string StateOfBirth { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string StateOfOrigin { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string Status { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        [JsonConverter(typeof(InterfaceConverter<IAddress, Address>))]
        public IAddress Address { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        [JsonConverter(typeof(InterfaceConverter<IName, Name>))]
        public IName Name { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        [JsonConverter(typeof(InterfaceConverter<IDate, LendFoundry.Syndication.LexisNexis.BusinessReport.Date>))]
        public IDate Dob { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string CaseTypeDescription { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        [JsonConverter(typeof(InterfaceListConverter<IName, Name>))]
        public List<IName> Akas { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        [JsonConverter(typeof(InterfaceListConverter<ICriminalReportOffense, CriminalReportOffense>))]
        public List<ICriminalReportOffense> Offenses { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        [JsonConverter(typeof(InterfaceListConverter<ICriminalReportPrison, CriminalReportPrison>))]
        public List<ICriminalReportPrison> PrisonSentences { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        [JsonConverter(typeof(InterfaceListConverter<ICriminalReportParoleEx, CriminalReportParoleEx>))]
        public List<ICriminalReportParoleEx> ParoleSentences { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        [JsonConverter(typeof(InterfaceListConverter<ICriminalReportEvent, CriminalReportEvent>))]
        public List<ICriminalReportEvent> Activities { get; set; }
    }
}