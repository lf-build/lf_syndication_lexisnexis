﻿namespace LendFoundry.Syndication.LexisNexis.CriminalRecord.Proxy.CriminalReport
{
    /// <summary>
    /// lexisnexis
    /// </summary>
    public interface ICriminalReportCourtSentence
    {
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
         string Jail { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
         string Probation { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
         string Suspended { get; set; }
    }
}
