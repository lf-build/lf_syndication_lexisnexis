﻿using LendFoundry.Foundation.Client;
using LendFoundry.Syndication.LexisNexis.BusinessReport;
using LendFoundry.Syndication.LexisNexis.CriminalRecord.Proxy.CriminalReport.ServiceReference;
using Newtonsoft.Json;

namespace LendFoundry.Syndication.LexisNexis.CriminalRecord.Proxy.CriminalReport
{
    /// <summary>
    /// lexisnexis
    /// </summary>
    public class CriminalReportArrest : ICriminalReportArrest
    {
        /// <summary>
        /// lexisnexis
        /// </summary>
        public CriminalReportArrest()
        {
        }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <param name="record"></param>
        public CriminalReportArrest(CrimReportArrest record)
        {
            if (record == null)
                return;
            Agency = record.Agency;
            CaseNumber = record.CaseNumber;
            Disposition = record.Disposition;
            Level = record.Level;
            Date = record.Date != null ? new BusinessReport.Date { Day = record.Date.Day, Month = record.Date.Month, Year = record.Date.Year } : null;
            DispositionDate = record.Date != null ? new BusinessReport.Date { Day = record.DispositionDate.Day, Month = record.DispositionDate.Month, Year = record.DispositionDate.Year } : null;
            Offense = record.Offense;
            Statute = record.Statute;
            Type = record.Type;
        }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string Agency { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string CaseNumber { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        [JsonConverter(typeof(InterfaceConverter<IDate, LendFoundry.Syndication.LexisNexis.BusinessReport.Date>))]
        public IDate Date { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string Disposition { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        [JsonConverter(typeof(InterfaceConverter<IDate, LendFoundry.Syndication.LexisNexis.BusinessReport.Date>))]
        public IDate DispositionDate { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string Level { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string Offense { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string Statute { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string Type { get; set; }
    }
}