﻿using LendFoundry.Foundation.Client;
using LendFoundry.Syndication.LexisNexis.BusinessReport;
using LendFoundry.Syndication.LexisNexis.CriminalRecord.Proxy.CriminalReport.ServiceReference;
using Newtonsoft.Json;

namespace LendFoundry.Syndication.LexisNexis.CriminalRecord.Proxy.CriminalReport
{
    /// <summary>
    /// lexisnexis
    /// </summary>
    public class CriminalReportAppeal : ICriminalReportAppeal
    {
        /// <summary>
        /// lexisnexis
        /// </summary>
        public CriminalReportAppeal()
        {
        }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <param name="record"></param>
        public CriminalReportAppeal(CrimReportAppeal record)
        {
            if (record == null)
                return;
            Date = record.Date != null ? new BusinessReport.Date { Day = record.Date.Day, Month = record.Date.Month, Year = record.Date.Year } : null;
            Disposition = record.Disposition;
            FinalDisposition = record.FinalDisposition;
        }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        [JsonConverter(typeof(InterfaceConverter<IDate, LendFoundry.Syndication.LexisNexis.BusinessReport.Date>))]
        public IDate Date { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string Disposition { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string FinalDisposition { get; set; }
    }
}