﻿using LendFoundry.Syndication.LexisNexis.BusinessReport;
using System.Collections.Generic;

namespace LendFoundry.Syndication.LexisNexis.CriminalRecord.Proxy.CriminalReport
{
    /// <summary>
    /// lexisnexis
    /// </summary>
    public class ICriminalReportRecord
    {
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
         string OffenderId { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
         string CaseNumber { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
         string CountyOfOrigin { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
         string DocNumber { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
         IDate CaseFilingDate { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
         string Eyes { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
         string Hair { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
         string Height { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
         string Weight { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
         string Race { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
         string Sex { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
         string Skin { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
         string DataSource { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
         string Ssn { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
         string UniqueId { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
         string StateOfBirth { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
         string StateOfOrigin { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
         string Status { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
         IAddress Address { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
         IName Name { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
         IDate Dob { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
         string CaseTypeDescription { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
         List<IName> Akas { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
         List<ICriminalReportOffense> offenses { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
         List<ICriminalReportPrison> prisonSentences { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
         List<CriminalReportParoleEx> paroleSentences { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
         List<ICriminalReportEvent> activities { get; set; }
    }
}
