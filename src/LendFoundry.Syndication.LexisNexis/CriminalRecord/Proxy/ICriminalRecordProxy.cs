﻿

using LendFoundry.Syndication.LexisNexis.CriminalRecord.Proxy.CriminalReport.ServiceReference;
using LendFoundry.Syndication.LexisNexis.CriminalRecord.Proxy.CriminalSearch.ServiceReference;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.LexisNexis.CriminalRecord.Proxy
{
    /// <summary>
    /// lexisnexis
    /// </summary>
    public interface ICriminalRecordProxy
    {
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <param name="user"></param>
        /// <param name="searchBy"></param>
        /// <param name="options"></param>
        /// <returns></returns>
        Task<FcraCriminalSearchResponse> CriminalRecordSearch(CriminalSearch.ServiceReference.User user, FcraCriminalSearchBy searchBy, FcraCriminalSearchOption options);
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <param name="user"></param>
        /// <param name="reportBy"></param>
        /// <param name="options"></param>
        /// <returns></returns>
        Task<FcraCriminalReportResponse> CriminalRecordReport(CriminalReport.ServiceReference.User user, FcraCriminalReportBy reportBy, FcraCriminalReportOption options);
    }
}   