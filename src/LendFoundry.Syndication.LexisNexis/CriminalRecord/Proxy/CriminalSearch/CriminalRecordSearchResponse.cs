﻿using System.Collections.Generic;
using System.Linq;
using LendFoundry.Syndication.LexisNexis.CriminalRecord.Proxy.CriminalSearch.ServiceReference;
using LendFoundry.Foundation.Client;
using Newtonsoft.Json;

namespace LendFoundry.Syndication.LexisNexis.CriminalRecord.Proxy
{
    /// <summary>
    /// lexisnexis
    /// </summary>
    public class CriminalRecordSearchResponse : ICriminalRecordSearchResponse
    {
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <param name="searchResponse"></param>
        public CriminalRecordSearchResponse(FcraCriminalSearchResponse searchResponse)
        {
            if (searchResponse == null)
                return;
            Header = new ResponseHeader(searchResponse.Header);
            RecordCount = searchResponse.RecordCount;
            if (searchResponse.Records != null)
            {
                Records = new List<ICriminalSearchRecord>(searchResponse.Records.Select(record => new CriminalSearchRecord(record)));
            }
        }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        [JsonConverter(typeof(InterfaceConverter<IResponseHeader, ResponseHeader>))]
        public IResponseHeader Header { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public int RecordCount { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        [JsonConverter(typeof(InterfaceListConverter<ICriminalSearchRecord, CriminalSearchRecord>))]
        public List<ICriminalSearchRecord> Records { get; set; }
    }
}