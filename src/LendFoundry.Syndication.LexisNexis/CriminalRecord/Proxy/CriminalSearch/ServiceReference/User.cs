﻿namespace LendFoundry.Syndication.LexisNexis.CriminalRecord.Proxy.CriminalSearch.ServiceReference
{
    /// <summary>
    /// lexisnexis
    /// </summary>
    public partial class User
    {
        /// <summary>
        /// lexisnexis
        /// </summary>
        public User()
        { }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <param name="user"></param>
        public User(IUser user)
        {
            if (user == null)
                return;
            ReferenceCode = user.ReferenceCode;
            BillingCode = user.BillingCode;
            QueryId = user.QueryId;
            GLBPurpose = user.GLBPurpose;
            DLPurpose = user.DLPurpose;
            EndUser = new EndUserInfo
            {
                City = user.EndUser.City,
                CompanyName = user.EndUser.CompanyName,
                State = user.EndUser.State,
                StreetAddress1 = user.EndUser.StreetAddress1,
                Zip5 = user.EndUser.Zip5
            };
            MaxWaitSeconds = user.MaxWaitSeconds;
            AccountNumber = user.AccountNumber;
        }
    }
}
