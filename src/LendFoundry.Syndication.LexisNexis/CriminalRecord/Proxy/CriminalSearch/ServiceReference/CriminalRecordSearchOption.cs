﻿using LendFoundry.Syndication.LexisNexis.Bankruptcy;

namespace LendFoundry.Syndication.LexisNexis.CriminalRecord.Proxy.CriminalSearch.ServiceReference
{
    /// <summary>
    /// lexisnexis
    /// </summary>
    public partial class FcraCriminalSearchOption
    {
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <param name="searchOption"></param>
        public FcraCriminalSearchOption(ISearchOption searchOption)
        {
            if (searchOption == null)
                return;
            ReturnCount = searchOption.ReturnCount;
            StartingRecord = searchOption.StartingRecord;
            FCRAPurpose = searchOption.FCRAPurpose;
            FCRAPurposeSpecified = true;
        }
    }
}
