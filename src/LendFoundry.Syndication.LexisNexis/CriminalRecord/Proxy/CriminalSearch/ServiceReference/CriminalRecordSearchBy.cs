﻿namespace LendFoundry.Syndication.LexisNexis.CriminalRecord.Proxy.CriminalSearch.ServiceReference
{
    /// <summary>
    /// lexisnexis
    /// </summary>
    public partial class FcraCriminalSearchBy
    {
        /// <summary>
        /// lexisnexis
        /// </summary>
        public FcraCriminalSearchBy()
        { }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <param name="searchBy"></param>
        public FcraCriminalSearchBy(ICriminalRecordSearchRequest searchBy)
        {
            if (searchBy == null)
                return;

            SSN = searchBy.Ssn;
            Name = new Name
            {
                First = searchBy.FirstName,
                Last = searchBy.LastName
            };
            Address = new Address
            {
                StreetAddress1 = searchBy.StreetAddress1,
                State = searchBy.State,
                City = searchBy.City,
                Zip5 = searchBy.Zip5
            };
        }
    }
}

