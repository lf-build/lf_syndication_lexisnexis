﻿using LendFoundry.Syndication.LexisNexis.BusinessReport;

namespace LendFoundry.Syndication.LexisNexis.CriminalRecord.Proxy
{
    /// <summary>
    /// lexisnexis
    /// </summary>
    public interface ICriminalSearchRecord
    {
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        bool AlsoFound { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        bool AlsoFoundSpecified { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string DataSource { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string OffenderId { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        IName Name { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        IAddress Address { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string Ssn { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        IDate Dob { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string StateOfOrigin { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string StateOfBirth { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string UniqueId { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string CountyOfOrigin { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string CaseNumber { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        IDate CaseFilingDate { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        IDate DateLastSeen { get; set; }
    }
}
