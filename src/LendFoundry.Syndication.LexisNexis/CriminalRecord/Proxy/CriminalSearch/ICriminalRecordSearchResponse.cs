﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.LexisNexis.CriminalRecord.Proxy
{
    /// <summary>
    /// lexisnexis
    /// </summary>
    public interface ICriminalRecordSearchResponse
    {
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        IResponseHeader Header { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        int RecordCount { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        List<ICriminalSearchRecord> Records { get; set; }
    }
}
