﻿using LendFoundry.Foundation.Client;
using LendFoundry.Syndication.LexisNexis.BusinessReport;
using LendFoundry.Syndication.LexisNexis.CriminalRecord.Proxy.CriminalSearch.ServiceReference;
using Newtonsoft.Json;

namespace LendFoundry.Syndication.LexisNexis.CriminalRecord.Proxy
{
    /// <summary>
    /// lexisnexis
    /// </summary>
    public class CriminalSearchRecord : ICriminalSearchRecord
    {
        /// <summary>
        /// lexisnexis
        /// </summary>
        public CriminalSearchRecord()
        {
        }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <param name="record"></param>
        public CriminalSearchRecord(CrimSearchRecord record)
        {
            if (record == null)
                return;

            AlsoFoundSpecified = record.AlsoFoundSpecified;
            AlsoFound = record.AlsoFound;
            DataSource = record.DataSource;
            OffenderId = record.OffenderId;
            CaseNumber = record.CaseNumber;
            Name = new Name(record.Name);
            Address = new Address(record.Address);
            Ssn = record.SSN;
            Dob = record.DOB != null ? new BusinessReport.Date { Day = record.DOB.Day, Month = record.DOB.Month, Year = record.DOB.Year } : null;
            StateOfOrigin = record.StateOfOrigin;
            UniqueId = record.UniqueId;
            CountyOfOrigin = record.CountyOfOrigin;
            DocNumber = record.DOCNumber;
            CaseFilingDate = record.CaseFilingDate != null ? new BusinessReport.Date { Day = record.CaseFilingDate.Day, Month = record.CaseFilingDate.Month, Year = record.CaseFilingDate.Year } : null;
            DateLastSeen = record.DateLastSeen != null ? new BusinessReport.Date { Day = record.DateLastSeen.Day, Month = record.DateLastSeen.Month, Year = record.DateLastSeen.Year } : null;
        }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public bool AlsoFound { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public bool AlsoFoundSpecified { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string DataSource { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string OffenderId { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        [JsonConverter(typeof(InterfaceConverter<IName, Name>))]
        public IName Name { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        [JsonConverter(typeof(InterfaceConverter<IAddress, Address>))]
        public IAddress Address { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string Ssn { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        [JsonConverter(typeof(InterfaceConverter<IDate, LendFoundry.Syndication.LexisNexis.BusinessReport.Date>))]
        public IDate Dob { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string StateOfOrigin { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string StateOfBirth { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string UniqueId { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string CountyOfOrigin { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string CaseNumber { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string DocNumber { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        [JsonConverter(typeof(InterfaceConverter<IDate, LendFoundry.Syndication.LexisNexis.BusinessReport.Date>))]
        public IDate CaseFilingDate { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        [JsonConverter(typeof(InterfaceConverter<IDate, LendFoundry.Syndication.LexisNexis.BusinessReport.Date>))]
        public IDate DateLastSeen { get; set; }
    }
}