﻿namespace LendFoundry.Syndication.LexisNexis.CriminalRecord
{
    /// <summary>
    /// lexisnexis
    /// </summary>
    public class TimeStamp
    {
        /// <summary>
        /// lexisnexis
        /// </summary>
        public TimeStamp()
        {}
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <param name="record"></param>
        public TimeStamp(Proxy.CriminalReport.ServiceReference.TimeStamp record)
        {
            if (record == null)
                return;

            Year = record.Year;
                YearSpecified = record.YearSpecified;
                Month = record.Month;
                MonthSpecified = record.MonthSpecified;
                Day = record.Day;
                DaySpecified = record.DaySpecified;
                Hour24 = record.Hour24;
                Minute = record.Minute;
                Hour24Specified = record.Hour24Specified;
                Second = record.Second;
                SecondSpecified = record.SecondSpecified;
        }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public short Year { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public bool YearSpecified { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public short Month { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public bool MonthSpecified { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public short Day { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public bool DaySpecified { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public short Hour24 { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public bool Hour24Specified { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public short Minute { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public bool MinuteSpecified { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public short Second { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public bool SecondSpecified { get; set; }
    }
}
