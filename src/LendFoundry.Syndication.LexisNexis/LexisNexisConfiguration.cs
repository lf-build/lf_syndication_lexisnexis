﻿using System.Collections.Generic;
using LendFoundry.Foundation.Client;

namespace LendFoundry.Syndication.LexisNexis
{
    /// <summary>
    /// lexisnexis
    /// </summary>
    public class LexisNexisConfiguration : ILexisNexisConfiguration,IDependencyConfiguration
    { 
        //public FlexIdServiceConfiguration FlexId { get; set; }
        //public FraudPointServiceConfiguration FraudPoint { get; set; }
        //public InstantIdServiceConfiguration InstantId { get; set; }
        //public KbaServiceConfiguration InstantIdKba { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public BankruptcyConfiguration Bankruptcy { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public CriminalRecordConfiguration CriminalRecord { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public Dictionary<string, string> Dependencies { get ; set ; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string Database { get ; set ; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public string ConnectionString { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public ADLConfiguration  AdlConfiguration { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        public LienJudgmentConfiguration LienJudgment { get; set; }
    }
}