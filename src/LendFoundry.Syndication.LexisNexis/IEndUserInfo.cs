namespace LendFoundry.Syndication.LexisNexis
{
	/// <summary>
	/// lexisnexis
	/// </summary>
	public interface IEndUserInfo
	{
        /// <summary>
        /// lexisnexis
        /// </summary>
		string CompanyName { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
		string StreetAddress1 { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
		string City { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
		string State { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
		string Zip5 { get; set; }
	}
}
