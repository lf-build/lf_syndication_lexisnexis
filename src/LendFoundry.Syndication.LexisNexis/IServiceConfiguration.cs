﻿namespace LendFoundry.Syndication.LexisNexis
{
    /// <summary>
    /// lexisnexis
    /// </summary>
    public interface IServiceConfiguration
    {
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string UserName { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string Password { get; set; }
    }
}
