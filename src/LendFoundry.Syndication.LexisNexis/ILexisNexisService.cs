﻿using LendFoundry.Syndication.LexisNexis.Bankruptcy;
using LendFoundry.Syndication.LexisNexis.CriminalRecord;
using LendFoundry.Syndication.LexisNexis.CriminalRecord.Proxy;
using LendFoundry.Syndication.LexisNexis.CriminalRecord.Proxy.CriminalReport;
using System.Threading.Tasks;
using LendFoundry.Syndication.LexisNexis.ADLFunctions;
using LendFoundry.Syndication.LexisNexis.LienJudgment;
using LendFoundry.Syndication.LexisNexis.LienJudgment.Response.Search;
using LendFoundry.Syndication.LexisNexis.LienJudgment.Search;


namespace LendFoundry.Syndication.LexisNexis
{
    /// <summary>
    /// lexisnexis
    /// </summary>
    public interface ILexisNexisService
    {
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        Task<IBankruptcySearchResponse> FcraBankruptcySearch(string entityType, string entityId, ISearchBankruptcyRequest request);
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        Task<IBankruptcyReportResponse> FcraBankruptcyReport(string entityType, string entityId, IBankruptcyFcraReportRequest request);
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        Task<IBankruptcyReportResponse> BankruptcyReport(string entityType, string entityId, IBankruptcyReportRequest request);
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        Task<ICriminalReportResponse> CriminalRecordReport(string entityType, string entityId, ICriminalRecordReportRequest request);
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        Task<ICriminalRecordSearchResponse> CriminalSearch(string entityType, string entityId, ICriminalRecordSearchRequest request);
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        Task<IADLSearchResponse> ADLSearch(string entityType, string entityId, IADLSearchRequest request);
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        Task<ILienJudgmentSearchResponse> LienJudgmentSearch(string entityType, string entityId,LendFoundry.Syndication.LexisNexis.LienJudgment.ILienJudgmentSearchRequest request);
        // IFlexIdResponse GetFlexIdVerificationResult(IFlexIdRequest request);
        // IFraudPointResponse GetFraudPointVerificationResult(IFraudPointRequest request);
        // InstantId.IInstantIdResponse GetInstantIdVerificationResult(InstantId.IInstantIdRequest request);
        // InstantIdKba.ITransactionResponse GetInstantIdKbaIdentityVerificationResult(InstantIdKba.IInstantIdIdentityRequest request);
        // InstantIdKba.ITransactionResponse GetInstantIdKbaContinuationVerificationResult(InstantIdKba.IInstantIdContinuationRequest request);
    }
}