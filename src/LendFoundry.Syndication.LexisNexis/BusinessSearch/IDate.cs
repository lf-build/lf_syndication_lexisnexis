namespace LendFoundry.Syndication.LexisNexis.BusinessSearch
{
	/// <summary>
	/// lexisnexis
	/// </summary>
	public interface IDate
	{
		/// <summary>
		/// lexisnexis
		/// </summary>
		/// <value></value>
		short Year { get; set; }
		/// <summary>
		/// lexisnexis
		/// </summary>
		/// <value></value>
		short Month { get; set; }
		/// <summary>
		/// lexisnexis
		/// </summary>
		/// <value></value>
		short Day { get; set; }
	}
}
