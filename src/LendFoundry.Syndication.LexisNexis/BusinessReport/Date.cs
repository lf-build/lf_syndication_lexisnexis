namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
    /// <summary>
    /// lexisnexis
    /// </summary>
	public class Date : IDate
	{
        /// <summary>
        /// lexisnexis
        /// </summary>
        public Date()
        { }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <param name="date"></param>
        public Date(Date date)
        {
            if (date == null)
                return;
            Year = date.Year;
            Month = date.Month;
            Day = date.Day;
        }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
		public short Year { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
		public short Month { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
		public short Day { get; set; }
	}
}
