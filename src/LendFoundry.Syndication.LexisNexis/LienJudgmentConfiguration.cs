﻿using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using LendFoundry.Syndication.LexisNexis.LienJudgment;

namespace LendFoundry.Syndication.LexisNexis
{
    /// <summary>
    /// LienJudgmentSearch
    /// </summary>
    public class LienJudgmentConfiguration
    {
        /// <summary>
        /// LienJudgmentSearch
        /// </summary>
        /// <value></value>
        public string Url { get; set; }
        /// <summary>
        /// LienJudgmentSearch
        /// </summary>
        /// <value></value>
        public string UserName { get; set; }
        /// <summary>
        /// LienJudgmentSearch
        /// </summary>
        /// <value></value>
        public string Password { get; set; }
        
        /// <summary>
        /// LienJudgmentSearch
        /// </summary>
        /// <value></value>
        [JsonConverter(typeof(InterfaceConverter<LienJudgment.IUser, LienJudgment.User>))]
        public LienJudgment.IUser EndUser { get; set; }
        
        /// <summary>
        /// LienJudgmentSearch
        /// </summary>
        /// <value></value>
        [JsonConverter(typeof(InterfaceConverter<ISearchOption, LienJudgment.SearchOption>))]
        public LienJudgment.ISearchOption Options { get; set; }
    }
}


