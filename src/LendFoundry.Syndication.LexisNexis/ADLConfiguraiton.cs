namespace LendFoundry.Syndication.LexisNexis
{
    /// <summary>
    /// lexisnexis
    /// </summary>
    public class ADLConfiguration
    {
        /// <summary>
        /// lexisnexis
        /// </summary>
        public string AllPossiblesADLSerchUrl {get;set;}
        /// <summary>
        /// lexisnexis
        /// </summary>
        public string UserName { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        public string Password { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        public string GLBPurpose { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        public string DLPurpose { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        public bool NameIsOrdered {get;set;}
        /// <summary>
        /// lexisnexis
        /// </summary>
        public int Threshold { get; set; }
        
        /// <summary>
        /// lexisnexis
        /// </summary>
        public bool BestSSN { get; set; } 
        /// <summary>
        /// lexisnexis
        /// </summary>
        public bool BestDOB { get; set; } 
        /// <summary>
        /// lexisnexis
        /// </summary>
        public bool BestName { get; set; } 
        /// <summary>
        /// lexisnexis
        /// </summary>
        public bool BestAddr { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        public bool BestPhone { get; set; } 
        /// <summary>
        /// lexisnexis
        /// </summary>
        public bool BestDOD { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        public bool VerifySSN { get; set; } 
        /// <summary>
        /// lexisnexis
        /// </summary>
        public bool VerifyDOB { get; set; } 
        /// <summary>
        /// lexisnexis
        /// </summary>
        public bool VerifyName { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        public bool VerifySSN4 { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        public bool VerifyAddr { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        public bool VerifyPhone { get; set; }
    }
    
}