﻿using System;
using LendFoundry.Syndication.LexisNexis.Bankruptcy.Proxy;
using LendFoundry.Foundation.Lookup;
using LendFoundry.EventHub;
using LendFoundry.Foundation.Logging;

namespace LendFoundry.Syndication.LexisNexis
{
    /// <summary>
    /// lexisnexis
    /// </summary>
    public class BankruptcyService : IBankruptcyService
    {
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <param name="bankruptcyServiceProxy"></param>
        /// <param name="configuration"></param>
        /// <param name="lookupService"></param>
        /// <param name="logger"></param>
        /// <param name="eventHubClient"></param>
        public BankruptcyService(IBankruptcyProxy bankruptcyServiceProxy, LexisNexisConfiguration configuration, ILookupService lookupService, ILogger logger, IEventHubClient eventHubClient)
        {
            if (bankruptcyServiceProxy == null)
                throw new ArgumentNullException(nameof(bankruptcyServiceProxy));
            if (configuration == null)
                throw new ArgumentNullException(nameof(configuration));

            BankruptcyServiceProxy = bankruptcyServiceProxy;
            Configuration = configuration;
            Logger = logger;
            EventHubClient = eventHubClient;
            LookupService = lookupService;
        }

        private IBankruptcyProxy BankruptcyServiceProxy { get; }
        private LexisNexisConfiguration Configuration { get; }
        private ILookupService LookupService { get; }
        private ILogger Logger { get; }
        private IEventHubClient EventHubClient { get; }

      
        private string EnsureEntityType(string entityType)
        {
            if (string.IsNullOrWhiteSpace(entityType))
                throw new ArgumentException($"{nameof(entityType)} is mandatory");

            entityType = entityType.ToLower();

            if (LookupService.GetLookupEntry("entityTypes", entityType) == null)
                throw new ArgumentException($"{entityType} is not a valid entity");

            return entityType;
        }
    }
}
