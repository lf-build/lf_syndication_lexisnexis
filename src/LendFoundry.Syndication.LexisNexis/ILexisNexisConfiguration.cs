﻿using LendFoundry.Foundation.Client;
using System.Collections.Generic;

namespace LendFoundry.Syndication.LexisNexis
{
    /// <summary>
    /// lexisnexis
    /// </summary>
    public interface ILexisNexisConfiguration : IDependencyConfiguration
    {
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        BankruptcyConfiguration Bankruptcy { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        LienJudgmentConfiguration LienJudgment { get; set; }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <value></value>
        string ConnectionString { get; set; }

    }
}