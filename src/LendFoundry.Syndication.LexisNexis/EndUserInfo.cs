namespace LendFoundry.Syndication.LexisNexis
{
    /// <summary>
    /// lexisnexis
    /// </summary>
    public class EndUserInfo : IEndUserInfo
    {
        /// <summary>
        /// lexisnexis
        /// </summary>
        public EndUserInfo() { }
        /// <summary>
        /// lexisnexis
        /// </summary>
        /// <param name="endUserInfo"></param>
        public EndUserInfo(LendFoundry.Syndication.LexisNexis.LienJudgment.Report.EndUserInfo endUserInfo)
        {
            if (endUserInfo == null)
                return;
            CompanyName = endUserInfo.CompanyName;
            StreetAddress1 = endUserInfo.StreetAddress1;
            City = endUserInfo.City;
            State = endUserInfo.State;
            //Zip5 = endUserInfo.Zip5;
        }
       
        /// <summary>
        /// lexisnexis
        /// </summary>
        public string CompanyName { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        public string StreetAddress1 { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        public string City { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        public string State { get; set; }

        /// <summary>
        /// lexisnexis
        /// </summary>
        public string Zip5 { get; set; }
    }
}