﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using LendFoundry.EventHub;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Lookup;
using LendFoundry.Foundation.Services;
using LendFoundry.Syndication.LexisNexis.ADLFunctions;
using LendFoundry.Syndication.LexisNexis.ADLFunctions.Proxy;
using LendFoundry.Syndication.LexisNexis.ADLFunctions.Proxy.AllPossiblesADL.ServiceReference;
using LendFoundry.Syndication.LexisNexis.Bankruptcy;
using LendFoundry.Syndication.LexisNexis.Bankruptcy.Proxy;
using LendFoundry.Syndication.LexisNexis.Bankruptcy.Proxy.BankruptcyReport;
using LendFoundry.Syndication.LexisNexis.Bankruptcy.Proxy.BankruptcyReport.NonFCRAServiceReference;
using LendFoundry.Syndication.LexisNexis.Bankruptcy.Proxy.BankruptcyReport.ServiceReference;
using LendFoundry.Syndication.LexisNexis.Bankruptcy.Proxy.BankruptcySearch.ServiceReference;
using LendFoundry.Syndication.LexisNexis.CriminalRecord;
using LendFoundry.Syndication.LexisNexis.CriminalRecord.Proxy;
using LendFoundry.Syndication.LexisNexis.CriminalRecord.Proxy.CriminalReport;
using LendFoundry.Syndication.LexisNexis.CriminalRecord.Proxy.CriminalReport.ServiceReference;
using LendFoundry.Syndication.LexisNexis.CriminalRecord.Proxy.CriminalSearch.ServiceReference;
using LendFoundry.Syndication.LexisNexis.Events;
using LendFoundry.Syndication.LexisNexis.LienJudgment;
using LendFoundry.Syndication.LexisNexis.LienJudgment.Response.Search;
using LendFoundry.Syndication.LexisNexis.LienJudgment.Search;

namespace LendFoundry.Syndication.LexisNexis
{
    /// <summary>
    /// LexisNexisService
    /// </summary>
    public class LexisNexisService : ILexisNexisService
    {
        /// <summary>
        /// LexisNexisService
        /// </summary>
        /// <param name="adlSearchPorxy"></param>
        /// <param name="criminalServiceProxy"></param>
        /// <param name="bankruptcyServiceProxy"></param>
        /// <param name="configuration"></param>
        /// <param name="lookupService"></param>
        /// <param name="logger"></param>
        /// <param name="eventHubClient"></param>
        /// <param name="tenantTime"></param>
        /// <param name="lienJudgmentClient"></param>
        public LexisNexisService
        (
            IADLSearchPorxy adlSearchPorxy,
            ICriminalRecordProxy criminalServiceProxy,
            IBankruptcyProxy bankruptcyServiceProxy, LexisNexisConfiguration configuration,
            ILookupService lookupService, ILogger logger, IEventHubClient eventHubClient, ITenantTime tenantTime,
            ILienJudgmentClient lienJudgmentClient
        )
        {
            //if (flexIdClient == null)
            //    throw new ArgumentNullException(nameof(flexIdClient));

            //if (fraudPointClient == null)
            //    throw new ArgumentNullException(nameof(fraudPointClient));

            //if (instantIdClient == null)
            //    throw new ArgumentNullException(nameof(instantIdClient));

            //if (instantIdKbaClient == null)
            //    throw new ArgumentNullException(nameof(instantIdKbaClient));

            if (bankruptcyServiceProxy == null)
                throw new ArgumentNullException(nameof(bankruptcyServiceProxy));
            if (configuration == null)
                throw new ArgumentNullException(nameof(configuration));

            if (criminalServiceProxy == null)
                throw new ArgumentNullException(nameof(criminalServiceProxy));
            if (adlSearchPorxy == null)
                throw new ArgumentException(nameof(adlSearchPorxy));

            if (lienJudgmentClient == null)
                throw new ArgumentException(nameof(lienJudgmentClient));
          
            //FlexIdClient = flexIdClient;
            //FraudPointClient = fraudPointClient;
            //InstantIdClient = instantIdClient;
            //InstantIdKbaClient = instantIdKbaClient;

            BankruptcyServiceProxy = bankruptcyServiceProxy;
            Configuration = configuration;
            Logger = logger;
            EventHubClient = eventHubClient;
            LookupService = lookupService;
            CriminalServiceProxy = criminalServiceProxy;
            TenantTime = tenantTime;
            AdlSearchPorxy = adlSearchPorxy;
            LienJudgmentClient = lienJudgmentClient;
        }

        private IBankruptcyProxy BankruptcyServiceProxy { get; }
        private LexisNexisConfiguration Configuration { get; }
        private ILookupService LookupService { get; }
        private ILogger Logger { get; }
        private IEventHubClient EventHubClient { get; }
        private ITenantTime TenantTime { get; }
        private ICriminalRecordProxy CriminalServiceProxy { get; }
        private IADLSearchPorxy AdlSearchPorxy { get; }

        private ILienJudgmentClient LienJudgmentClient { get; }
        /// <summary>
        /// FcraBankruptcySearch
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<IBankruptcySearchResponse> FcraBankruptcySearch(string entityType, string entityId,
            ISearchBankruptcyRequest request)
        {
            IBankruptcySearchResponse searchResult = null;
            var referencenumber = Guid.NewGuid().ToString("N");
            try
            {
                #region Validation

                entityType = EnsureEntityType(entityType);

                if (string.IsNullOrEmpty(entityId))
                    throw new ArgumentNullException(nameof(entityId));

                if (request == null)
                    throw new ArgumentNullException($"The {nameof(request)} cannot be null");

                if (string.IsNullOrWhiteSpace(request.Ssn))
                    throw new ArgumentNullException($"The {nameof(request.Ssn)} cannot be null");

                if (!Regex.IsMatch(request.Ssn, "^([0-9]{9}|[0-9]{3}-[0-9]{2}-[0-9]{4}|[0-9]+)$"))
                    throw new ArgumentException($"Invalid  {nameof(request.Ssn)}");

                if (string.IsNullOrWhiteSpace(request.FirstName))
                    throw new ArgumentNullException($"The {nameof(request.FirstName)} cannot be null");

                if (!Regex.IsMatch(request.FirstName, "^([a-zA-Z\\s]+)$"))
                    throw new ArgumentException($"Invalid {nameof(request.FirstName)}");

                if (string.IsNullOrWhiteSpace(request.LastName))
                    throw new ArgumentNullException($"The {nameof(request.LastName)} cannot be null");

                if (!Regex.IsMatch(request.LastName, "^([a-zA-Z\\s]+)$"))
                    throw new ArgumentException($"Invalid {nameof(request.LastName)}");

                #endregion Validation

                Logger.Info("Started Execution for FcraBankruptcySearch Request Info: Date & Time:" + TenantTime.Now +
                            " EntityType:" + entityType + " EntityId:" + entityId + "ReferenceNumber" +
                            referencenumber + " TypeOf LexisNexis: BankruptcySearch");

                var user = new Bankruptcy.Proxy.BankruptcySearch.ServiceReference.User(Configuration.Bankruptcy
                    .EndUser);
                var options = new FcraBankruptcySearch3Option(Configuration.Bankruptcy.Options);
                var searchBy = new FcraBankruptcySearch3By(request);
                searchResult =
                    new BankruptcySearchResponse(
                        await BankruptcyServiceProxy.BankruptcySearch(user, searchBy, options));
                if (searchResult != null)
                {
                    await EventHubClient.Publish(new BankruptcySearchRequested
                    {
                        EntityId = entityId,
                        EntityType = entityType,
                        Response = searchResult,
                        Request = searchBy,
                        ReferenceNumber = referencenumber
                    });
                }
            }
            catch (LexisNexisException ex)
            {
                Logger.Error($"The method Bankruptcy Search({request}) raised an error: {ex.Message}");
                await EventHubClient.Publish(new BankruptcySearchRequesteFail
                {
                    EntityId = entityId,
                    EntityType = entityType,
                    Response = ex.Message,
                    Request = request,
                    ReferenceNumber = referencenumber
                });
                throw new NotFoundException($"Information not found for this request");
            }
            catch (System.Exception ex)
            {
                Logger.Error(ex.Message);
                await EventHubClient.Publish(new BankruptcySearchRequesteFail
                {
                    EntityId = entityId,
                    EntityType = entityType,
                    Response = ex.Message,
                    Request = request,
                    ReferenceNumber = referencenumber
                });
                throw ex;
            }

            Logger.Info("Completed Execution for FcraBankruptcySearch Request Info: Date & Time:" + TenantTime.Now +
                        " EntityType:" + entityType + " EntityId:" + entityId + "ReferenceNumber" + referencenumber +
                        " TypeOf LexisNexis: BankruptcySearch");

            return searchResult;
        }
        /// <summary>
        /// FcraBankruptcyReport
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<IBankruptcyReportResponse> FcraBankruptcyReport(string entityType, string entityId,
            IBankruptcyFcraReportRequest request)
        {
            IBankruptcyReportResponse searchResult = null;
            try
            {
                Logger.Info("Started Execution for FcraBankruptcyReport Request Info: Date & Time:" + TenantTime.Now +
                            " EntityType:" + entityType + " EntityId:" + entityId +
                            " TypeOf LexisNexis: FcraBankruptcyReport");

                entityType = EnsureEntityType(entityType);

                if (string.IsNullOrEmpty(entityId))
                    throw new ArgumentNullException(nameof(entityId));

                if (request == null)
                    throw new ArgumentNullException($"The {nameof(request)} cannot be null");

//                if (string.IsNullOrWhiteSpace(request.QueryId))
//                    throw new ArgumentNullException($"The {nameof(request.QueryId)} cannot be null");
//
//                if (!Regex.IsMatch(request.QueryId, "^([a-zA-Z0-9]+)$"))
//                    throw new ArgumentException($"Invalid {nameof(request.QueryId)}");

                if (string.IsNullOrWhiteSpace(request.UniqueId))
                    throw new ArgumentNullException($"The {nameof(request.UniqueId)} cannot be null");

                if (!Regex.IsMatch(request.UniqueId, "^([0-9]+)$"))
                    throw new ArgumentException($"Invalid {nameof(request.UniqueId)}");

                var user = new Bankruptcy.Proxy.BankruptcyReport.ServiceReference.User(request);
                var options = new FcraBankruptcyReport3By(request);
                var searchBy = new FcraBankruptcyReport3Option(Configuration.Bankruptcy.Options);
                searchResult =
                    new BankruptcyReportResponse(await BankruptcyServiceProxy.BankruptcyReport(user, options, searchBy),
                        true);
            }
            catch (LexisNexisException ex)
            {
                Logger.Error($"The method Bankruptcy Search({request}) raised an error: {ex.Message}");
                throw new NotFoundException($"Information not found for this request");
            }
            catch (System.Exception ex)
            {
                Logger.Error(ex.Message);
                throw ex;
            }

            Logger.Info("Completed Execution for FcraBankruptcyReport Request Info: Date & Time:" + TenantTime.Now +
                        " EntityType:" + entityType + " EntityId:" + entityId +
                        " TypeOf LexisNexis: FcraBankruptcyReport");

            return searchResult;
        }
        /// <summary>
        /// NonFcraBankruptcyReport
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<IBankruptcyReportResponse> NonFcraBankruptcyReport(string entityType, string entityId,
            IBankruptcyReportRequest request)
        {
            IBankruptcyReportResponse searchResult = null;
            try
            {
                if (string.IsNullOrEmpty(entityType))
                    throw new ArgumentNullException(nameof(entityType));

                if (string.IsNullOrEmpty(entityId))
                    throw new ArgumentNullException(nameof(entityId));

                if (request == null)
                    throw new ArgumentNullException($"The {nameof(request)} cannot be null");

                entityType = EnsureEntityType(entityType);

                var user = new Bankruptcy.Proxy.BankruptcyReport.NonFCRAServiceReference.User(request);
                var options = new BankruptcyReport2Option(Configuration.Bankruptcy.Options);
                var reportBy = new BankruptcyReport2By(request);
                searchResult =
                    new BankruptcyReportResponse(
                        await BankruptcyServiceProxy.NonFCRABankruptcyReport(user, reportBy, options));
            }
            catch (LexisNexisException ex)
            {
                Logger.Error($"The method Bankruptcy Search({request}) raised an error: {ex.Message}");
                throw new NotFoundException($"Information not found for this request");
            }
            catch (System.Exception ex)
            {
                Logger.Error(ex.Message);
                throw ex;
            }

            return searchResult;
        }
        /// <summary>
        /// BankruptcyReport
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<IBankruptcyReportResponse> BankruptcyReport(string entityType, string entityId,
            IBankruptcyReportRequest request)
        {
            IBankruptcyReportResponse searchResult = null;
            var referencenumber = Guid.NewGuid().ToString("N");
            try
            {
                Logger.Info("Started Execution for BankruptcyReport Request Info: Date & Time:" + TenantTime.Now +
                            " EntityType:" + entityType + " EntityId:" + entityId +
                            " TypeOf LexisNexis: BankruptcyReport");

                if (string.IsNullOrEmpty(entityType))
                    throw new ArgumentNullException(nameof(entityType));

                if (string.IsNullOrEmpty(entityId))
                    throw new ArgumentNullException(nameof(entityId));

                if (request == null)
                    throw new ArgumentNullException($"The {nameof(request)} cannot be null");

                entityType = EnsureEntityType(entityType);

                IBankruptcyReportResponse fcraResult = await FcraBankruptcyReport(entityType, entityId,
                    new BankruptcyFcraReportRequest
                    {
                        QueryId = request.TMSId,
                        UniqueId = request.UniqueId
                    });

                IBankruptcyReportResponse nonFcraResult = await NonFcraBankruptcyReport(entityType, entityId, request);

                Bankruptcy.IResponseHeader header = null;
                List<IBankruptcyReportRecord> records = new List<IBankruptcyReportRecord>();
                int count = 0;

                if (fcraResult != null)
                {
                    header = fcraResult.Header;
                    records = fcraResult.Records;
                    count += fcraResult.RecordCount;
                }
                else
                {
                    if (nonFcraResult != null)
                    {
                        header = nonFcraResult.Header;
                        if (nonFcraResult.Records != null && nonFcraResult.Records.Any())
                            records.AddRange(nonFcraResult.Records);
                        count += nonFcraResult.RecordCount;
                    }
                }

                if (nonFcraResult != null)
                {
                    header = header == null ? nonFcraResult.Header : header;
                    if (nonFcraResult.Records != null && nonFcraResult.Records.Any())
                        records.AddRange(nonFcraResult.Records);
                    count += nonFcraResult.RecordCount;
                }

                searchResult = new BankruptcyReportResponse();
                searchResult.Header = header;
                searchResult.RecordCount = count;
                searchResult.Records = records;

                if (searchResult != null)
                {
                    await EventHubClient.Publish(new BankruptcyReportRequested
                    {
                        EntityId = entityId,
                        EntityType = entityType,
                        Response = searchResult,
                        Request = request,
                        ReferenceNumber = referencenumber
                    });
                }
            }
            catch (LexisNexisException ex)
            {
                Logger.Error($"The method Bankruptcy Search({request}) raised an error: {ex.Message}");
                await EventHubClient.Publish(new BankruptcyReportRequesteFail
                {
                    EntityId = entityId,
                    EntityType = entityType,
                    Response = ex.Message,
                    Request = request,
                    ReferenceNumber = referencenumber,
                });
                throw new NotFoundException($"Information not found for this request");
            }
            catch (System.Exception ex)
            {
                Logger.Error(ex.Message);
                await EventHubClient.Publish(new BankruptcyReportRequesteFail
                {
                    EntityId = entityId,
                    EntityType = entityType,
                    Response = ex.Message,
                    Request = request,
                    ReferenceNumber = referencenumber
                });
                throw ex;
            }

            Logger.Info("Completed Execution for BankruptcyReport Request Info: Date & Time:" + TenantTime.Now +
                        " EntityType:" + entityType + " EntityId:" + entityId + " TypeOf LexisNexis: BankruptcyReport");

            return searchResult;
        }
        /// <summary>
        /// CriminalSearch
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<ICriminalRecordSearchResponse> CriminalSearch(string entityType, string entityId,
            ICriminalRecordSearchRequest request)
        {
            ICriminalRecordSearchResponse searchResult = null;
            var referencenumber = Guid.NewGuid().ToString("N");
            try
            {
                #region Validations

                entityType = EnsureEntityType(entityType);

                if (string.IsNullOrEmpty(entityId))
                    throw new ArgumentNullException(nameof(entityId));

                if (request == null)
                    throw new ArgumentNullException($"The {nameof(request)} cannot be null");

                if (string.IsNullOrWhiteSpace(request.Ssn))
                    throw new ArgumentNullException($"The {nameof(request.Ssn)} cannot be null");

                if (!Regex.IsMatch(request.Ssn, "^([0-9]{3}-[0-9]{2}-[0-9]{4}|[0-9]+)$"))
                    throw new ArgumentException("Invalid Ssn Number format");

                if (string.IsNullOrWhiteSpace(request.FirstName))
                    throw new ArgumentNullException($"The {nameof(request.FirstName)} cannot be null");

                if (!Regex.IsMatch(request.FirstName, "^([a-zA-Z\\s]+)$"))
                    throw new ArgumentException($"Invalid {nameof(request.FirstName)}");

                if (string.IsNullOrWhiteSpace(request.LastName))
                    throw new ArgumentNullException($"The {nameof(request.LastName)} cannot be null");

                if (!Regex.IsMatch(request.LastName, "^([a-zA-Z\\s]+)$"))
                    throw new ArgumentException($"Invalid {nameof(request.LastName)}");

                #endregion Validations

                Logger.Info("Started Execution for CriminalSearch Request Info: Date & Time:" + TenantTime.Now +
                            " EntityType:" + entityType + " EntityId:" + entityId +
                            " TypeOf LexisNexis: CriminalSearch");
                var user = new CriminalRecord.Proxy.CriminalSearch.ServiceReference.User(Configuration.CriminalRecord
                    .EndUser);
                var options = new FcraCriminalSearchOption(Configuration.CriminalRecord.Options);
                var searchBy = new FcraCriminalSearchBy(request);
                searchResult =
                    new CriminalRecordSearchResponse(
                        await CriminalServiceProxy.CriminalRecordSearch(user, searchBy, options));
                if (searchResult != null)
                {
                    await EventHubClient.Publish(new CriminalRecordSearchRequested
                    {
                        EntityId = entityId,
                        EntityType = entityType,
                        Response = searchResult,
                        Request = request,
                        ReferenceNumber = referencenumber
                    });
                }
            }
            catch (LexisNexisException ex)
            {
                Logger.Error($"The method Bankruptcy Search({request}) raised an error: {ex.Message}");
                await EventHubClient.Publish(new CriminalRecordSearchRequesteFail
                {
                    EntityId = entityId,
                    EntityType = entityType,
                    Response = ex.Message,
                    Request = request,
                    ReferenceNumber = referencenumber
                });
                throw new NotFoundException($"Information not found for this request");
            }
            catch (System.Exception ex)
            {
                Logger.Error(ex.Message);
                await EventHubClient.Publish(new CriminalRecordSearchRequesteFail
                {
                    EntityId = entityId,
                    EntityType = entityType,
                    Response = ex.Message,
                    Request = request,
                    ReferenceNumber = referencenumber
                });
                throw ex;
            }

            Logger.Info("Completed Execution for CriminalSearch Request Info: Date & Time:" + TenantTime.Now +
                        " EntityType:" + entityType + " EntityId:" + entityId + " TypeOf LexisNexis: CriminalSearch");

            return searchResult;
        }
        /// <summary>
        /// CriminalRecordReport
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<ICriminalReportResponse> CriminalRecordReport(string entityType, string entityId,
            ICriminalRecordReportRequest request)
        {
            ICriminalReportResponse reportResult = null;
            var referencenumber = Guid.NewGuid().ToString("N");
            try
            {
                #region Validations

                entityType = EnsureEntityType(entityType);

                if (string.IsNullOrEmpty(entityId))
                    throw new ArgumentNullException(nameof(entityId));

                if (request == null)
                    throw new ArgumentNullException($"The {nameof(request)} cannot be null");

//                if (string.IsNullOrWhiteSpace(request.QueryId))
//                    throw new ArgumentNullException($"The {nameof(request.QueryId)} cannot be null");
//
//                if (!Regex.IsMatch(request.QueryId, "^([a-zA-Z0-9]+)$"))
//                    throw new ArgumentException($"Invalid {nameof(request.QueryId)}");

                if (string.IsNullOrWhiteSpace(request.UniqueId))
                    throw new ArgumentNullException($"The {nameof(request.UniqueId)} cannot be null");

                if (!Regex.IsMatch(request.UniqueId, "^([0-9]+)$"))
                    throw new ArgumentException($"Invalid {nameof(request.UniqueId)}");

                #endregion Validations

                Logger.Info("Started Execution for CriminalRecordReport Request Info: Date & Time:" + TenantTime.Now +
                            " EntityType:" + entityType + " EntityId:" + entityId +
                            " TypeOf LexisNexis: CriminalRecordReport");

                var user = new CriminalRecord.Proxy.CriminalReport.ServiceReference.User(request);
                var options = new FcraCriminalReportOption(Configuration.CriminalRecord.Options);
                var searchBy = new FcraCriminalReportBy(request);
                reportResult =
                    new CriminalRecord.Proxy.CriminalReport.CriminalReportResponse(
                        await CriminalServiceProxy.CriminalRecordReport(user, searchBy, options));
                if (reportResult != null)
                {
                    await EventHubClient.Publish(new CriminalRecordReportRequested
                    {
                        EntityId = entityId,
                        EntityType = entityType,
                        Response = reportResult,
                        Request = request,
                        ReferenceNumber = referencenumber
                    });
                }
            }
            catch (LexisNexisException ex)
            {
                Logger.Error($"The method Bankruptcy Search({request}) raised an error: {ex.Message}");
                await EventHubClient.Publish(new CriminalRecordReportRequesteFail
                {
                    EntityId = entityId,
                    EntityType = entityType,
                    Response = ex.Message,
                    Request = request,
                    ReferenceNumber = referencenumber
                });
                throw new NotFoundException($"Information not found for this request");
            }
            catch (System.Exception ex)
            {
                Logger.Error(ex.Message);
                await EventHubClient.Publish(new CriminalRecordReportRequesteFail
                {
                    EntityId = entityId,
                    EntityType = entityType,
                    Response = ex.Message,
                    Request = request,
                    ReferenceNumber = referencenumber
                });
                throw ex;
            }

            Logger.Info("Completed Execution for CriminalRecordReport Request Info: Date & Time:" + TenantTime.Now +
                        " EntityType:" + entityType + " EntityId:" + entityId +
                        " TypeOf LexisNexis: CriminalRecordReport");

            return reportResult;
        }
        /// <summary>
        /// ADLSearch
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<IADLSearchResponse> ADLSearch(string entityType, string entityId, IADLSearchRequest request)
        {
            IADLSearchResponse searchResult = null;
            var referencenumber = Guid.NewGuid().ToString("N");
            try
            {
                #region Validations

                entityType = EnsureEntityType(entityType);

                if (string.IsNullOrEmpty(entityId))
                    throw new ArgumentNullException(nameof(entityId));

                if (request == null)
                    throw new ArgumentNullException($"The {nameof(request)} cannot be null");

                if (string.IsNullOrWhiteSpace(request.Ssn))
                    throw new ArgumentNullException($"The {nameof(request.Ssn)} cannot be null");

                if (!Regex.IsMatch(request.Ssn, "^([0-9]{3}-[0-9]{2}-[0-9]{4}|[0-9]+)$"))
                    throw new ArgumentException("Invalid Ssn Number format");

                if (string.IsNullOrWhiteSpace(request.Firstname))
                    throw new ArgumentNullException($"The {nameof(request.Firstname)} cannot be null");

                if (!Regex.IsMatch(request.Firstname, "^([a-zA-Z\\s]+)$"))
                    throw new ArgumentException($"Invalid {nameof(request.Firstname)}");

                if (string.IsNullOrWhiteSpace(request.Lastname))
                    throw new ArgumentNullException($"The {nameof(request.Lastname)} cannot be null");

                if (!Regex.IsMatch(request.Lastname, "^([a-zA-Z\\s]+)$"))
                    throw new ArgumentException($"Invalid {nameof(request.Lastname)}");

                #endregion Validations

                Logger.Info("Started Execution for ADLSearch Request Info: Date & Time:" + TenantTime.Now +
                            " EntityType:" + entityType + " EntityId:" + entityId + " TypeOf LexisNexis: ADLSearch");
                var proxyrequest = new AllPossiblesADLRequest(request, Configuration.AdlConfiguration);
               // var xml = Serialize<AllPossiblesADLRequest>(proxyrequest);
                var proxyresponse = await AdlSearchPorxy.AllPossiblesADLSearch(proxyrequest);
                
                searchResult = new ADLSearchResponse(proxyresponse);
                if (searchResult?.PossibleADLs != null)
                {
                    await EventHubClient.Publish(new ADLSearchRequested
                    {
                        EntityId = entityId,
                        EntityType = entityType,
                        Response = searchResult,
                        Request = request,
                        ReferenceNumber = referencenumber
                    });
                }
      
               
             
            }
            catch (LexisNexisException ex)
            {
                Logger.Error($"The method ADLSearch ({request}) raised an error: {ex.Message}");
                await EventHubClient.Publish(new ADLSearchRequestFail()
                {
                    EntityId = entityId,
                    EntityType = entityType,
                    Response = ex.Message,
                    Request = request,
                    ReferenceNumber = referencenumber
                });
                throw new NotFoundException($"Information not found for this request");
            }
            catch (System.Exception ex)
            {
                Logger.Error(ex.Message);
                await EventHubClient.Publish(new ADLSearchRequestFail
                {
                    EntityId = entityId,
                    EntityType = entityType,
                    Response = ex.Message,
                    Request = request,
                    ReferenceNumber = referencenumber
                });
                throw ex;
            }

            Logger.Info("Completed Execution for ADLSearch Request Info: Date & Time:" + TenantTime.Now +
                        " EntityType:" + entityType + " EntityId:" + entityId + " TypeOf LexisNexis: ADLSearch");

            return searchResult;
        }
        /// <summary>
        /// LienJudgmentSearch
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<ILienJudgmentSearchResponse> LienJudgmentSearch(string entityType, string entityId, ILienJudgmentSearchRequest request)
        {
             ILienJudgmentSearchResponse searchResponse = null;
             var referenceNumber = Guid.NewGuid().ToString("N");
                try
                {
                    entityType = EnsureEntityType(entityType);
                    if (string.IsNullOrEmpty(entityId))
                        throw new ArgumentNullException(nameof(entityId));
                    if (request == null)
                        throw new ArgumentNullException($"The {nameof(request)} cannot be null");
                    
                     Logger.Info("Started Execution for LienJudgment search Request Info: Date & Time:" + TenantTime.Now +
                            " EntityType:" + entityType + " EntityId:" + entityId + " TypeOf LexisNexis: LienJudgmentSearch");
                     
                     searchResponse = await LienJudgmentClient.Search(request);
                    await EventHubClient.Publish("LienJudgmentSearchRequested", new
                    {
                        EntityId = entityId,
                        EntityType = entityType,
                        Response = searchResponse,
                        Request = request,
                        ReferenceNumber = referenceNumber
                    });
                }
                catch (LexisNexisException ex)
                {
                    Logger.Error($"The method LienJudgment Search({request}) raised an error: {ex.Message}");

                    throw new NotFoundException($"Information not found for this request");
                }
                  Logger.Info("Completed Execution for LienJudgment Search Request Info: Date & Time:" + TenantTime.Now +
                        " EntityType:" + entityType + " EntityId:" + entityId + " TypeOf LexisNexis: LienJudgmentSearch");

           return searchResponse;
        }

        private string EnsureEntityType(string entityType)
        {
            if (string.IsNullOrWhiteSpace(entityType))
                throw new ArgumentException($"{nameof(entityType)} is mandatory");

            entityType = entityType.ToLower();

            if (LookupService.GetLookupEntry("entityTypes", entityType) == null)
                throw new ArgumentException($"{entityType} is not a valid entity");

            return entityType;
        }
    }
}